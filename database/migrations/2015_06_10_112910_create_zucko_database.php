<?php

//
// NOTE Migration Created: 2015-06-10 11:29:10
// --------------------------------------------------

use Zucko\Core\Business\Business;
use Zucko\Core\Photo\Photo;
use Zucko\Core\User\Role;

class CreateZuckoDatabase
{
//
// NOTE - Make changes to the database.
// --------------------------------------------------

    public function up()
    {

//
// NOTE -- bl_business
// --------------------------------------------------

        Schema::create('bl_business', function ($table) {
            $table->increments('id');
            $table->string('company_or_service', 150);
            $table->string('address', 100);
            $table->string('zip_code', 30);
            $table->string('brand', 30);
            $table->string('category', 30);
            $table->unsignedInteger('category_id');
            $table->string('city', 30);
            $table->string('client', 30);
            $table->string('first_name', 30);
            $table->string('position', 30);
            $table->string('email', 100);
            $table->string('facebook', 100);
            $table->string('fax', 30);
            $table->string('logo', 100);
            $table->string('island_country', 40);
            $table->string('last_name', 20);
            $table->string('pro_ref', 30);
            $table->string('share_capital', 30);
            $table->string('tahiti_id', 30);
            $table->string('tahiti_rc', 30);
            $table->string('company_type', 30);
            $table->string('vini', 30);
            $table->string('website', 100);
            $table->string('phone', 20);
        });


//
// NOTE -- blog_categories
// --------------------------------------------------

        Schema::create('blog_categories', function ($table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('slag', 50);
            $table->string('style', 150);
            $table->unsignedInteger('blog_count');
        });


//
// NOTE -- blog_comments
// --------------------------------------------------

        Schema::create('blog_comments', function ($table) {
            $table->increments('id')->unsigned();
            $table->text('description');
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('blog_id')->unsigned();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->unsignedInteger('parent_id')->unsigned();
        });


//
// NOTE -- blogs
// --------------------------------------------------

        Schema::create('blogs', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('title', 150);
            $table->text('content');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('user_id')->unsigned();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->unsignedInteger('featured_img')->unsigned();
        });


//
// NOTE -- business_metadata
// --------------------------------------------------

        Schema::create('business_metadata', function ($table) {
            $table->increments('id');
            $table->unsignedInteger('business_id')->unsigned();
            $table->string('phone', 20);
            $table->string('email', 100);
            $table->string('fax', 30);
            $table->string('fb_url', 150);
            $table->string('twitter_url', 150);
            $table->string('website', 150);
            $table->text('opening_hours');
            $table->text('opening_hour_hash');
            $table->boolean('good_for_groups');
            $table->boolean('water_service');
            $table->boolean('credit_card_accepted');
            $table->boolean('parking_valet');
            $table->unsignedInteger('price_low');
            $table->unsignedInteger('price_high');
            $table->string('price_currency', 10);
            $table->boolean('outdoor_seating');
            $table->boolean('good_for_kids');
            $table->string('good_for', 20);
            $table->boolean('take_reservation');
            $table->boolean('alcohol');
            $table->boolean('delivery');
            $table->boolean('to_go');
            $table->boolean('dogs_allowed');
            $table->string('brand_name', 100);
            $table->string('client_name', 100);
            $table->string('pro_ref', 40);
            $table->string('owner_pos', 100);
            $table->string('company_vini', 40);
            $table->string('share_capital', 30);
            $table->text('video_url');
            $table->string('youtube_url', 150);
            $table->string('gplus_url', 150);
            $table->string('linkedin_url', 150);
            $table->boolean('wheelchair_accessible');
            $table->boolean('important');
            $table->boolean('slider_featured');
        });


//
// NOTE -- businesses
// --------------------------------------------------

        Schema::create('businesses', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name', 100);
            $table->string('address', 150);
            $table->unsignedInteger('category_id');
            $table->string('city', 30);
            $table->unsignedInteger('city_id');
            $table->string('state', 30);
            $table->string('country', 30);
            $table->unsignedInteger('zip_code');
            $table->char('latitude', 20);
            $table->char('longitude', 20);
            $table->string('geohash', 100);
            $table->unsignedInteger('user_id')->unsigned();
            $table->string('listing_type', 20);
            $table->string('company_type', 20);
            $table->string('food_type', 20);
            $table->string('responsibility', 20);
            $table->string('tahiti_numero', 100);
            $table->string('tahiti_rc', 100);
            $table->text('description');
            $table->float('rating_score');
            $table->unsignedInteger('view_count');
            $table->unsignedInteger('review_count')->unsigned();
            $table->text('keywords');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->unsignedInteger('logo')->unsigned();
            $table->string('listing_status', 30)->default(Business::DRAFT);
        });


//
// NOTE -- categories
// --------------------------------------------------

        Schema::create('categories', function ($table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('slag', 30);
            $table->text('style');
            $table->unsignedInteger('business_count');
        });


//
// NOTE -- cities
// --------------------------------------------------

        Schema::create('cities', function ($table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('slag', 30);
            $table->unsignedInteger('business_count');
            $table->unsignedInteger('event_count');
        });


//
// NOTE -- comments
// --------------------------------------------------

        Schema::create('comments', function ($table) {
            $table->increments('id')->unsigned();
            $table->text('description');
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('target_id')->unsigned();
            $table->string('target_type', 100);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });


//
// NOTE -- compliments
// --------------------------------------------------

        Schema::create('compliments', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('target_id')->unsigned();
            $table->string('target_type', 20);
            $table->text('description');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });


//
// NOTE -- countries
// --------------------------------------------------

        Schema::create('countries', function ($table) {
            $table->increments('id');
            $table->string('name', 20);
            $table->unsignedInteger('business_count');
        });


//
// NOTE -- events
// --------------------------------------------------

        Schema::create('events', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('title', 150);
            $table->string('location', 100);
            $table->string('link', 200);
            $table->string('video_url', 200);
            $table->text('description');
            $table->unsignedInteger('user_id')->unsigned();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->dateTime('event_time');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('interested_count')->unsigned();
            $table->string('listing_status', 30)->default("draft");
            $table->string('latitude', 20);
            $table->string('longitude', 20);
            $table->string('geohash', 40);
        });


//
// NOTE -- form_options
// --------------------------------------------------

        Schema::create('form_options', function ($table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('type', 50);
        });


//
// NOTE -- friendships
// --------------------------------------------------

        Schema::create('friendships', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('friend_id')->unsigned();
            $table->boolean('status');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });


//
// NOTE -- messages
// --------------------------------------------------

        Schema::create('messages', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('sender_id')->unsigned();
            $table->unsignedInteger('receiver_id')->unsigned();
            $table->text('message');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->unsignedInteger('thread_id');
            $table->boolean('unread');
        });

//
// NOTE -- photos
// --------------------------------------------------

        Schema::create('photos', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('target_id')->unsigned();
            $table->string("type", 30)->default(Photo::TYPE_REGULAR);
            $table->text('target_type');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->text('caption');
            $table->string('relative_path', 200);
            $table->string('thumb_path', 200);
        });


//
// NOTE -- reviews
// --------------------------------------------------

        Schema::create('reviews', function ($table) {
            $table->increments('id');
            $table->float('rating');
            $table->text('description');
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('business_id')->unsigned();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->unsignedInteger('view_count');
            $table->unsignedInteger('useful_count');
            $table->unsignedInteger('funny_count');
            $table->unsignedInteger('likes_count');
        });


//
// NOTE -- roles
// --------------------------------------------------

        Schema::create('roles', function ($table) {
            $table->increments('id');
            $table->string("label", 60);
            $table->string('slag', 30);
        });


//
// NOTE -- states
// --------------------------------------------------

        Schema::create('states', function ($table) {
            $table->increments('id');
            $table->string('name', 20);
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('business_count');
        });


//
// NOTE -- threads
// --------------------------------------------------

        Schema::create('threads', function ($table) {
            $table->increments('id');
            $table->bigInteger('user1_id')->unsigned();
            $table->bigInteger('user2_id')->unsigned();
            $table->string('subject', 100);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->boolean('user1_deleted');
            $table->boolean('user2_deleted');
        });


//
// NOTE -- users
// --------------------------------------------------

        Schema::create('users', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('first_name', 40);
            $table->string('last_name', 40);
            $table->string('nickname', 40);
            $table->string('email', 80);
            $table->string('password', 100);
            $table->enum('gender', array('MALE', 'FEMALE'));
            $table->unsignedInteger('zip_code');
            $table->dateTime('birthdate');
            $table->text('profile_pic');
            $table->text('remember_token');
            $table->boolean('hide_name');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->string('email_status', 100);
            $table->text('about');
            $table->text('city_name');
            $table->char('role_id', 20)->default(Role::USER);
        });


//
// NOTE -- votes
// --------------------------------------------------

        Schema::create('votes', function ($table) {
            $table->increments('id')->unsigned();
            $table->unsignedInteger('user_id')->unsigned();
            $table->unsignedInteger('target_id')->unsigned();
            $table->string('target_type', 20);
            $table->string('type', 20);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });

    }

//
// NOTE - Revert the changes to the database.
// --------------------------------------------------

    public function down()
    {

        Schema::drop('bl_business');
        Schema::drop('blog_categories');
        Schema::drop('blog_comments');
        Schema::drop('blogs');
        Schema::drop('business_metadata');
        Schema::drop('businesses');
        Schema::drop('categories');
        Schema::drop('cities');
        Schema::drop('comments');
        Schema::drop('compliments');
        Schema::drop('countries');
        Schema::drop('events');
        Schema::drop('form_options');
        Schema::drop('friendships');
        Schema::drop('messages');
        Schema::drop('photos');
        Schema::drop('reviews');
        Schema::drop('roles');
        Schema::drop('states');
        Schema::drop('threads');
        Schema::drop('users');
        Schema::drop('votes');

    }
}