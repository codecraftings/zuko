<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Illuminate\Support\Facades\DB;
use Mockery\Generator\Generator;
use Zucko\Core\Address\City;
use Zucko\Core\Blog\Blog;
use Zucko\Core\Blog\Comment;
use Zucko\Core\Business\Business;
use Zucko\Core\Business\BusinessMeta;
use Zucko\Core\Business\Category;
use Zucko\Core\Event\Event;
use Zucko\Core\Inbox\Message;
use Zucko\Core\Inbox\Thread;
use Zucko\Core\Photo\Photo;
use Zucko\Core\Review\Review;
use Zucko\Core\User\Role;
use Zucko\Core\User\User;

function randomDemoImage($type = "bb"){
    if($type=="pp"){
        return "demo/pp-".mt_rand(1,7).".jpg";
    }
}
$factory->define(Zucko\Core\User\User::class, function ($faker) {
    return [
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName(),
        'nickname' => $faker->userName(),
        'email' => $faker->email(),
        'password' => str_random(10),
        'remember_token' => str_random(10),
        'gender' => $faker->randomElement(["MALE","FEMALE"]),
        'zip_code' => $faker->postcode(),
        'birthdate' => $faker->dateTimeBetween("-30 years"),
        'profile_pic' => randomDemoImage("pp"),
        "hide_name" => mt_rand(0,1),
        "email_status" => "verified",
        "about"  => $faker->realText(120),
        "city_name"=> $faker->city(),
        "role_id" => Role::USER
    ];
});
$factory->define(Comment::class, function($faker){
    return [
        "user_id" => randomID(User::class),
        "description" => $faker->paragraph(4),
        "created_at" => $faker->dateTime(),
    ];
});
$factory->define(Blog::class, function($faker){
    return [
        "title" => $faker->realText(100),
        "content" => implode("\n\r", $faker->paragraphs(5)),
        "user_id" => randomID(User::class),
        "category_id" => randomID(\Zucko\Core\Blog\Category::class),
        "created_at" => $faker->dateTime(),
        "featured_img" => mt_rand(1,10)
    ];
});
$factory->define(BusinessMeta::class, function($faker){
    return [
        "business_id" => randomID(Business::class),
        "phone" => $faker->phoneNumber(),
        "email" => $faker->email(),
        "price_low" => mt_rand(0, 3000),
        "price_high" => mt_rand(1500, 6000),
        "slider_featured" => mt_rand(0,1)
    ];
});

$factory->define(Business::class, function($faker){
    return [
        "name" => $faker->company(),
        "address" => $faker->address(),
        "category_id" => randomID(Category::class),
        "city"  => $faker->city(),
        "city_id" => randomID(City::class),
        "state" => $faker->state(),
        "country" => $faker->country(),
        "zip_code" => $faker->postcode(),
        "latitude" => $faker->latitude(),
        "longitude" => $faker->longitude(),
        "user_id" => randomID(User::class),
        "listing_type" => "place",
        "logo" => mt_rand(1, 10),
        "listing_status" => Business::PUBLISHED
    ];
});

$factory->define(City::class, function($faker){
    $city = $faker->city();
   return [
       "name" => $city,
       "slag" => make_slag($city)
   ];
});
function randomID($model){
    return $model::query()->orderByRaw("rand()")->first()->id;
}
$factory->define(\Zucko\Core\Comment\Comment::class, function($faker){
   return [
       "description" => $faker->realText(100),
       "user_id" => randomID(User::class),
       "target_id" => randomID(Review::class),
       "target_type" => Review::class,
       "created_at" => $faker->dateTime()
   ];
});

$factory->define(Event::class, function($faker){
    return [
       "title" => $faker->realText(100),
       "location" => $faker->address(),
       "link" => $faker->url(),
       "video_url" => $faker->url(),
       "description" => $faker->realText(300),
       "user_id" => randomID(User::class),
       "created_at" => $faker->dateTime(),
       "event_time" => $faker->dateTimeBetween("now", "2 months"),
       "city_id" => randomID(City::class),
       "listing_status" => Event::PUBLISHED,
       "latitude" => $faker->latitude(),
       "longitude" => $faker->longitude()
    ];
});

function getPath($type, $num, $ext="jpg"){
    return "demo/".$type."-".$num.".".$ext;
}
$factory->define(Photo::class, function($faker){
    $num = mt_rand(1, 24);
    return [
        "user_id" => randomID(User::class),
        "target_id" => randomID(Business::class),
        "target_type" => Business::class,
        "created_at" => $faker->dateTime(),
        "caption" => $faker->realText(100),
        "relative_path" => getPath("bb", $num),
        "thumb_path" => getPath("bb", $num),
    ];
});
$factory->defineAs(Photo::class, "logo", function($faker) use($factory){
   $base = $factory->raw(Photo::class);
    $num = mt_rand(1, 15);
    return array_merge($base, [
        "type" => Photo::TYPE_LOGO,
        "relative_path" => getPath("logo", $num, "png"),
        "thumb_path" => getPath("logo", $num, "png"),
    ]);
});
$factory->defineAs(Photo::class, "profile_pic", function($faker) use($factory){
    $base = $factory->raw(Photo::class);
    $num = mt_rand(1, 7);
    return array_merge($base, [
        "target_id"=> randomID(User::class),
        "target_type" => User::class,
        "type" => Photo::TYPE_PROFILE_PIC,
        "relative_path" => getPath("pp", $num),
        "thumb_path" => getPath("pp", $num),
    ]);
});
$factory->define(Review::class, function($faker){
    return [
        "rating" => mt_rand(2, 5),
        "description" => $faker->realText(250),
        "user_id" => randomID(User::class),
        "business_id" => randomID(Business::class),
        "created_at" => $faker->dateTime(),
        "likes_count" => mt_rand(0, 10)
    ];
});
$factory->define(Thread::class, function($faker){
    return [
        "user1_id" => randomID(User::class),
        "user2_id" => randomID(User::class),
        "created_at" => $faker->dateTime(),
        "user1_deleted" => mt_rand(0,1),
        "user2_deleted" => mt_rand(0,1),
        "subject" => $faker->realText(20)
    ];
});

$factory->define(Message::class, function($faker){
    return [
        "sender_id" => randomID(User::class),
        "receiver_id" => randomID(User::class),
        "message" => $faker->realText(200),
        "created_at" => $faker->dateTime(),
        "thread_id" => randomID(Thread::class),
        "unread" => mt_rand(0,1)
    ];
});