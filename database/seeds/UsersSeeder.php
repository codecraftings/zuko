<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Zucko\Core\User\User;

class UsersSeeder extends Seeder{
    public function run(){
        Model::unguard();
        DB::connection()->disableQueryLog();
        echo "creating users.. \n";
        factory(User::class, 10)->create();
        echo "10 users created\n";
    }
}