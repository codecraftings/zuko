<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Zucko\Core\Comment\Comment;
use Zucko\Core\Review\Review;

class ReviewsSeeder extends Seeder {
    public function run(){
        Model::unguard();
        DB::connection()->disableQueryLog();
        echo "creating reviews..\n";
        factory(Review::class, 10)->create()
            ->each(function ($review) {
                echo $review->id." ";
                factory(Comment::class, mt_rand(2, 5))
                    ->create([
                        "target_id" => $review->id
                    ]);
            });
        echo "\n50 reviews created\n";
    }
}