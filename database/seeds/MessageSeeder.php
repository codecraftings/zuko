<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Zucko\Core\Inbox\Message;
use Zucko\Core\Inbox\Thread;

class MessageSeeder extends Seeder {
    public function run(){
        Model::unguard();
        DB::connection()->disableQueryLog();
        echo "creating messages... ";
        factory(Thread::class, 5)->create()
            ->each(function ($thread) {
                $users = [$thread->user1_id, $thread->user2_id];
                for ($i = 0; $i < 5; $i ++) {
                    $msg = factory(Message::class)->create([
                        "sender_id"   => $users[mt_rand(0, 1)],
                        "receiver_id" => $users[mt_rand(0, 1)],
                        "thread_id"   => $thread->id
                    ]);
                    echo $msg->id." ";
                }
            });
        echo "\ncreated messages\n";
    }
}