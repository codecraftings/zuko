<?php

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Model::unguard();
        $this->emptyTables();
        DB::connection()->disableQueryLog();
        $this->call("SeedDefaultData");
//        $this->call("SeedDemoData");
        if (Config::get("app.debug")) {
        }

        Model::reguard();
    }
    private function emptyTables(){
        $tables = DB::select("SHOW TABLES");
        $Tables_in_database = "Tables_in_".Config::get("database.connections.mysql.database");
        echo "truncating table...";
        foreach($tables as $table){
            if($table->$Tables_in_database !=="migrations"){
                echo "truncating table ".$table->$Tables_in_database;
                DB::table($table->$Tables_in_database)->truncate();
            }
        }
    }
}
