<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Zucko\Core\Address\City;
use Zucko\Core\User\User;

class CitiesSeeder extends Seeder{
    public function run(){
        Model::unguard();
        DB::connection()->disableQueryLog();
        echo "creating cities.. \n";
        factory(City::class, 10)->create();
        echo "10 cities created\n";
    }
}