<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Zucko\Core\User\Role;
use Zucko\Core\User\User;

class SeedDefaultData extends Seeder
{
    /**
     *
     */
    public function run()
    {
        Model::unguard();
        DB::connection()->disableQueryLog();
        $this->loadDBBusinesses();
        $this->loadBlogCategories();
        $this->loadBusinessCategories();
        $this->loadStates();
        $this->loadCountries();
        $this->loadRoles();
        $this->createAdmin();
    }

    private function loadDBBusinesses()
    {
        $importer = new Zucko\Core\Business\ImportFromCsv('first.csv', App::make('Zucko\Core\Address\Repos\AddressRepo'), App::make('Zucko\Core\Business\Repos\BusinessRepo'));
        $importer->startImport();
    }

    private function executeSql($filename)
    {
        $sql = file_get_contents(base_path("resources/db/" . $filename));
        DB::unprepared($sql);
    }

    private function loadBlogCategories()
    {
        $this->executeSql("blog_categories.sql");
    }

    private function loadBusinessCategories()
    {
        $this->executeSql("categories.sql");
    }

    private function loadStates()
    {
        $this->executeSql("states.sql");
    }

    private function loadCountries()
    {
        $this->executeSql("countries.sql");
    }

    private function loadRoles()
    {
        Role::create([
            "slag"  => Role::USER,
            "label" => "user"
        ]);
        Role::create([
            "slag"  => Role::ADMIN,
            "label" => "admin"
        ]);
    }

    private function createAdmin()
    {
        factory(User::class)->create([
            "email" => "admin@zuckoo.com",
            "password" => Hash::make("bangla"),
            "role_id" => Role::ADMIN
        ]);
    }
}