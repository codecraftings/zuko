<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Zucko\Core\Address\City;
use Zucko\Core\Blog\Blog;
use Zucko\Core\Business\Business;
use Zucko\Core\Business\BusinessMeta;
use Zucko\Core\Comment\Comment;
use Zucko\Core\Event\Event;
use Zucko\Core\Inbox\Message;
use Zucko\Core\Inbox\Thread;
use Zucko\Core\Photo\Photo;
use Zucko\Core\Review\Review;
use Zucko\Core\User\User;

class SeedDemoData extends Seeder
{
    public function run()
    {
        Model::unguard();
        DB::connection()->disableQueryLog();
        $this->createDemoUsers();
        $this->createMessages();
        $this->createDemoCities();
        $this->createDemoEvents();
        $this->createDemoBusinesses();
        $this->createDemoBlogs();
        $this->createReviews();
    }

    private function createDemoUsers()
    {
        $this->call('UsersSeeder');

    }

    private function createDemoCities()
    {
        $this->call('CitiesSeeder');
    }

    private function createDemoBusinesses()
    {
        $this->call('BusinessSeeder');
    }

    private function createReviews()
    {
        $this->call('ReviewsSeeder');
    }

    private function createDemoEvents()
    {
        $this->call('EventsSeeder');
    }

    private function createDemoBlogs()
    {
        $this->call('BlogsSeeder');
    }

    private function createMessages()
    {
        $this->call('MessageSeeder');
    }
}