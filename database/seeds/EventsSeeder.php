<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Zucko\Core\Event\Event;

class EventsSeeder extends Seeder{
    public function run(){
        Model::unguard();
        DB::connection()->disableQueryLog();
        echo "creating events...\n";
        factory(Event::class, 30)->create()
            ->each(function ($event) {
                echo $event->id." ";
                $city = $event->city;
                $city->event_count += 1;
                $city->save();
            });
        echo "\n30 events created!\n";
    }
}