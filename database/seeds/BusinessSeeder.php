<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Zucko\Core\Business\Business;
use Zucko\Core\Business\BusinessMeta;
use Zucko\Core\Photo\Photo;

class BusinessSeeder extends Seeder {
    public function run(){
        Model::unguard();
        DB::connection()->disableQueryLog();
        echo "creating business \n";
        factory(Business::class, 10)->create()
            ->each(function ($business, $key) {
                echo ($key + 1) . " ";
                $logo = factory(Photo::class, "logo")->create([
                    "target_id" => $business->id
                ]);
                $business->logo = $logo->id;
                $business->save();
                factory(BusinessMeta::class)->create([
                    "business_id" => $business->id
                ]);
                factory(Photo::class, mt_rand(5, 10))->create([
                    "target_id"   => $business->id,
                    "target_type" => Business::class,
                    "user_id"     => $business->user->id
                ]);
            });
        echo "\nbusinesses added!\n";
    }
}