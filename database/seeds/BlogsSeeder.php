<?php


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Zucko\Core\Blog\Blog;

class BlogsSeeder extends Seeder{
    public function run(){
        Model::unguard();
        DB::connection()->disableQueryLog();
        echo "creating blogs...\n";
        factory(Blog::class, 10)->create()
            ->each(function ($blog) {
                echo $blog->id." ";
                factory(\Zucko\Core\Blog\Comment::class, mt_rand(2, 10))
                    ->create([
                        "blog_id" => $blog->id
                    ])
                    ->each(function ($comment) use ($blog) {
                        factory(\Zucko\Core\Blog\Comment::class, mt_rand(2, 5))
                            ->create([
                                "blog_id"   => $blog->id,
                                "parent_id" => $comment->id
                            ]);
                    });
            });
        echo "\n20 blogs created!\n";
    }
}