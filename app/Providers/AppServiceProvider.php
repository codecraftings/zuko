<?php

namespace Zucko\Providers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \Blade::setRawTags('{{', '}}');
        \Blade::setContentTags('{{{', '}}}');
        \Blade::setEscapedContentTags('{{{', '}}}');
        $this->app->bind('Zucko\Core\User\Repos\UserRepoInterface','Zucko\Core\User\Repos\UserRepo');
        $this->app->bind('Zucko\Core\User\Repos\FriendsRepoInterface','Zucko\Core\User\Repos\FriendsRepo');
        \Route::matched(function($route, Request $request){
            if($request->has("locale")){
                $locale = $request->get("locale");
                \Cookie::queue("locale", $locale, 60*24*30);
            }
            else if($request->hasCookie("locale")){
                $locale = \Cookie::get("locale");
            }
            else{
                $locale = $request->getLocale();
            }
            \App::setLocale($locale);
            setlocale(LC_TIME, $locale);
        });
    }
}
