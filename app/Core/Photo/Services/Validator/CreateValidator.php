<?php
namespace Zucko\Core\Photo\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class CreateValidator extends LaravelValidator{
	protected $rules = array(
		'user_id' => 'required|exists:users,id',
		'target_id' => 'required',
		'target_type' => 'required|in:Zucko\Core\Event\Event,Zucko\Core\Business\Business,Zucko\Core\Blog\Blog,Zucko\Core\User\User',
		'caption' => '',
		'relative_path' => 'required',
		'thumb_path' => 'required'
		);
}