<?php
namespace Zucko\Core\Photo\Repos;
use Zucko\Core\Photo\Photo;
use Zucko\Core\Photo\Services\Validator\CreateValidator;
use Zucko\Core\Services\File\Uploader;
use Zucko\Core\Services\File\ImageEditor;
use Illuminate\Support\MessageBag;
class PhotoRepo{
	protected $photo;
	public $create_validator;
	public $errors;
	public $uploader;
	public $editor;
	public function __construct(Photo $photo, ImageEditor $editor, MessageBag $errors, Uploader $uploader, CreateValidator $create_validator){
		$this->photo = $photo;
		$this->uploader = $uploader;
		$this->create_validator = $create_validator;
		$this->editor = $editor;
		$this->errors = $errors;
	}
	public function get($id){
		if($id instanceof Photo){
			return $id;
		}
		return $this->photo->find($id);
	}
	public function createFromUpload($input_files, $target_id, $target_type, $user_id, $caption="", $maxW = null, $maxH=null){
		$this->uploader->setFiles($input_files);
		$this->uploader->acceptUploadImages();
		$files = $this->uploader->getMovedFiles();
		if(!is_array($files)){
			$this->errors->add('file', $this->errors()->first()?:"Something went wrong");
			return false;
		}
		//dd($files);
		foreach ($files as $key => &$file) {
			if($maxW||$maxH){
				$this->editor->ensureMaxSize($file, $maxW, $maxH);
			}
			$thumb = $this->editor->generateThumb($file);
			$photo = $this->create(array(
					'user_id' => $user_id,
					'target_id' => $target_id,
					'target_type' => $target_type,
					'caption' => $caption,
					'relative_path' => $file->getPublicPath(),
					'thumb_path' => $thumb->getPublicPath()
				));
			if(!$photo){
				return false;
			}
			$file = $photo;
		}
		return $files;
	}
	public function create(array $data){
		$this->create_validator->with($data);
		if(!$this->create_validator->passes()){
			$this->errors = $this->create_validator->errors();
			return false;
		}
		$photo = $this->photo->create($data);
		return $photo;
	}
	public function update($photo , $data){
		$photo = $this->get($photo);
		if($photo){
			$photo->update($data);
			return $photo;
		}
	}
	public function errors(){
		return $this->errors;
	}
}