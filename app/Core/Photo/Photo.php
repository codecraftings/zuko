<?php
namespace Zucko\Core\Photo;
/*
id
user_id
target_id
target_type
type
created_at
updated_at
caption
relative_path
thumb_path
*/
use Zucko\Core\BaseModel as Base;
class Photo extends Base{
	const TYPE_REGULAR = "regular";
	const TYPE_LOGO = "logo";
	const TYPE_PROFILE_PIC = "profile_pic";
	protected $table = "photos";
	protected $fillable = array('user_id','target_id','relative_path','thumb_path','target_type','caption', 'type');
	protected $appends = array('url', 'thumb_url');
	public function getThumbUrlAttribute(){
		return url($this->thumb_path);
	}
	public function target(){
		return $this->morphTo();
	}
	public function getUrlAttribute(){
		return url($this->relative_path);
	}
}