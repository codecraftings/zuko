<?php
namespace Zucko\Core\Review\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class UpdateValidator extends LaravelValidator{
	protected $rules = array(
			'rating' => 'sometimes|required|numeric|max:5',
			'description' => 'sometimes|required|min:10',
			'user_id' => 'sometimes|required|exists:users,id',
			'business_id' => 'sometimes|required|exists:businesses,id'
		);
}