<?php
namespace Zucko\Core\Review\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class VoteCreateValidator extends LaravelValidator{
	protected $rules = array(
			'type' => 'required|in:interested,likes,useful,useful,funny',
			'target_id' => 'required|integer',
			'target_type' => 'required|in:Zucko\Core\Review\Review,Zucko\Core\Event\Event',
			'user_id' => 'required|exists:users,id'
		);
}