<?php
namespace Zucko\Core\Review\Repos;
use Zucko\Core\Review\Review;
use Zucko\Core\Review\Vote;
use Zucko\Core\Review\Services\Validator\CreateValidator;
use Zucko\Core\Review\Services\Validator\UpdateValidator;
use Zucko\Core\Business\Repos\BusinessRepo;
use Illuminate\Support\MessageBag;
use Zucko\Core\Services\Search\PaginatorFactory;

class ReviewRepo{
	protected $review;
	protected $vote;
	protected $create_validator;
	protected $update_validator;
	protected $errors;
	protected $business;
	protected $paginator;
	public function __construct(PaginatorFactory $paginator, Review $review, BusinessRepo $business, Vote $vote, CreateValidator $create_validator, UpdateValidator $update_validator, MessageBag $bag){
		$this->review = $review;
		$this->paginator = $paginator;
		$this->business = $business;
		$this->vote = $vote;
		$this->create_validator = $create_validator;
		$this->update_validator = $update_validator;
		$this->errors = $bag;
	}
	public function get($id){
		if($id instanceof Review){
			return $id;
		}
		return $this->review->find($id);
	}
	public function getRecents($num=10){
		return $this->review->query()->orderBy('id','desc')->take($num)->get();
	}
	public function getTop($num=10){
		return $this->review->query()->orderByRaw('rand()')->take($num)->get();
	}
	public function alreadyReviewed($business_id, $user_id){
		$q = $this->review->where('business_id','=',$business_id)->where('user_id','=',$user_id);
		if($q->count()>0){
			return $q->first();
		}
		return false;
	}
	public function search($str, $filters, $pagging, $page){
		$q = $this->review->query();
		if(!empty($search)){
			$q->where('description','like','%'.$search.'%');
		}
//		$total = $q->count();
		if(isset($filters['sort'])){
			switch ($filters['sort']) {
				case 'updated':
				$q->orderBy('updated_at','desc');
				break;
				case 'old':
				$q->orderBy('created_at','asc');
				break;
				default:
				$q->orderBy('id','desc');
				break;
			}
		}
//		$items = $q->skip(($page-1)*$pagging)->take($pagging)->get()->all();
//		$this->paginator->setCurrentPage($page);
		return $this->paginator->make($q, $pagging, $page);
	}
	public function create(array $data){
		$this->create_validator->with($data);
		if(!$this->create_validator->passes()){
			$this->errors = $this->create_validator->errors();
			return false;
		}
		if($this->review->where('business_id','=',$data['business_id'])->where('user_id','=',$data['user_id'])->count()>1){
			$this->errors->add('review','You already Reviewed This Business. Please edit that review if you want to add something.');
			return false;
		}
		$review = $this->review->create($data);
		$business = $this->business->get($data['business_id']);
		if(!$this->business->update($business, array(
				"rating_score" => $business->reviews()->avg('rating'),
				"review_count" => $business->reviews()->count()
			))){
			$this->errors = $this->business->errors();
			return false;
		}
		return $review;
	}
	public function update($review, $data){
		$review = $this->get($review);
		if(!$review){
			$this->errors()->add("review",'This review does not exist!');
			return false;
		}
		if($data instanceof Review){
			if($data->id==$review->id){
				$data->update();
			}
			return $data;
		}
		$this->update_validator->with($data);
		if(!$this->update_validator->passes()){
			$this->errors = $this->update_validator->errors();
			return false;
		}
		$review = $review->update($data);
		return $review;
	}
	public function delete($review){
		$review = $this->get($review);
		if(!$review){
			$this->errors()->add("review",'This review does not exist!');
			return false;
		}
		return $review->delete();
	}
	public function alreadyLiked($review, $user_id){
		if($review->likes()->where('user_id',$user_id)->count()>0){
			return true;
		}else{
			return false;
		}
	}
	public function likeReview($review, $user_id){
		$review = $this->get($review);
		if(!$review){
			return false;
		}
		if($this->alreadyLiked($review, $user_id)){
			return false;
		}
		$like = $this->vote->create(array(
				'user_id' => $user_id,
				'target_id' => $review->id,
				'target_type' => 'Zucko\Core\Review\Review',
				'type' => 'likes'
			));
		return $review->likes()->count();
	}
	public function errors(){
		return $this->errors;
	}
}