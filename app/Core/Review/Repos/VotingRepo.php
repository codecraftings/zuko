<?php
namespace Zucko\Core\Review\Repos;
use Illuminate\Support\MessageBag;
use Zucko\Core\Review\Vote;
use Zucko\Core\Services\Api\API_ERROR;
use Zucko\Core\Review\Services\Validator\VoteCreateValidator;
class VotingRepo{
	protected $errors;
	protected $vote;
	protected $event_class;
	protected $create_validator;
	public function __construct(MessageBag $bag, Vote $vote, VoteCreateValidator $create_validator){
		$this->errors = $bag;
		$this->vote = $vote;
		$this->create_validator = $create_validator;
		$this->event_class = 'Zucko\Core\Event\Event';
	}
	public function alreadyInterested($event_id, $user_id){
		$q = $this->vote->where('type','=','interested')->where('target_type','=',$this->event_class)->where('target_id','=',$event_id)->where('user_id','=',$user_id);
		return $q->count()>0;
	}
	public function addInterested($event_id, $user_id){
		if($this->alreadyInterested($event_id, $user_id)){
			$this->errors->add('event', API_ERROR::msg(API_ERROR::UNKNOWN_ERROR));
			return false;
		}
		$data = array(
				'type' => 'interested',
				'target_id' => $event_id,
				'target_type' => $this->event_class,
				'user_id' => $user_id
			);
		$vote = $this->vote->create($data);
		return true;
	}
	public function get($vote){
		if($vote instanceof Vote){
			return $vote;
		}
		return $this->vote->find($vote);
	}
	public function getByOwners($user_id, $target_id, $target_type, $type){
		$q = $this->vote->where(compact('user_id','target_id','target_type', 'type'));
		if($q->count()<1){
			return false;
		}
		return $q->first();
	}
	public function create(array $data){
		$this->create_validator->with($data);
		if($this->create_validator->fails()){
			$this->errors = $this->create_validator->errors();
			return false;
		}
		$vote = $this->vote->create($data);
		return $vote;
	}
	public function delete($vote){
		$vote = $this->get($vote);
		$vote->delete();
	}
	public function errors(){
		return $this->errors;
	}
}