<?php
namespace Zucko\Core\User\Repos;
interface FriendsRepoInterface{
	public function get($user_id);
	public function getInvitations($user_id);
	public function addFriend($user_id,$friend_id);
	public function acceptFriend($user_id, $friend_id);
	public function removeFriend($user_id, $friend_id);
	public function errors();
}