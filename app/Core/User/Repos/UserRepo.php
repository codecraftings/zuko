<?php
namespace Zucko\Core\User\Repos;
use Zucko\Core\Services\Search\PaginatorFactory;
use Zucko\Core\User\Services\Validator\UserCreateValidator;
use Zucko\Core\User\Services\Validator\UserUpdateValidator;
use Zucko\Core\User\User;
use Zucko\Core\User\Role;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use Illuminate\Support\MessageBag;
class UserRepo{
	protected $errors;
	protected $create_validator;
	protected $update_validator;
	protected $user;
	protected $hash;
	protected $paginator;
	public function __construct(PaginatorFactory $paginator, UserCreateValidator $create_validator,UserUpdateValidator $update_validator, User $user, Hash $hash, MessageBag $bag){
		$this->create_validator = $create_validator;
		$this->update_validator = $update_validator;
		$this->hash = $hash;
		$this->user = $user;
		$this->errors = $bag;
		$this->paginator = $paginator;
	}
	public function get($user){
		if($user instanceof User){
			return $user;
		}
		$user = $this->user->find($user);
		if(!$user){
			$this->errors->add("user","User doesn't exists");
			return false;
		}
		return $user;
	}
	public function search($name, $filters, $pagging=10, $page=1){
		$q = $this->user->query();
		if(!empty($name)){
			$q = $q->where(function($q) use ($name){
				$q->where('first_name','like','%'.$name.'%')
					->orWhere('last_name','like','%'.$name.'%')
					->orWhere('nickname','like','%'.$name.'%');
			});
		}
		if(isset($filters['city_name'])&&$filters['city_name']){
			$q->where('city_name','like','%'.$filters['city_name'].'%');
		}
		if(isset($filters['sort'])){
			switch ($filters['sort']) {
				case 'updated':
					$q->orderBy('updated_at','desc');
					break;
				case 'old':
					$q->orderBy('created_at','asc');
					break;
				default:
					$q->orderBy('id','desc');
					break;
			}
		}
//		$items = $q->skip(($page-1)*$pagging)->take($pagging)->get()->all();
//		$this->paginator->setCurrentPage($page);
		return $this->paginator->make($q, $pagging, $page);
	}
	public function getByNick($nick){
		$user = $this->user->where('nickname','=',$nick);
		if($user->count()<1){
			$this->errors->add('user', "No account associated with this nickname yet!");
			return false;
		}
		return $user->first();
	}
	public function getByEmail($email){
		$user = $this->user->where('email','=',$email);
		if($user->count()<1){
			$this->errors->add('user', "No account associated with this email yet!");
			return false;
		}
		return $user->first();
	}
	public function create(array $data){
		$this->create_validator->with($data);
		if(!$this->create_validator->passes()){
			$this->errors = $this->create_validator->errors();
			return false;
		}
		if(isset($data['password'])){
			if($this->hash->needsRehash($data['password']))
			$data['password'] = $this->hash->make($data['password']);
		}
		$user = $this->user->create($data);
		//$user->email_status = "verified";
		$user->email_status = str_random(30);
		$user->save();
		return $user;
	}
	public function changePassword($user, $newpassword){
		$user = $this->get($user);
		$user->password = $this->hash->make($newpassword);
		$user->save();
		return $user;
	}
	public function changeRole($user, $role_id){
		$user = $this->get($user);
		$user->role_id = $role_id;
		$user->save();
		return $user;
	}
	public function update($user, array $data){
		$user = $this->get($user);
		if(!$user){
			return false;
		}
		//dd($data);
		if(isset($data['email'])){
			if($data['email']==$user->email){
				unset($data['email']);
			}
		}
		if(isset($data['nickname'])){
			if($data['nickname']==$user->nickname){
				unset($data['nickname']);
			}
		}
		if(isset($data['password'])&&empty($data['password'])){
			unset($data['password']);
		}
		$this->update_validator->with($data);
		if(!$this->update_validator->passes()){
			$this->errors = $this->update_validator->errors();
			return false;
		}
		if(isset($data['password'])&&!empty($data['password'])){
			$oldpass = $data['oldpassword'];
			if($this->hash->check($oldpass, $user->password)){
				if($this->hash->needsRehash($data['password']))
				$data['password'] = $this->hash->make($data['password']);
			}else{
				$this->errors->add("password","Old password mismatch");
				return false;
			}
		}
		$user->update($data);
		return $user;
	}
	public function delete($user){
		$user = $this->get($user);
		if(!$user){
			return false;
		}
		return $user->delete();
	}
	public function verifyEmail($email, $token){
		$user = $this->getByEmail($email);
		if(!$user){
			$this->errors->add('email','No account associated with this email');
			return false;
		}
		if($user->email_verified()){
			$this->errors->add('email','Email already verified');
			return false;
		}
		if($user->email_status!==$token){
			$this->errors->add("passreset", "Invalid token");
			return false;
		}else{
			$user->email_status = "verified";
			$user->save();
			return $user;
		}
	}
	public function errors(){
		return $this->errors;
	}
}