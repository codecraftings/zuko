<?php
namespace Zucko\Core\User\Repos;
interface UserRepoInterface{
	public function get($id);
	public function search($q, $filter);
	public function getByEmail($email);
	public function create(array $data);
	public function changePassword($user, $newpassword);
	public function update($user, array $data);
	public function verifyEmail($email, $token);
	public function errors();
}