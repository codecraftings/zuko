<?php
namespace Zucko\Core\User;
/*
id
user_id
friend_id
status boolean false
created_at
updated_at
*/
use Zucko\Core\BaseModel as Base;
class Friendship extends Base{
	protected $table = "friendships";
	protected $fillable = array('user_id','friend_id','status');
	public function friend(){
		$this->belongsTo('Zucko\Core\User\User','friend_id');
	}
}