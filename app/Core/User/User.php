<?php
namespace Zucko\Core\User;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Laracasts\Presenter\PresentableTrait;
/*
* @property string id
* @property string first_name
* @property string last_name
* @property string nickname
* @property string hide_name--- boolean false
* @property string email
* @property string password
* @property string gender enam(MALE, FEMALE)
* @property string zip_code
* @property string email_status
* @property DateTime birthdate
* @property string profile_pic
* @property string remember_token
* @property string created_at
* @property string updated_at
* @property string about
* @property string city_name
*/
use Zucko\Core\BaseModel as Base;
class User extends Base implements Authenticatable, CanResetPassword {

	use \Illuminate\Auth\Authenticatable, \Illuminate\Auth\Passwords\CanResetPassword;
	use PresentableTrait;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $presenter = 'Zucko\Core\User\Presenters\UserPresenter';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $fillable = array('first_name','last_name','nickname','hide_name','city_name','email','password','gender','about','zip_code','birthdate','profile_pic');
	protected $appends = array('profile_url');
	public function getProfileUrlAttribute(){
		return url('user/'.$this->id.'/'.$this->nickname);
	}
	public function getProfilePicAttribute(){
		$pp = $this->attributes['profile_pic'];
		if(empty($pp)){
			return asset_url('img/avatar.jpg');
		}
		return asset_url($pp);
	}
	public function email_verified(){
		return $this->email_status=="verified";
	}
	public function role(){
		return $this->belongsTo('Zucko\Core\User\Role', "slag");
	}
	public function hasRole($role_id){
		if($this->role_id==="admin"){
			return true;
		}
		if($this->role_id==$role_id){
			return true;
		}
		return false;
	}
	public function businesses(){
		return $this->hasMany('Zucko\Core\Business\Business');
	}
	public function blogs(){
		return $this->hasMany('Zucko\Core\Blog\Blog');
	}
	public function comments(){
		return $this->hasMany('Zucko\Core\Comment\Comment');
	}
	public function blog_comments(){
		return $this->hasMany('Zucko\Core\Blog\Comment');
	}
	public function events(){
		return $this->hasMany('Zucko\Core\Event\Event');
	}
	public function reviews(){
		return $this->hasMany('Zucko\Core\Review\Review');
	}
	public function photos(){
		return $this->hasMany('Zucko\Core\Photo\Photo');
	}
	public function compliments_given(){
		return $this->hasMany('Zucko\Core\Review\Compliment');
	}
	public function compliments_received(){
		return $this->morphMany('Zucko\Core\Review\Compliment','target');
	}
	public function votes_given(){
		return $this->hasMany('Zucko\Core\Review\Vote');
	}
	public function likes_given(){
		return $this->hasMany('Zucko\Core\Review\Vote')->where('type','=','like');
	}
	public function messages_received(){
		return $this->hasMany('Zucko\Core\Inbox\Message','receiver_id');
	}
	public function messages_sent(){
		return $this->hasMany('Zucko\Core\Inbox\Message','sender_id');
	}
	public function is_friend($user_id){
		return $this->all_friends()->where('friend_id','=',$user_id)->count()>0;
	}
	public function all_friends(){
		return $this->belongsToMany('Zucko\Core\User\User','friendships','user_id','friend_id');
	}
	public function friends(){
		return $this->belongsToMany('Zucko\Core\User\User','friendships','user_id','friend_id')->wherePivot('status','=',1);
	}
	public function invitations(){
		return $this->belongsToMany('Zucko\Core\User\User','friendships','user_id','friend_id')->wherePivot('status','=',0);
	}
	public function getDates(){
	    return ['birthdate', 'created_at', 'updated_at'];
	}
}
