<?php
namespace Zucko\Core\User;
use Zucko\Core\BaseModel as Base;
/*
 * @property integer id
 * @property string label
 * @property string slag
 */
class Role extends Base{
	const USER = "user";
	const ADMIN = "admin";
	protected $table = 'roles';
	public $timestamps = false;
}