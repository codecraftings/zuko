<?php
namespace Zucko\Core\User\Services\Session;
use Zucko\Core\User\Repos\UserRepo;
use Zucko\Core\User\Repos\FriendsRepo;
use Zucko\Core\User\User;
use Illuminate\Auth\AuthManager as Auth;
use Illuminate\Contracts\Auth\PasswordBroker as Password;
use Illuminate\Support\MessageBag;
class SessionService{
	protected $user_repo;
	protected $auth;
	protected $password;
	protected $error;
	public function __construct(Auth $auth, Password $password, UserRepo $user_repo, MessageBag $bag){
		$this->user_repo = $user_repo;
		$this->auth = $auth;
		$this->password = $password;
		$this->errors = $bag;
	}
	public function login($email, $password){
		if(!$this->auth->attempt(array('email'=>$email,'password'=>$password,'email_status'=>'verified'),true)){
			$this->errors->add('login',"Invalid Credentials");
			return false;
		}
		return true;
	}
	public function isLoggedIn(){
		return $this->auth->check();
	}
	public function getCurrentUser(){
		return $this->auth->user();
	}
	public function logout(){
		return $this->auth->logout();
	}
	public function requestPassReset($email){
		switch ($response = $this->password->remind(compact('email'), function($message){
			$message->subject('Request of new password on Zuckoo');
		}))
		{
			case Password::INVALID_USER:
				$this->errors->add('user', "User doesn't exists.");
				return false;

			case Password::REMINDER_SENT:
				return \Lang::get($response);
		}
	}
	public function confirmPassReset($email, $password, $password_confirmation, $token){
		$credentials = compact(
			'email', 'password', 'password_confirmation', 'token'
		);

		$response = $this->password->reset($credentials, function($user, $password)
		{
			$this->user_repo->changePassword($user, $password);
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				$this->errors->add('password', \Lang::get($response));
				return false;

			case Password::PASSWORD_RESET:
				return true;
		}
	}
	public function errors(){
		return $this->errors;
	}
}