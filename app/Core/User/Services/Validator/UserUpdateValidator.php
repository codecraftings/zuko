<?php
namespace Zucko\Core\User\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class UserUpdateValidator extends LaravelValidator{
	protected $rules = array(
			'first_name' => 'sometimes|required|max:20',
			'last_name' => 'sometimes|required|max:20',
			'nickname' => 'sometimes|required|min:5|max:20|alpha_dash|unique:users,nickname',
			'hide_name' => 'boolean',
			'email' => 'sometimes|required|min:5|max:50|email|unique:users,email',
			'password' => 'sometimes|required|min:6|max:20|confirmed',
			'gender' => 'sometimes|required|in:MALE,FEMALE',
			'birthdate' => 'sometimes|required|date',
			'profile_pic' =>'url'
		);
}