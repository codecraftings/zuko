<?php
namespace Zucko\Core\Address;
/*
id
name
country_id
business_count
*/
use Zucko\Core\BaseModel as Base;
class State extends Base{
	public $timestamps = false;
	protected $table = "states";
	protected $fillable = array('name');
	
}