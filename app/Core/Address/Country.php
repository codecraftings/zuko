<?php
namespace Zucko\Core\Address;
/*
id
name
business_count
*/
use Zucko\Core\BaseModel as Base;

class Country extends Base{
	public $timestamps = false;
	protected $table = "countries";
	protected $fillable = array('name');

}