<?php
namespace Zucko\Core\Address\Repos;
use Zucko\Core\Address\City;
use Zucko\Core\Address\Country;
use Zucko\Core\Address\State;
use Zucko\Core\BaseModel;

class AddressRepo implements AddressRepoInterface{
	protected $city;
	protected $country;
	protected $state;
	public function __construct(City $city, Country $country, State $state){
		$this->city = $city;
		$this->country = $country;
		$this->state = $state;
	}
	public function getAllCountries(){
		return $this->country->all();
	}
	public function getAllStates(){
		return $this->state->all();
	}
	public function getStatesByName($name){
		$state = $this->state->whereRaw('soundex(`name`) like soundex(?)', array($name));
		return $state->get();
	}
	public function addState($name){
		$state = $this->state->create(array('name'=>$name));
		return $state;
	}
	public function getCityByName($name){
		$city = $this->city->where('name','=',ucwords($name));
		if($city->count()<1){
			return false;
		}
		return $city->first();
	}
	public function getCityBySlag($slag){
		$city = $this->city->where('slag','=',$slag);
		return $city->first();
	}
	public function getCitiesByName($name){
		$city = $this->city->whereRaw('soundex(`name`) like soundex(?)', array($name));
		return $city->get();
	}
	public function saveCity($name){
		$city = $this->city->create(array('name'=>ucwords($name), 'slag'=>make_slag($name)));
		return $city;
	}
	public function updateCity($city_id, $newName=""){
		$city = $this->city->find($city_id);
		$city->business_count = $city->businesses()->count();
		$city->event_count = $city->events()->where('listing_status','=', BaseModel::PUBLISHED)->count();
		if($newName){
			$city->name = ucwords($newName);
			$city->slag = make_slag($newName);
		}
		return $city->save();
	}
	public function getPopularCities($len=10){
		$cities = $this->city->orderBy('event_count','desc');
		return $cities->take($len)->get();
	}
}