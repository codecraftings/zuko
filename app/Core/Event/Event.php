<?php
namespace Zucko\Core\Event;
/*
id
title
location
link
video_url
description
user_id
event_time
interested_count
city_id
listing_status
created_at
updated_at
latitude
longitude
*/
use Zucko\Core\BaseModel as Base;
use Laracasts\Presenter\PresentableTrait;
class Event extends Base{
	use PresentableTrait;
	protected $presenter = "Zucko\Core\Event\Presenters\EventPresenter";
	protected $table = "events";
	protected $fillable = array('title','location','city_id','link','video_url','description','user_id','event_time','latitude','longitude');
	protected $appends = array('profile_url','embedded_video','voted_interested');
	private $_voted_interested = false;
	private $_interaction_calculated = false;
	public function getVotedInterestedAttribute(){
		return $this->_voted_interested;
	}
	public function withUserInteractions($user){
		if(!$user||$this->_interaction_calculated){
			return $this;
		}
		if($this->votes()->where(array('type'=>'interested','user_id'=>$user->id))->count()>0){
			$this->_voted_interested = true;
		}
		$_interaction_calculated = true;
		return $this;
	}
	public function getEmbeddedVideoAttribute(){
		$parts = array();
		$video_id = 0;
		preg_match('/\?v=([^&]*)/', $this->attributes['video_url'], $parts);
		if(count($parts)>1){
		    $video_id = $parts[1];
		}else{
			return false;
		}
		return "http://www.youtube.com/embed/".$video_id."?autohide=1&showinfo=0&rel=0";
	}
	public function getProfileUrlAttribute(){
		return url('event/'.$this->id.'/'.urlencode(str_replace(array('"',"'","/","\\"), "",$this->title)));
	}
	public function published(){
		return $this->query()->where('listing_status','=',Base::PUBLISHED);
	}
	public function is_published(){
		return $this->listing_status==Base::PUBLISHED;
	}
	public function city(){
		return $this->belongsTo('Zucko\Core\Address\City');
	}
	public function user(){
		return $this->belongsTo('Zucko\Core\User\User');
	}
	public function photos(){
		return $this->morphMany('Zucko\Core\Photo\Photo', 'target');
	}
	public function getDates(){
		return ['created_at','updated_at','event_time'];
	}
	public function votes(){
		return $this->morphMany('Zucko\Core\Review\Vote','target');
	}
	public function interested(){
		return $this->votes()->where('type','=','interested');
	}
}