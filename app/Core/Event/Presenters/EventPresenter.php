<?php
namespace Zucko\Core\Event\Presenters;
use Laracasts\Presenter\Presenter;
class EventPresenter extends Presenter{
	public function show_title($len=30){
		$title = $this->title;
		if(strlen($title)>$len){
			$title = e(substr($title, 0, $len));
			$title .='...';
		}
		return $title;
	}
	public function desc($len=1000, $end="more"){
		$desc = $this->description;
		if($len<400){
			$desc = strip_tags($desc);
		}
		if(strlen($desc)>$len){
			$desc = e(substr($desc, 0, $len));
			if($end=="more"){	
			$desc .='... <a href="'.$this->profile_url.'">read more</a>';
			}else{
				$desc .= $end;
			}
		}
		return $desc;
	}
	public function city_name(){
		if($this->city){
			return $this->city->name;
		}
		else{
			return 'Unknown';
		}
	}
	public function show_location($len=1000){
		$location = $this->location;
		if(strlen($location)>$len){
			$location = e(substr($location, 0, $len));
			$location .='...';
		}
		return $location;
	}
	public function photoSrc(){
		$p = $this->photos;
		if(count($p)<1){
			return url('img/no_image.jpg');
		}
		return $p->first()->thumb_url;
	}
}