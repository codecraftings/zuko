<?php
namespace Zucko\Core\Event\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class CreateValidator extends LaravelValidator{
	protected $rules = array(
		'title' => 'required|min:10|max:50',
		'location' => 'required',
		'link' =>'url',
		'video_url' => 'url',
		'description' =>'required',
		'user_id' => 'required|exists:users,id',
		'city_id' => 'required|exists:cities,id',
		'event_time' => 'required|date'
		);
}