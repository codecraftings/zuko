<?php namespace Zucko\Core\Services\Search;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class PaginatorFactory{
	public function __construct(){
	}
	public function make($query, $pagging=10, $page=1){
		$total = $query->count();
		$items = $query->skip(($page-1)*$pagging)->take($pagging)->get()->all();
		$result = $this->create($items, $total, $pagging, $page);
		return $result;
	}
	public function create($items, $total, $pagging, $page){
		return new Paginator($items, $total, $pagging, $page);
	}
}