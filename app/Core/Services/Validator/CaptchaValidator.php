<?php
namespace Zucko\Core\Services\Validator;
class CaptchaValidator extends LaravelValidator{
	protected $rules = array(
			'recaptcha_response_field' => 'required|recaptcha'
		);
}