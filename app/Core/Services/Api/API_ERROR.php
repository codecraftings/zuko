<?php
namespace Zucko\Core\Services\Api;
class API_ERROR{
	const NOT_LOGGED_IN = 111;
	const UNKNOWN_ERROR = 666;
	const ALREADY_REVIEWED = 555;
	public static function get_message($code){
		$messages = array(
			API_ERROR::NOT_LOGGED_IN => 'You are not logged in. Please login to continue',
			API_ERROR::UNKNOWN_ERROR => 'Something wrong happened!',
			API_ERROR::ALREADY_REVIEWED => 'You already reviewed this business',
			);
		if(isset($messages[$code])){
			return $messages[$code];
		}else{
			return $messages[API_ERROR::UNKNOWN_ERROR];
		}
	}
	public static function msg($code){
		return self::get_message($code);
	}
}