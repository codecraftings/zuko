<?php
namespace Zucko\Core\Services\Filters;
use Zucko\Core\User\Services\Session\SessionService;
use Zucko\Core\Services\Api\Api;
use Zucko\Core\Services\Api\API_ERROR;
class AuthFilter{
	protected $session;
	protected $api;
	public function __construct(SessionService $session, Api $api){
		$this->session = $session;
		$this->api = $api;
	}
	public function filter($route, $request){
		if(!$this->session->isLoggedIn()){
			if($request->ajax()){
				return $this->api->error(API_ERROR::NOT_LOGGED_IN);
			}
			return \Redirect::to('login')->with('pending_url',$request->fullUrl());
		}
	}
}