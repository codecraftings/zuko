<?php
namespace Zucko\Core\Services\Filters;
use Zucko\Core\Services\Validator\CaptchaValidator;
class CaptchaFilter{
	protected $validator;
	public function __construct(CaptchaValidator $validator){
		$this->validator = $validator;
	}
	public function filter($route, $request){
		$this->validator->with(\Input::all());
		if(!$this->validator->passes()){
			$data = \Input::except('profile_pic');
			$data['error'] = $this->validator->errors()->first();
			//return \Redirect::back()->with($data);
		}
	}
}