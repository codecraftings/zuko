<?php
namespace Zucko\Core\Services\Mail\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class ContactMailValidator extends LaravelValidator{
	protected $rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'subject' => 'required',
			'msg_body' => 'required'
		);
}