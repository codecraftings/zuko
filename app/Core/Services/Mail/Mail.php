<?php
namespace Zucko\Core\Services\Mail;
use Illuminate\Mail\Mailer;
use Zucko\Core\User\User;
use Illuminate\Support\MessageBag;
use Zucko\Core\Services\Mail\Validator\ContactMailValidator;
use Zucko\Core\User\Repos\UserRepo;
class Mail{
	protected $mailer;
	protected $admin_email;
	protected $no_reply_email;
	protected $contact_validator;
	protected $user;
	protected $errors;
	public function __construct(Mailer $mailer, MessageBag $bag, ContactMailValidator $contact_validator, UserRepo $user){
		$this->user = $user;
		$this->mailer = $mailer;
		$this->admin_email = "contact@zuckoo.com";
		$this->contact_validator = $contact_validator;
		$this->errors = $bag;
	}
	public function sendVerificationEmail(User $user){
		$user = $this->user->get($user);
		if(!$user||$user->email_verified()){
			$this->errors->add('verification','Invalid user!');
			return false;
		}
		$this->mailer->send('emails.auth.email-verification', compact('user'), function($message) use ($user){
			$message->to($user->email, $user->present()->name());
			$message->subject('Confirm your email');
		});
	}
	public function sendContactEmail($name, $email, $subject, $msg_body){
		$data = compact('name','email','subject','msg_body');
		$this->contact_validator->with($data);
		if(!$this->contact_validator->passes()){
			$this->errors = $this->contact_validator->errors();
			return false;
		}
		$this->mailer->send('emails.user-contact', $data, function($message) use ($email, $name, $subject){
			$message->to($this->admin_email, 'Zuckoo Admin');
			$message->from($email, $name);
			$message->subject($subject);
		});
		return true;
	}
	public function errors(){
		return $this->errors;
	}
}