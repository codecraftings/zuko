<?php
namespace Zucko\Core\Services\File;
use Zucko\Core\Services\Validator\ImageValidator;
use Illuminate\Support\MessageBag;
use Zucko\Core\Services\File\Directory;
use Zucko\Core\Services\File\File;
class Uploader{
	protected $upload_path;
	public $moved_files;
	protected $uploading_files;
	protected $image_validator;
	protected $errors;
	protected $directory;
	protected $file;
	public function __construct(ImageValidator $image_validator, MessageBag $bag, Directory $dir, File $file){
		$this->upload_path = public_path().DIRECTORY_SEPARATOR.'uploads';
		$this->image_validator = $image_validator;
		$this->errors = $bag;
		$this->directory = $dir;
		$this->file = $file;
	}
	public function setFiles($files){
		if(is_array($files)){
			$this->uploading_files = array();
			foreach ($files as $key => $file) {
				array_push($this->uploading_files, $this->file->fromUploadedFile($file));
			}
		}
		else{
			$this->uploading_files = array($this->file->fromUploadedFile($files));
		}
	}
	public function acceptUploadImages(){
		$this->moved_files = array();
		//dd($this->uploading_files);
		foreach ($this->uploading_files as $key => $file) {
			$this->image_validator->with(array('file'=>$file->uploaded_file));
			if($this->image_validator->passes()){
				$move_dir = $this->getCurrentUploadDir();
				$ext = $file->getExtension();
				$newName = $move_dir->uniqueFilename($ext, $file->getName());
				$file->uploaded_file->move($move_dir->getPath(), $newName);
				$file->filename = $newName;
				$file->path = $move_dir->getPath().DIRECTORY_SEPARATOR.$newName;
				$file->uploaded_file = null;
				array_push($this->moved_files, $file);
			}else{
				continue;
			}
		}
	}
	public function getMovedFiles(){
		if(empty($this->moved_files)){
			return false;
		}
		return $this->moved_files;
	}
	public function getCurrentUploadDir(){
		$dir = $this->directory->newInstance($this->upload_path);
		$dir = $dir->gotoOrCreate(date('Y'))->gotoOrCreate(date('n'));
		if($dir->dirCount()<1){
			$dir = $dir->addDir('1');
		}else{
			$dir = $dir->lastDir();
		}
		if($dir->filesCount()>100){
			$newName = $dir->dirName()+1;
			$dir = $dir->parent()->addDir($newName);
		}
		return $dir;
	}
	public function errors(){
		return $this->errors;
	}
}