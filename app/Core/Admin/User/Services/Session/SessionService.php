<?php
namespace Zucko\Core\Admin\User\Services\Session;
use Zucko\Core\User\Repos\UserRepo;
use Illuminate\Support\MessageBag;
use Illuminate\Auth\AuthManager as Auth;
use Zucko\Core\User\Role;
class SessionService{
	protected $user;
	protected $errors;
	protected $auth;
	public function __construct(UserRepo $user, Auth $auth, MessageBag $errors){
		$this->user = $user;
		$this->errors = $errors;
		$this->auth = $auth;
	}
	public function login($email, $password){
		if(!$this->auth->attempt(array('email'=>$email,'password'=>$password,'role_id'=>Role::ADMIN),true)){
			$this->errors->add('login',"Invalid Credentials");
			return false;
		}
		return true;
	}
	public function getCurrentUser(){
		$user = $this->auth->user();
		if(!$user||$user->role_id!=Role::ADMIN){
			return false;
		}
		return $user;
	}
	public function isLoggedIn(){
		return $this->getCurrentUser()?true:false;
	}
	public function isAdmin(){
		$user = $this->getCurrentUser();
		if(!$user){
			return false;
		}
		return $user->role_id==Role::ADMIN;
	}
	public function logout(){
		return $this->auth->logout();
	}
	public function errors(){
		return $this->errors;
	}
}