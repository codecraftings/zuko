<?php
namespace Zucko\Core;
class BaseModel extends \Eloquent{
	const PUBLISHED = "published";
	const DRAFT = "draft";
}