<?php
namespace Zucko\Core\Comment\Repos;
use Zucko\Core\Review\Repos\ReviewRepo;
use Zucko\Core\Comment\Services\Validator\CommentCreateValidator;
use Zucko\Core\Comment\Services\Validator\CommentUpdateValidator;
use Zucko\Core\Comment\Comment;
use Illuminate\Support\MessageBag;
use Zucko\Core\Services\Search\PaginatorFactory;

class CommentRepo{
	protected $review;
	protected $comment;
	protected $create_validator;
	protected $update_validator;
	protected $errors;
	protected $paginator;
	public function __construct(MessageBag $bag, PaginatorFactory $paginator, ReviewRepo $review, Comment $comment, CommentCreateValidator $create_validator, CommentUpdateValidator $update_validator){
		$this->review = $review;
		$this->comment = $comment;
		$this->create_validator = $create_validator;
		$this->update_validator = $update_validator;
		$this->errors = $bag;
		$this->paginator = $paginator;
	}
	public function get($comment){
		if($comment instanceof Comment){
			return $comment;
		}
		$comment = $this->comment->find($comment);
		if(!$comment){
			$this->errors()->add("comment","Comment does not exists");
		}
		return $comment;
	}
	public function search($str, $filters, $pagging, $page){
		$q = $this->comment->query();
		if(!empty($search)){
			$q->where('description','like','%'.$search.'%');
		}
//		$total = $q->count();
		if(isset($filters['sort'])){
			switch ($filters['sort']) {
				case 'updated':
				$q->orderBy('updated_at','desc');
				break;
				case 'old':
				$q->orderBy('created_at','asc');
				break;
				default:
				$q->orderBy('id','desc');
				break;
			}
		}
//		$items = $q->skip(($page-1)*$pagging)->take($pagging)->get()->all();
//		$this->paginator->setCurrentPage($page);
		return $this->paginator->make($q, $pagging, $page);
	}
	public function delete($comment){
		$comment = $this->get($comment);
		if(!$comment){
			$this->errors()->add("comment","This comment does not exist!");
			return false;
		}
		return $comment->delete();
	}
	public function getWithAll($comment_id){
		return $this->comment->with('user','target')->where('id',$comment_id)->first();
	}
	public function create($data){
		$this->create_validator->with($data);
		if(!$this->create_validator->passes()){
			$this->errors = $this->create_validator->errors();
			return false;
		}
		$comment = $this->comment->create($data);
		return $comment;
	}
	public function update($comment, $data){
		$comment = $this->get($comment);
		if(!$comment){
			return false;
		}
		$this->update_validator->with($data);
		if(!$this->update_validator->passes()){
			$this->errors = $this->update_validator->errors();
			return false;
		}
		$comment = $comment->update($data);
		return $comment;
	}
	public function errors(){
		return $this->errors;
	}
}