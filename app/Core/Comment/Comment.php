<?php
namespace Zucko\Core\Comment;
/*
id
description
user_id
target_id
target_type
created_at
updated_at
*/
use Zucko\Core\BaseModel as Base;
class Comment extends Base{
	protected $table = "comments";
	protected $fillable = array('description','user_id','target_id','target_type');
	protected $appends = array('permalink');
	public function user(){
		return $this->belongsTo('Zucko\Core\User\User');
	}
	public function target(){
		return $this->morphTo();
	}
	public function getPermalinkAttribute(){
		return url('');
	}
}