<?php
namespace Zucko\Core\Comment\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class CommentCreateValidator extends LaravelValidator{
	protected $rules = array(
			'target_id' => 'required',
			'target_type' => 'required',
			'description' => 'required|min:5',
			'user_id' => 'required|exists:users,id'
		);
}