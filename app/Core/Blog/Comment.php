<?php
namespace Zucko\Core\Blog;
/*
id
description
user_id
blog_id
parent_id
created_at
updated_at
*/
use Zucko\Core\BaseModel as Base;
class Comment extends Base{
	protected $table = "blog_comments";
	protected $fillable = array('description','user_id','blog_id','parent_id');
	protected $appends = array('permalink');
	public function getPermalinkAttribute(){
		$url = $this->blog->permalink;
		$url .="#comment_".$this->id;
		return $url;
	}
	public function user(){
		return $this->belongsTo('Zucko\Core\User\User');
	}
	public function blog(){
		return $this->belongsTo('Zucko\Core\Blog\Blog');
	}
	public function replies(){
		return $this->hasMany('Zucko\Core\Blog\Comment','parent_id');
	}
}