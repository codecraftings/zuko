<?php
namespace Zucko\Core\Blog\Repos;
use Zucko\Core\Blog\Blog;
use Zucko\Core\Blog\Category;
use Zucko\Core\Blog\Comment;
use Zucko\Core\Blog\Services\Validator\CreateValidator;
use Zucko\Core\Blog\Services\Validator\UpdateValidator;
use Zucko\Core\Blog\Services\Validator\CommentCreateValidator;
use Illuminate\Support\MessageBag;
use Symfony\Component\DomCrawler\Crawler;
class BlogRepo{
	protected $blog;
	protected $category;
	protected $create_validator;
	protected $update_validator;
	protected $comment_create_validator;
	protected $errors;
	protected $crawler;
	protected $comment;
	public function __construct(Blog $blog, Category $category, CreateValidator $create_validator, UpdateValidator $update_validator, MessageBag $bag, Crawler $crawler, CommentCreateValidator $comment_create_validator, Comment $comment){
		$this->blog = $blog;
		$this->category = $category;
		$this->create_validator = $create_validator;
		$this->update_validator = $update_validator;
		$this->comment_create_validator = $comment_create_validator;
		$this->crawler = $crawler;
		$this->comment = $comment;
		$this->errors = $bag;
	}
	public function get($blog){
		if($blog instanceof Blog){
			return $blog;
		}
		$blog = $this->blog->find($blog);
		if(!$blog){
			$this->errors->add('error','Blog does not exists!');
			return false;
		}
		return $blog;
	}
	public function search($q, $cats, $paggin=10){
		$query = $this->blog->query();
		if($q){
			$query = $query->where('title','like','%'.$q.'%');
		}
		if($cats){
			$query = $query->whereIn('category_id',$cats);
		}
		$query = $query->orderBy('created_at','desc')->paginate($paggin);
		return $query;
	}
	public function getCategory($category){
		if(!$category instanceof Category){
			$category = $this->category->find($category);
		}
		return $category;
	}
	public function getCategories(){
		return $this->category->all();
	}
	public function  getBlogsByCategory($category){
		$category = $this->getCategory($category);
		$blogs = $this->blog->where('category_id','=',$category->id)->get();
		return $blogs;
	}
	public function create(array $data){
		$this->create_validator->with($data);
		if(!$this->create_validator->passes()){
			$this->errors = $this->create_validator->errors();
			return false;
		}
		/*$this->crawler->clear();
		$this->crawler->addHtmlContent($data['content']);
		dd($this->crawler->children()->filter('p')->html());
		*/
		$blog = $this->blog->create($data);
		if(!$blog){
			$this->errors->add('error','Unknown Error');
			return false;
		}
		return $blog;
	}
	public function update($blog, array $data){
		$blog = $this->get($blog);
		if(!$blog){
			return false;
		}
		$this->update_validator->with($data);
		if(!$this->update_validator->passes()){
			$this->errors = $this->update_validator->errors();
			return false;
		}
		$blog->update($data);
		return $blog;
	}
	public function createComment(array $data){
		$this->comment_create_validator->with($data);
		if(!$this->comment_create_validator->passes()){
			$this->errors = $this->comment_create_validator->errors();
			return false;
		}
		$comment = $this->comment->create($data);
		if(!$comment){
			$this->errors->add('comment','Something went wrong');
		}
		return $comment;
	}
	public function errors(){
		return $this->errors;
	}
}