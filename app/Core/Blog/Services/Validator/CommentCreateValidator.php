<?php
namespace Zucko\Core\Blog\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class CommentCreateValidator extends LaravelValidator{
	protected $rules = array(
			'description' => 'required|min:4|max:1000',
			'user_id' => 'required|exists:users,id',
			'blog_id' => 'required|exists:blogs,id',
			'parent_id' => 'sometimes|required|exists:users,id'
		);
}