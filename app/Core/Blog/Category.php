<?php
namespace Zucko\Core\Blog;
/*
id
name
slag
style
blog_count
*/
use Zucko\Core\BaseModel as Base;
class Category extends Base{
	protected $table = "blog_categories";
	protected $fillable = array('name','slag','style');
	protected $appends = array('listing_page');
	protected $timestamp = false;
	public function  getListingPageAttribute(){
		return url('blogs?cat='.$this->id);
	}
	public function blogs(){
		return $this->hasMany('Zucko\Core\Blog\Blog');
	}
	public function getStyleAttribute(){
		return json_decode($this->attributes['style']);
	}
	
}