<?php
namespace Zucko\Core\Business;
/*
id
name
address
category_id
city
city_id
state
country
zip_code
latitude
longitude
geohash
user_id
listing_type---form_options
company_type---form_options
food_type---form_options
responsibility---form_options
tahiti_numero
tahiti_rc
description
rating_score
view_count
review_count
keywords
listing_status
logo
*/
use Zucko\Core\BaseModel as Base;
use Laracasts\Presenter\PresentableTrait;
use Zucko\Core\Photo\Photo;

class Business extends Base{
	use PresentableTrait;
	protected $table = "businesses";
	protected $presenter = "Zucko\Core\Business\Presenters\BusinessPresenter";
	protected $fillable = array(
			'name',
			'address',
			'user_id',
			'category_id',
			'city_id',
			'city',
			'state',
			'country',
			'zip_code',
			'latitude',
			'longitude',
			'geohash',
			'listing_type',
			'company_type',
			'food_type',
			'responsibility',
			'tahiti_numero',
			'tahiti_rc',
			'description',
			'rating_score',
			'review_count',
			'view_count',
			'logo'
		);
	protected $appends = array('profile_url', 'review_page','edit_page', 'is_bal');
	public function getIsBalAttribute(){
		return false;
	}
	public function getEditPageAttribute(){
		return url('business/'.$this->id.'/edit');
	}
	public function getReviewPageAttribute(){
		//return url("business/".$this->id."/reviews");
		return $this->profile_url.'#reviews';
	}
	public function getProfileUrlAttribute(){
		return url('business/show/'.$this->id.'/'.urlencode(str_replace(array('"',"'","/","\\"), "",$this->name)));
	}
	public function published(){
		return $this->query()->where('listing_status','=',Base::PUBLISHED);
	}
	public function is_published(){
		return $this->listing_status==Base::PUBLISHED;
	}
	public function _logo(){
		return Photo::find($this->logo);
	}
	public function meta(){
		return $this->hasOne('Zucko\Core\Business\BusinessMeta');
	}
	public function category(){
		return $this->belongsTo('Zucko\Core\Business\Category');
	}
	public function user(){
		return $this->belongsTo('Zucko\Core\User\User');
	}
	public function photos(){
		return $this->morphMany('Zucko\Core\Photo\Photo','target');
	}
	public function sliderPhotos(){
		return $this->photos()->where('type', Photo::TYPE_REGULAR)->get();
	}
	public function reviews(){
		return $this->hasMany('Zucko\Core\Review\Review');
	}
}