<?php
namespace Zucko\Core\Business\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class CreateValidator extends LaravelValidator{
	protected $rules = array(
		'name' => 'required|min:5|max:80',
		'address' => 'required|min:3|max:150',
		'category_id' => 'required|exists:categories,id',
		'city_id' => 'required|exists:cities,id',
		'city' => 'required',
		'state' => '',
		'country' => 'required',
		'zip_code' => 'required|numeric',
		'latitude' => '',
		'longitude' => '',
		'user_id' => 'required|exists:users,id',
		'terms' => 'required|accepted',
		'listing_type' => 'required'
		);
}