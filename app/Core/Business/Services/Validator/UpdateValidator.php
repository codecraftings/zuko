<?php
namespace Zucko\Core\Business\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class UpdateValidator extends LaravelValidator{
	protected $rules = array(
		'name' => 'sometimes|required|min:5|max:80',
		'address' => 'sometimes|required|min:3|max:150',
		'category_id' => 'sometimes|required|exists:categories,id',
		'city_id' => 'sometimes|required|exists:cities,id',
		'city' => 'sometimes|required',
		'state' => '',
		'country' => 'sometimes|required',
		'zip_code' => 'sometimes|required|numeric',
		'latitude' => 'sometimes|required',
		'longitude' => 'sometimes|required',
		'user_id' => 'sometimes|required|exists:users,id'
		);
}