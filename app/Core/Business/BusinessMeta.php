<?php
namespace Zucko\Core\Business;
/*
phone
email
fax
fb_url
twitter_url
youtube_url
gplus_url
linkedin_url
website
video_url
opening_hours---json text
opening_hour_hash
good_for_groups---boolean
water_service---boolean
credit_card_accepted---boolean
parking_valet---boolean
wheelchair_accessible
price_low
price_high
price_currency
outdoor_seating---bolean
good_for_kids---boolean
good_for--- form_options
take_reservation---- boolean
alcohol---boolean
delivery---bolean
to_go--- boolean
dogs_allowed--- boolean
brand_name
client_name
pro_ref
owner_pos
company_vini
share_capital
important
slider_featured
*/
use Zucko\Core\BaseModel as Base;
class BusinessMeta extends Base{
	public $timestamps = false;
	protected $fillable = array(
			'business_id',
			'phone',
			'email',
			'fax',
			'fb_url',
			'twitter_url',
			'website',
			'video_url',
			'opening_hours',
			'opening_hour_hash',
			'good_for_groups',
			'water_service',
			'credit_card_accepted',
			'parking_valet',
			'wheelchair_accessible',
			'price_low',
			'price_high',
			'price_currency',
			'outdoor_seating',
			'good_for_kids',
			'good_for',
			'take_reservation',
			'alcohol',
			'delivery',
			'to_go',
			'dogs_allowed',
			'brand_name',
			'client_name',
			'pro_ref',
			'owner_pos',
			'company_vini',
			'share_capital',
			'youtube_url',
			'gplus_url',
			'linkedin_url',
			'important',
			'slider_featured'
		);
	protected $table = "business_metadata";
	public function getOpeningHoursAttribute(){
		if(empty($this->attributes['opening_hours'])){
			return json_decode('[{"day1":"mon","day2":"sun","time1":"00","time2":"00"}]');
		}else{
			return json_decode($this->attributes['opening_hours']);
		}
	}
	public function getVideoUrlAttribute(){
		$url = $this->attributes['video_url'];
		if(!$url){
			return false;
		}
		$parts = [];
		preg_match('/\?v=([^&]*)/', $url, $parts);
		if(count($parts)<2){
		    return false;
		}
        return "http://www.youtube.com/embed/".$parts[1]."?autoplay=0&controls=0&autohide=1&showinfo=0&modestbranding=1&rel=0";
    }
}