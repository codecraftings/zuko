<?php
namespace Zucko\Core\Business;
use Zucko\Core\BaseModel as Base;
class BLBusiness extends Base{
	protected $table = "bl_business";
	public $timestamps = false;
	protected $fillable = array(
			'company_or_service',
			'address',
			'zip_code',
			'brand',
			'category',
			'category_id',
			'city',
			'client',
			'first_name',
			'position',
			'email',
			'facebook',
			'fax',
			'logo',
			'island_country',
			'last_name',
			'phone',
			'pro_ref',
			'share_capital',
			'tahiti_id',
			'tahiti_rc',
			'company_type',
			'vini',
			'website'
		);
	protected $appends = array('is_bal');
	public function getIsBalAttribute(){
		return true;
	}
	public function getSerializedData(){
		$data = array();
		if($this->pro_ref){
			//$data['PRO Reference'] = $this->pro_ref;
		}
		if($this->company_or_service){
			$data['Name'] = $this->company_or_service;
		}
		if($this->category){
			$data['Category'] = $this->category;
		}
		if($this->position){
			$data['Position'] = $this->position;
		}
		if($this->first_name){
			$data['First Name'] = $this->first_name;
		}
		if($this->last_name){
			$data['Last Name'] = $this->last_name;
		}
		if($this->company_type){
			$data['Type of Company'] = $this->company_type;
		}
		if($this->phone){
			$data['Phone'] = $this->phone;
		}
		if($this->email){
			$data['Email'] = $this->email;
		}
		if($this->vini){
			$data['VINI'] = $this->vini;
		}
		if($this->fax){
			$data['Fax'] = $this->fax;
		}
		if($this->tahiti_rc){
			$data['Tahiti RC'] = $this->tahiti_rc;
		}
		if($this->island_country){
			$data['Island or Country'] = $this->island_country;
		}
		if($this->share_capital){
			$data['Share Capital(XPF)'] = $this->share_capital;
		}
		if($this->tahiti_id){
			$data['Tahiti ID'] = $this->tahiti_id;
		}
		if($this->address){
			$data['Address'] = $this->address;
		}
		if($this->city){
			$data['City'] = $this->city;
		}
		if($this->zip_code){
			$data['BP/Zipcode'] = $this->zip_code;
		}
		if($this->brand){
			$data['Brand'] = $this->brand;
		}
		if($this->facebook){
			$data['Facebook'] = $this->facebook;
		}
		if($this->website){
			$data['Website'] = $this->website;
		}
		return $data;
	}
}