<?php
namespace Zucko\Core\Inbox;
/*
id
user1_id
user2_id
user1_deleted
user2_deleted
subject
created_at
updated_at
*/
use Zucko\Core\BaseModel as Base;
class Thread extends Base{
	protected $table = "threads";
	private $last_message;
	protected $fillable = array('user1_id','user2_id','subject');
	protected $appends = array('permalink');
	public function first_user(){
		return $this->belongsTo('Zucko\Core\User\User','user1_id');
	}
	public function other_user_id($user_one_id){
		if($this->user1_id==$user_one_id){
			return $this->user2_id;
		}
		return $this->user1_id;
	}
	public function getPermalinkAttribute(){
		return url('inbox/thread/'.$this->id);
	}
	public function messages(){
		return $this->hasMany('Zucko\Core\Inbox\Message');
	}
	public function lastMessage(){
		if(!$this->last_message){
			$this->last_message = $this->messages()->orderBy('id','desc')->first();
		}
		return $this->last_message;
	}
}