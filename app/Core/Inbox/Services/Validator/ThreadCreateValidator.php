<?php
namespace Zucko\Core\Inbox\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class ThreadCreateValidator extends LaravelValidator{
	protected $rules = array(
			'user1_id' => 'required|exists:users,id',
			'user2_id' => 'required|exists:users,id',
			'subject' => 'required',
		);
}