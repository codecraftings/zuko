<?php
namespace Zucko\Core\Inbox\Services\Validator;
use Zucko\Core\Services\Validator\LaravelValidator;
class MessageCreateValidator extends LaravelValidator{
	public $rules = array(
			'sender_id' => 'required|exists:users,id',
			'receiver_id' => 'required|exists:users,id',
			'thread_id' => 'required|exists:threads,id',
			'message' => 'required'
		);
}