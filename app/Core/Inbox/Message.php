<?php
namespace Zucko\Core\Inbox;
/*
id
sender_id
receiver_id
message
unread
thread_id
created_at
updated_at
*/
use Zucko\Core\BaseModel as Base;
class Message extends Base{
	protected $table = "messages";
	protected $fillable = array('sender_id','receiver_id','message','unread','thread_id');
	public function sender(){
		return $this->belongsTo('Zucko\Core\User\User','sender_id');
	}
	public function receiver(){
		return $this->belongsTo('Zucko\Core\User\User','receiver_id');
	}
	public function thread(){
		return $this->belongsTo('Zucko\Core\Inbox\Thread','thread_id');
	}
	public function readBy($user_id){
		if(!$this->unread){
			return true;
		}
		if($this->sender_id==$user_id){
			return true;
		}
		return false;
	}
	public function text($len=1000){
		$desc = $this->message;
		if(strlen($desc)>$len){
			$desc = e(substr($desc, 0, $len));
			$desc .='... <a href="#">read more</a>';
		}
		return $desc;
	}
}