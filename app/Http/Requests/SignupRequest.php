<?php


namespace Zucko\Http\Requests;


class SignupRequest extends Request {
    public function rules(){
        return [
            'recaptcha_response_field' => 'required|recaptcha'
        ];
    }
}