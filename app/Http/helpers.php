<?php
function make_slag($name){
	return snake_case(strtolower($name));
}
function yes_or_no($boolean){
	return $boolean?trans('app.Yes'):trans('app.No');
}
function admin_url($path){
	return url(Config::get('app.admin_dir').'/'.$path);
}
function url_append_query($query){
	$url = Request::fullUrl();
	$url = trim($url, "?,&");
	$seg = explode("?", $url);
	if(count($seg)>1){
		$params = array();
		$inp = explode("&", $seg[1]);
		foreach ($inp as $key => $value) {
			$tmp = explode("=", $value);
			$params[$tmp[0]] = $tmp[1];
		}
		$query = explode("&", $query);
		foreach ($query as $key => $value) {
			$tmp = explode("=", $value);
			$params[$tmp[0]] = $tmp[1];
		}
		$url = $seg[0]."?";
		foreach ($params as $key => $value) {
			$url .=$key."=".$value."&";
		}
		$url = trim($url, "?,&");
	}else{
		$url .= "?".$query;
	}
	return $url;
}
function create_array($objArr, $key, $value){
	$keys = array_pluck($objArr, $key);
	$values = array_pluck($objArr, $value);
	$arr = array_combine($keys, $values);
	return $arr;
}

function limit_str($str, $len, $tail = ".."){
	if(strlen($str)<=$len){
		return $str;
	}
	$str = substr($str, 0, $len).$tail;
	return $str;
}
function asset_url($path="/"){
	return url($path);
}
function api_error($msg ="Bad Request", $code=400){
	Log::error($msg);
	return Response::json(array(
		'status' => $code,
		'error'=>array('message'=>$msg)
		), $code);
}

function api_success($data, $code=200){
	$response = array(
		'status' => $code
		);
	if(is_string($data)){
		$response['message'] = $data;
	}
	elseif(is_array($data)){
		$response = array_merge($response, $data);
	}else{
		$response['data'] = $data;
	}
	return Response::json($response, $code);
}