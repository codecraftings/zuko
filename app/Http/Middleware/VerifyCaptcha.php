<?php


namespace Zucko\Http\Middleware;


use Zucko\Core\Services\Validator\CaptchaValidator;

class VerifyCaptcha {
    /**
     * @var CaptchaValidator
     */
    private $validator;

    public function __construct(CaptchaValidator $validator)
    {

        $this->validator = $validator;
    }
    public function handle($request, \Closure $next){
        $this->validator->with($request->all());
        if(!$this->validator->passes()){
            $data = \Input::except('profile_pic');
            $data['error'] = $this->validator->errors()->first();
            //return \Redirect::back()->with($data);
        }
        return $next($request);
    }
}