<?php

namespace Zucko\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role = "user")
    {
        if ($this->auth->guest()||!$this->auth->user()->hasRole($role)) {
            if ($request->ajax()) {
                return api_error("Unauthorized", 401);
            } else {
                return redirect()->guest('login');
            }
        }
        return $next($request);
    }
}
