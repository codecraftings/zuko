<?php namespace Zucko\Http\Controllers;

use Illuminate\Support\Facades\App;
use Zucko\Core\Business\Repos\BusinessRepo;
use Zucko\Core\Review\Repos\ReviewRepo;
use Zucko\Core\Event\Repos\EventRepo;
use Zucko\Core\User\Services\Session\SessionService;
use Zucko\Core\Services\Mail\Mail;
use Zucko\Core\Address\Repos\AddressRepo;

class PagesController extends BaseController
{
    protected $business;
    protected $review;
    protected $event;
    protected $mail;

    public function __construct(BusinessRepo $business, EventRepo $event, ReviewRepo $review, SessionService $session, Mail $mail, AddressRepo $address)
    {
        $this->business = $business;
        $this->review = $review;
        $this->event = $event;
        $this->mail = $mail;
        parent::__construct($session, $address);
    }

    public function showHome()
    {
        \View::share('categories', $this->business->getCategories());
        \View::share('recent_reviews', $this->review->getRecents(3));
        \View::share('top_reviews', $this->review->getTop(3));
        \View::share('top_events', $this->event->getPopular('', 3));
        \View::share('latest_events', $this->event->getRecent('', 2));
        \View::share('popular_businesses', $this->business->getPopularBusinesses('city', 12));
        return view('zucko.home');
    }

    public function showCities()
    {
        $q = \Input::get('q');
        if (empty($q)) {
            \View::share('cities', $this->address->getPopularCities());
        } else {
            \View::share('q', $q);
            \View::share('cities', $this->address->getCitiesByName($q));
        }
        return view('zucko.search.list-cities-page');
    }

    public function showAboutPage()
    {
        return view('pages.about-us');
    }

    public function showTermsPage()
    {
        if (App::getLocale() == "fr") {
            return view('pages.terms-fr');
        } else {
            return view('pages.terms');
        }
    }

    public function showPrivacyPage()
    {
        if (App::getLocale() == "fr") {
            return view('pages.privacy-fr');
        } else {
            return view('pages.privacy');
        }
    }

    public function showCareerPage()
    {
        return view('pages.career');
    }

    public function showMobilePage()
    {
        return view('pages.mobile');
    }

    public function showContactPage()
    {
        return view('pages.contact-us');
    }

    public function submitContactPage()
    {
        $data = \Input::all();
        $attempt = $this->mail->sendContactEmail($data['name'], $data['email'], $data['subject'], $data['message']);
        if (!$attempt) {
            $data['error'] = $this->mail->errors()->first();
            //dd($data);
            return \Redirect::back()->with($data);
        } else {
            return \Redirect::back()->withSuccess("Your message sent successfully to admin. We will get back to you shortly");
        }
    }
}
