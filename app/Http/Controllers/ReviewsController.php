<?php namespace Zucko\Http\Controllers;
use Zucko\Core\Services\Search\PaginatorFactory;
use Zucko\Core\User\Services\Session\SessionService;
use Zucko\Core\Business\Repos\BusinessRepo;
use Zucko\Core\Address\Repos\AddressRepo;
use Zucko\Core\Review\Repos\ReviewRepo;
use Zucko\Core\Review\Repos\VotingRepo;
use Zucko\Core\Comment\Repos\CommentRepo;
use Zucko\Core\Services\Api\Api;
use Zucko\Core\Services\Api\API_ERROR;
class ReviewsController extends BaseController{
	protected $business;
	protected $review;
	protected $api;
	protected $comment;
	protected $voting;
	/**
	 * @var PaginatorFactory
	 */
	private $paginator;

	public function __construct(CommentRepo $comment, VotingRepo $voting, SessionService $session, BusinessRepo $business, AddressRepo $address, ReviewRepo $review, Api $api, PaginatorFactory $paginator){
		$this->business = $business;
		$this->voting = $voting;
		$this->api = $api;
		$this->review = $review;
		$this->comment = $comment;
		$this->paginator = $paginator;
		parent::__construct($session, $address);
	}
	public function showAddPage(){
		$name = \Input::get('name');
		$address = \Input::get('address');
		$filters = array();
		$result = $this->business->search($name, $address, $filters, \Input::get('page'), 10);
		$businesses = $this->paginator->create($result->items, $result->total_items, 10, \Input::get('page',1));
		return view('zucko.review.add-page')->withBusinesses($businesses);
	}
	public function addReview(){
		$data = array();
		$data['description'] = \Input::get('description');
		$data['user_id'] = \Input::get('user_id');
		$data['business_id'] = \Input::get('business_id');
		$data['rating'] = \Input::get('score');
		if(($review = $this->review->alreadyReviewed($data['business_id'], $data['user_id']))){
			$review = $this->review->update($review, $data);
		}else{
			$review = $this->review->create($data);
		}
		if(!$review){
			$data['error'] = $this->review->errors()->first();
			return \Redirect::back()->with($data);
		}else{
			//$this->business->updateRating($data['business_id']);
			return \Redirect::back()->withSuccess('Thank you for your review. It will help others a lot.');
		}
	}
	public function ajaxAddReview(){
		$data = array();
		$data['description'] = \Input::get('description');
		$user = $this->session->getCurrentUser();
		$data['user_id'] = $user->id;
		$data['business_id'] = \Input::get('business_id');
		$data['rating'] = \Input::get('score');
		if(($review = $this->review->alreadyReviewed($data['business_id'], $data['user_id']))){
			//$review = $this->review->update($review, $data);
			return $this->api->error(API_ERROR::ALREADY_REVIEWED);
		}else{
			$review = $this->review->create($data);
		}
		if(!$review){
			return $this->api->error(API_ERROR::UNKNOWN_ERROR, $this->review->errors()->first());
		}else{
			//$this->business->updateRating($data['business_id']);
			return $this->api->success('Thank you for your review. It will help others a lot.');
		}
	}
	public function likeReview($id){
		$review = $this->review->get($id);
		$like = $this->review->likeReview($id, $this->session->getCurrentUser()->id);
		$review->likes_count += 1;
		$this->review->update($review, $review);
		return \Redirect::back();
	}
	public function ajaxLikeReview(){
		$review = $this->review->get(\Input::get('review_id'));
		$likes = $this->review->likeReview($review, $this->session->getCurrentUser()->id);
		$review->likes_count += 1;
		$this->review->update($review, $review);
		return $this->api->success(array('likes_count'=>$review->likes()->count()));
	}
	public function addComment($id){

	}
	public function ajaxAddComment(){
		$data = \Input::except('_token');
		$user = $this->session->getCurrentUser();
		$data['user_id'] = $user->id;
		$data['target_type'] = 'Zucko\Review\Review';
		$comment = $this->comment->create($data);
		if(!$comment){
			return $this->api->error(API_ERROR::UNKNOWN_ERROR, $this->comment->errors()->first());
		}
		$comment = $this->comment->getWithAll($comment->id);
		return $this->api->success(array('message'=>'Comment added successfully','comment'=>$comment));
	}
	public function showPage($id){
		$review = $this->review->get($id);
		if(!$review){
			return \Redirect::to('/');
		}
		$user = $this->session->getCurrentUser();
		$review->withUserInteractions($user);
		return view('zucko.review.profile-page')->withReview($review);
	}
	public function show($id){
		$review = $this->review->get($id);
		if(!$review){
			return $this->api->error(API_ERROR::UNKNOWN_ERROR);
		}
		$user = $this->session->getCurrentUser();
		$review->withUserInteractions($user);
		return $this->api->success($review->toArray());
	}
	public function createVote($review_id){
		$data = \Input::all();
		$review = $this->review->get($review_id);
		if(!$review){
			return $this->api->error(API_ERROR::UNKNOWN_ERROR);
		}
		$data['target_id'] = $review_id;
		$data['target_type'] = "Zucko\Review\Review";
		$data['user_id'] = $this->session->getCurrentUser()->id;
		$vote = $this->voting->create($data);
		if(!$vote){
			return $this->api->error(API_ERROR::UNKNOWN_ERROR, $this->voting->errors()->first());
		}
		if($data['type']=="useful"){
			$review->useful_count += 1;
		}elseif($data['type']=="funny"){
			$review->funny_count += 1;
		}elseif($data['type']=="likes"){
			$review->likes_count += 1;
		}
		$this->review->update($review, $review);
		return $this->api->success(array('message'=> 'Vote added successfully','id' => $vote->id));
	}
	public function deleteVote($review_id, $user_id, $type){
		$user = $this->session->getCurrentUser();
		if(!$type||$user->id!=$user_id){
			return api_error();
		}
		$vote = $this->voting->getByOwners($user_id, $review_id, "Zucko\Review\Review", $type);
		if(!$vote){
			return api_error();
		}
		$data = \Input::all();
		$this->voting->delete($vote);
		$review = $this->review->get($review_id);
		if($type=="useful"){
			$review->useful_count -= 1;
		}elseif($type=="funny"){
			$review->funny_count -= 1;
		}elseif($type=="likes"){
			$review->likes_count -= 1;
		}
		$this->review->update($review, $review);
		return api_success("deleted successfully!");
	}
}