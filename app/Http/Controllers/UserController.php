<?php namespace Zucko\Http\Controllers;
use Zucko\Core\User\Repos\UserRepo;
use Zucko\Core\User\Repos\FriendsRepo;
use Zucko\Core\User\Services\Session\SessionService;
use Carbon\Carbon;
use Zucko\Core\Services\File\Uploader;
use Zucko\Core\Address\Repos\AddressRepo;
use Zucko\Core\Photo\Repos\PhotoRepo;
class UserController extends BaseController{
	protected $user;
	protected $friends;
	protected $carbon;
	protected $uploader;
	protected $photo;
	public function __construct(UserRepo $user, PhotoRepo $photo, Uploader $uploader, Carbon $carbon, SessionService $session, FriendsRepo $friends, AddressRepo $address){
		$this->user = $user;
		$this->friends = $friends;
		$this->carbon = $carbon;
		$this->uploader = $uploader;
		$this->photo = $photo;
		parent::__construct($session, $address);
	}
	public function showUserBusinesses($id){
		$user = $this->user->get($id);
		if(!$user){
			return \Redirect::to('/');
		}
		$current_user = $this->session->getCurrentUser();
		if($user->id!=$current_user->id){
			return \Redirect::to('/');
		}
		return view('zucko.user.list-businesses')->withUser($user);
	}
	public function showUserEvents($id){
		$user = $this->user->get($id);
		if(!$user){
			return \Redirect::to('/');
		}
		$current_user = $this->session->getCurrentUser();
		if($user->id!=$current_user->id){
			return \Redirect::to('/');
		}
		return view('zucko.user.list-events')->withUser($user);
	}
	public function showEditUser($id){
		$user = $this->user->get($id);
		if(!$user){
			return \Redirect::to('/');
		}
		$current_user = $this->session->getCurrentUser();
		if($user->id!=$current_user->id){
			return \Redirect::to('/');
		}
		return view('zucko.user.edit-page')->withUser($user);
	}
	public function submitEditUser($id){
		Log::info(\Input::all());
		$user = $this->user->get($id);
		if(!$user){
			return \Redirect::to('/');
		}
		$current_user = $this->session->getCurrentUser();
		if($user->id!=$current_user->id){
			return \Redirect::to('/');
		}
		$data = \Input::except('profile_pic');
		if(\Input::hasFile('profile_pic')){
			$photos = $this->photo->createFromUpload(\Input::file('profile_pic'), $user->id, 'Zucko\User\User', $user->id, $user->nickname, 1000, 800);
			if($photos){
				$data['profile_pic'] = $photos[0]->thumb_url;
			}
		}
		if(\Input::has('dob-year')&&\Input::has('dob-month')&&\Input::has('dob-day'))
		$data['birthdate'] = $this->carbon->parse($data['dob-year']."-".$data['dob-month']."-".$data['dob-day']);
		if(\Input::has('hide_name'))
		$data['hide_name'] = \Input::get('hide_name',0);
		//dd($data);
		$user = $this->user->update($user, $data);
		Log::info($user);
		if(!$user){
			$data['error'] = $this->user->errors()->first();
			return \Redirect::back()->with($data);
		}
		return \Redirect::back()->withSuccess('Profile updated successfully');
	}
	public function showUser($id, $nickname=""){
		$user = $this->user->get($id);
		if(!$user){
			return \Redirect::to('/');
		}
		$reviews = $user->reviews();
		switch (\Input::get('sort', 'date')) {
			case 'date':
				$reviews->orderBy('created_at','desc');
				break;
			case 'useful':
				$reviews->orderBy('useful_count','desc');
				break;
			case 'funny':
				$reviews->orderBy('funny_count','desc');
				break;
			default:
				break;
		}
		$reviews = $reviews->paginate(\Input::get('pagging',10));
		\View::share('reviews', $reviews);
		return view('zucko.user.profile-page')->withUser($user);
	}
	public function showFindFriend(){
		$filters = array();
		if(\Input::get('city')){
			$filters['city_name'] = \Input::get('city');
		}
		$friends = $this->user->search(\Input::get('name'), $filters, \Input::get('pagging',10), \Input::get('page',1));
		return view('zucko.user.find-friends-page')->with(compact('friends'));
	}
	public function addFriend($friend_id){
		$friend = $this->user->get($friend_id);
		$user = $this->session->getCurrentUser();
		if(!$friend||!$user){
			return \Redirect::back();
		}
		if($user->all_friends()->where('friend_id','=',$friend_id)->count()>1){
			return \Redirect::back();
		}
		$this->friends->addFriend($user->id, $friend->id);
		return \Redirect::back();
	}
	public function acceptFriend($friend_id){
		$friend = $this->user->get($friend_id);
		$user = $this->session->getCurrentUser();
		if(!$friend||!$user){
			return \Redirect::back();
		}
		if($user->invitations()->where('friend_id','=',$friend_id)->count()<1){
			return \Redirect::back();
		}
		$this->friends->acceptFriend($user->id, $friend->id);
		return \Redirect::back();
	}
	public function declineFriend($friend_id){
		$friend = $this->user->get($friend_id);
		$user = $this->session->getCurrentUser();
		if(!$friend||!$user){
			return \Redirect::back();
		}
		if($user->all_friends()->where('friend_id','=',$friend_id)->count()<1){
			return \Redirect::back();
		}
		$this->friends->removeFriend($user->id, $friend->id);
		return \Redirect::back();
	}
}