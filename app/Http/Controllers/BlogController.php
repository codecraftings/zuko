<?php namespace Zucko\Http\Controllers;
use Zucko\Core\User\Repos\UserRepo;
use Zucko\Core\Blog\Repos\BlogRepo;
use Zucko\Core\User\Services\Session\SessionService;
use Carbon\Carbon;
use Zucko\Core\Services\File\Uploader;
use Zucko\Core\Address\Repos\AddressRepo;
use Zucko\Core\Photo\Repos\PhotoRepo;
class BlogController extends BaseController{
	protected $user;
	protected $blog;
	protected $carbon;
	protected $uploader;
	protected $photo;
	public function __construct(UserRepo $user, PhotoRepo $photo, Uploader $uploader, Carbon $carbon, SessionService $session, BlogRepo $blog, AddressRepo $address){
		$this->user = $user;
		$this->blog = $blog;
		$this->carbon = $carbon;
		$this->uploader = $uploader;
		$this->photo = $photo;
		\View::share('categories',$this->blog->getCategories());
		parent::__construct($session, $address);
	}
	public function showAllBlogs(){
		$cat = $this->blog->getCategory(\Input::get('cat'));
		if($cat){
			$search_term = $cat->name;
			$cat = array($cat->id);
		}
		$blogs = $this->blog->search(\Input::get('q'), $cat, \Input::get('pagging',10));
		return view('zucko.blog.listing-page')->with(compact('blogs','search_term'));
	}
	public function showAddPage(){
		return view('zucko.blog.add-page');
	}
	public function submitAddPage(){
		$data = \Input::except('_token', 'photo');
		if(!$this->session->isLoggedIn()){
			return \Redirect::to('login');
		}
		$user = $this->session->getCurrentUser();
		$data['user_id'] = $user->id;
		$blog = $this->blog->create($data);
		if(!$blog){
			$data['error'] = $this->blog->errors()->first();
			return \Redirect::back()->with($data);
		}
		if(\Input::hasFile('photo')){
			$photos = $this->photo->createFromUpload(\Input::file('photo'), $blog->id, 'Zucko\Blog\Blog', $user->id, $blog->title, 700, 350);
			if(is_array($photos)){
				$data['featured_img'] = $photos[0]->id;
				$blog = $this->blog->update($blog, $data);
			}
		}
		return \Redirect::to($blog->permalink);
	}
	public function showBlogPage($id, $title=""){
		$blog = $this->blog->get($id);
		if(!$blog){
			return \Redirect::to('blogs');
		}
		return view('zucko.blog.details-page')->withBlog($blog);
	}
	public function showEditPage($id){
		$blog = $this->blog->get($id);
		$user = $this->session->getCurrentUser();
		if(!$blog||$blog->author->id!=$user->id){
			return \Redirect::to('blogs');
		}
		return view('zucko.blog.edit-page')->withBlog($blog);
	}
	public function submitEditPage($id){
		$blog = $this->blog->get($id);
		$user = $this->session->getCurrentUser();
		if(!$blog||$blog->author->id!=$user->id){
			return \Redirect::to('blogs');
		}
		$data = \Input::except('_token', 'photo');
		$data['user_id'] = $user->id;
		if(\Input::hasFile('photo')){
			$photos = $this->photo->createFromUpload(\Input::file('photo'), $blog->id, 'Zucko\Blog\Blog', $user->id, $blog->title, 700, 350);
			if(is_array($photos)){	
				$data['featured_img'] = $photos[0]->id;
			}
		}
		$blog = $this->blog->update($blog, $data);
		if(!$blog){
			$data['error'] = $this->blog->errors()->first();
			return \Redirect::back()->with($data);
		}
		return \Redirect::back()->withSuccess('Your blog updated successfully.');
	}
	public function addComment(){
		$data = \Input::except('_token');
		$user = $this->session->getCurrentUser();
		$data['user_id'] = $user->id;
		$comment = $this->blog->createComment($data);
		if(!$comment){
			$data['error'] = $this->blog->errors()->first();
			return \Redirect::back()->with($data);
		}
		return \Redirect::to($comment->permalink);
	}
}