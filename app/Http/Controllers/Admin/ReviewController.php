<?php namespace Zucko\Http\Controllers\Admin;
use Zucko\Core\Admin\User\Services\Session\SessionService;
use Zucko\Core\Business\Repos\BusinessRepo;
use Zucko\Core\Address\Repos\AddressRepo;
use Zucko\Core\Photo\Repos\PhotoRepo;
use Zucko\Core\User\Repos\UserRepo;
use Zucko\Core\Review\Repos\ReviewRepo;
class ReviewController extends AdminController{
	protected $business;
	protected $address;
	protected $photo;
	public function __construct(SessionService $session, ReviewRepo $review, PhotoRepo $photo, BusinessRepo  $business, AddressRepo $address, UserRepo $user){
		$this->business = $business;
		$this->address = $address;
		$this->photo = $photo;
		$this->review = $review;
		parent::__construct($session, $user);
	}
	public function showListPage(){
		$filters = array();
		$filters['sort'] = \Input::get('sort','new');
		$reviews = $this->review->search(\Input::get('search'),$filters,\Input::get('pagging',100),\Input::get('page',1));
		return view('admin.zucko.review.list-reviews')->withReviews($reviews);
	}
	public function showEditPage($id){
		$review = $this->review->get($id);
		if(!$review){
			return \Redirect::back();
		}
		return view('admin.zucko.review.edit-review')->withReview($review);
	}
	public function submitEditPage($id){
		$review = $this->review->get($id);
		if(!$review){
			return \Redirect::back();
		}
		$data = \Input::except('_token');
		$review = $this->review->update($review, $data);
		if(!$review){
			return \Redirect::back()->withError($this->review->errors()->first())->withInput($data);
		}
		return \Redirect::back()->withSuccess("Updated Successfully!");
	}
	public function deleteReview($id){
		$this->review->delete($id);
		return \Redirect::back();
	}
}