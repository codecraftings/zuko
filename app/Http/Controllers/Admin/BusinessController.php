<?php namespace Zucko\Http\Controllers\Admin;
use Zucko\Core\Admin\User\Services\Session\SessionService;
use Zucko\Core\Business\Repos\BusinessRepo;
use Zucko\Core\Address\Repos\AddressRepo;
use Zucko\Core\Photo\Repos\PhotoRepo;
use Zucko\Core\User\Repos\UserRepo;

class BusinessController extends AdminController
{
    protected $business;
    protected $address;
    protected $photo;

    public function __construct(SessionService $session, PhotoRepo $photo, BusinessRepo $business, AddressRepo $address, UserRepo $user)
    {
        $this->business = $business;
        $this->address = $address;
        $this->photo = $photo;
        parent::__construct($session, $user);
    }

    public function showListPage()
    {
        $filters = array();
        $filters['sort'] = \Input::get('sort', 'new');
        $businesses = $this->business->powerSearch(\Input::get('search'), $filters, \Input::get('pagging', 100), \Input::get('page', 1));
        return view('admin.zucko.business.list-business')->withBusinesses($businesses);
    }

    public function listPageAction()
    {
        $business = $this->business->get(\Input::get("business_id"));
        if (!$business) {
            return \Redirect::back();
        }
        switch (\Input::get("action")) {
            case 'set_important':
                $this->business->update($business, ["important" => \Input::get("important", 0)]);
                break;
            case 'set_slider_featured':
                $this->business->update($business, ["slider_featured" => \Input::get("slider_featured", 0)]);
                break;
            default:
                # code...
                break;
        }
        return \Redirect::back();
    }

    public function showEditPage($id)
    {
        $business = $this->business->get($id);
        if (!$business) {
            return \Redirect::to('/');
        }
        \View::share('categories', $this->business->getCategories());
        \View::share('countries', $this->address->getAllCountries());
        \View::share('states', $this->address->getAllStates());
        return view('admin.zucko.business.edit-business')->withBusiness($business);
    }

    public function submitEditPage($id)
    {
        $business = $this->business->get($id);
        if (!$business) {
            return \Redirect::to('/');
        }
        $user = $this->session->getCurrentUser();
        $oldphotos = \Input::get('oldphotos');
        foreach ($business->photos as $key => $photo) {
            if (!$oldphotos || !in_array($photo->id, $oldphotos)) {
                $this->business->removePhoto($business, $photo);
            }
        }
        $data = \Input::except('newphoto', '_token', 'logo');
        if (\Input::hasFile('newphoto')) {
            $photos = $this->photo->createFromUpload(\Input::file('newphoto'), $business->id, 'Zucko\Business\Business', $user->id, $business->name);
            if (!$photos) {
                $error = $this->photo->errors()->first() ?: "Something went wrong";
                return \Redirect::back()->withError($error)->withInput($data);
            }
        }
        if (\Input::hasFile('logo')) {
            $photos = $this->photo->createFromUpload(\Input::file('logo'), $business->id, 'Zucko\Business\Business', $user->id, $business->name, 400, 160);
            if (!$photos) {
                $error = $this->photo->errors()->first() ?: "Something went wrong";
                return \Redirect::back()->withError($error)->withInput($data);
            }
            $data['logo'] = $photos[0]->id;
        }
        if (!($city = $this->address->getCityByName($data['city']))) {
            $city = $this->address->saveCity($data['city']);
        }
        $data['city_id'] = $city->id;
        $opening_hours = \Input::get('day1');
        foreach ($opening_hours as $key => &$hour) {
            $hour = new \stdClass();
            $hour->day1 = \Input::get('day1')[$key];
            $hour->day2 = \Input::get('day2')[$key];
            $hour->time1 = \Input::get('time1')[$key];
            $hour->time2 = \Input::get('time2')[$key];
        }
        $data['opening_hours'] = $opening_hours;
        if (!($business = $this->business->update($business, $data))) {
            $error = $this->business->errors()->first();
            return \Redirect::back()->withError($error)->withInput($data);
        }
        $this->business->changeStatus($business, $data['listing_status']);
        return \Redirect::back()->withSuccess('Your business informations updated successfully.');
    }

    public function deleteBusiness($id)
    {
        $this->business->delete($id);
        return \Redirect::back();
    }
}