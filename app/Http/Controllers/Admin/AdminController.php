<?php namespace Zucko\Http\Controllers\Admin;
use Zucko\Core\Admin\User\Services\Session\SessionService;
use Zucko\Core\User\Repos\UserRepo;
use Zucko\Http\Controllers\Controller;

class AdminController extends Controller{
	protected $session;
	protected $user;
	public function __construct(SessionService $session, UserRepo $user){
		$this->session = $session;
		$this->user = $user;
		if($this->session->isLoggedIn()){
			\View::share('current_user',$this->session->getCurrentUser());
		}
		if($this->session->isAdmin()){
			\View::share('admin', $this->session->getCurrentUser());
		}
	}
	public function showLogin(){
		return view('admin.zucko.dashboard.login');
	}
	public function submitLogin(){
		$data = \Input::except('_token');
		if(!$this->session->login($data['email'],$data['password'])){
			$data['error'] = $this->session->errors()->first();
			return \Redirect::back()->with($data);
		}
		return \Redirect::to(admin_url('dashboard'));
	}
	public function showDashboard(){
		return view('admin.zucko.dashboard.index');
	}
	public function showEditAccount(){
		$admin = $this->session->getCurrentUser();
		return view('admin.zucko.dashboard.edit-account')->withAdmin($admin);
	}
	public function submitEditAccount(){
		$data = \Input::except('_token');
		$admin = $this->session->getCurrentUser();
		$admin = $this->user->update($admin, $data);
		if(!$admin){
			$error = $this->user->errors()->first();
			return \Redirect::back()->withError($error)->withInput($data);
		}
		return \Redirect::back()->withSuccess('Account Updated Successfully!');
	}
	public function logout(){
		$this->session->logout();
		return \Redirect::to(admin_url('/'));
	}
}