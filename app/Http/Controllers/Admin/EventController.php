<?php namespace Zucko\Http\Controllers\Admin;
use Zucko\Core\Admin\User\Services\Session\SessionService;
use Zucko\Core\Event\Repos\EventRepo;
use Zucko\Core\Address\Repos\AddressRepo;
use Zucko\Core\Photo\Repos\PhotoRepo;
use Zucko\Core\User\Repos\UserRepo;
class EventController extends AdminController{
	protected $event;
	protected $address;
	protected $photo;
	public function __construct(SessionService $session, PhotoRepo $photo, EventRepo $event, AddressRepo $address, UserRepo $user){
		$this->event = $event;
		$this->address = $address;
		$this->photo = $photo;
		parent::__construct($session, $user);
	}
	public function showListPage(){
		$filters = array();
		$filters['sort'] = \Input::get('sort','new');
		$events = $this->event->powerSearch(\Input::get('search'),$filters,\Input::get('pagging',100),\Input::get('page',1));
		return view('admin.zucko.event.list-events')->withEvents($events);
	}
	public function showEditPage($id){
		$event = $this->event->get($id);
		if(!$event){
			return \Redirect::to('/');
		}
		return view('admin.zucko.event.edit-event')->withEvent($event);
	}
	public function submitEditPage($id){
		$event = $this->event->get($id);
		if(!$event){
			return \Redirect::to('/');
		}
		$user = $this->session->getCurrentUser();
		$oldphotos = \Input::get('oldphotos');
		foreach ($event->photos as $key => $photo) {
			if(!$oldphotos||!in_array($photo->id, $oldphotos)){
				$this->event->removePhoto($event, $photo);
			}
		}
		$data = \Input::except('newphoto','_token');
		if(\Input::hasFile('newphoto')){
			$photos = $this->photo->createFromUpload(\Input::file('newphoto'), $event->id, 'Zucko\Event\Event', $user->id, $event->title);
			if(!$photos){
				$error = $this->photo->errors()->first()?:"Something went wrong";
				return \Redirect::back()->withError($error)->withInput($data);
			}
		}
		if(!($city = $this->address->getCityByName($data['city']))){
			$city = $this->address->saveCity($data['city']);
		}
		$data['city_id'] = $city->id;
		//dd($data);
		if(!($event = $this->event->update($event, $data))){
			$error = $this->event->errors()->first();
			//dd($this->event->errors());
			return \Redirect::back()->withError($error)->withInput($data);
		}
		$this->event->changeStatus($event, $data['listing_status']);
		return \Redirect::back()->withSuccess('Event updated successfully');
	}
	public function deleteEvent($id){
		$this->event->delete($id);
		return \Redirect::back();
	}
}