<?php namespace Zucko\Http\Controllers;
use Zucko\Core\User\Services\Session\SessionService;
use Zucko\Core\User\Repos\UserRepo;
use Zucko\Core\Services\File\Uploader;
use Illuminate\View\Factory as View;
use Carbon\Carbon;
use Zucko\Core\Services\Mail\Mail;
use Zucko\Core\Address\Repos\AddressRepo;
use Zucko\Core\Photo\Repos\PhotoRepo;
class SessionController extends BaseController{
	protected $user_session;
	protected $user;
	protected $view;
	protected $uploader;
	protected $carbon;
	protected $mail;
	protected $photo;
	public function __construct(UserRepo $user, PhotoRepo $photo, SessionService $user_session, View $view, Uploader $uploader, Carbon $carbon, Mail $mail, AddressRepo $address){
		$this->user = $user;
		$this->user_session = $user_session;
		$this->view = $view;
		$this->uploader = $uploader;
		$this->carbon = $carbon;
		$this->mail = $mail;
		$this->photo = $photo;
		parent::__construct($this->user_session, $address);
	}
	public function showPassReset(){
		if($this->session->isLoggedIn()){
			return \Redirect::to('/');
		}
		return $this->view->make('zucko.user.pass-reset');
	}
	public function submitPassReset(){
		$attempt = $this->session->requestPassReset(\Input::get('email'));
		if(!$attempt){
			return \Redirect::back()->withError($this->session->errors()->first());
		}
		return \Redirect::back()->withSuccess('Please check you email inbox for furthur instructions about how to confirm you password reset.');
	}
	public function showConfirmReset(){
		if($this->session->isLoggedIn()){
			return \Redirect::to('/');
		}
		$token = \Input::get('token');
		$email = \Input::get('email');
		if(empty($token)||empty($email)){
			return \Redirect::to('password/reset');
		}
		return $this->view->make('zucko.user.pass-reset')->with(compact('token','email'));
	}
	public function submitConfirmReset(){
		$token = \Input::get('token');
		$email = \Input::get('email');
		$password = \Input::get('password');
		$password_confirmation = \Input::get('password_confirmation');
		$attempt = $this->session->confirmPassReset($email, $password, $password_confirmation, $token);
		if(!$attempt){
			return \Redirect::back()->withError($this->session->errors()->first());
		}
		return \Redirect::to('login')->withSuccess('Your password has been reset. You can now login with your new password.');
	}
	public function showLogin(){
		if($this->user_session->isLoggedIn()){
			return \Redirect::to('/');
		}
		return $this->view->make('zucko.user.login-page');
	}
	public function submitLogin(){
		if(!$this->user_session->login(\Input::get('email'), \Input::get('password'))){
			$data = \Input::all();
			$data['error'] = $this->user_session->errors()->first();
			return \Redirect::back()->with($data);
		}
		if(\Input::has('pending_url')){
			return \Redirect::to(\Input::get('pending_url'));
		}
		return \Redirect::to('/');
	}
	public function logout(){
		$this->user_session->logout();
		return \Redirect::to('/');
	}
	public function showSignup(){
		if($this->user_session->isLoggedIn()){
			return \Redirect::to('/');
		}
		return $this->view->make('zucko.user.signup-page');
	}
	public function verifyEmail(){
		$token = \Input::get('token');
		$email = \Input::get('email');
		$user = $this->user->verifyEmail($email, $token);
		if(!$user){
			return \Redirect::to('login')->withError($this->user->errors()->first());
		}
		return \Redirect::to('login')->withSuccess('Email verified successfully. You can login now.');
	}
	public function submitSignup(){
		$data = \Input::except('profile_pic');
		$data['birthdate'] = $this->carbon->parse($data['dob-year']."-".$data['dob-month']."-".$data['dob-day']);
		if($user = $this->user->create($data)){
			$this->mail->sendVerificationEmail($user);
			if(\Input::hasFile('profile_pic')){
				$photos = $this->photo->createFromUpload(\Input::file('profile_pic'), $user->id, 'Zucko\User\User', $user->id, $user->nickname, 1000, 800);
				if($photos){
					$this->user->update($user, array('profile_pic'=>$photos[0]->thumb_url));
				}
			}
			return \Redirect::back()->with('info','A link is sent to your email, please click on that link to activate your profile!');
		}else{
			$data['error'] = $this->user->errors()->first();
			return \Redirect::back()->with($data);
		}
	}
}