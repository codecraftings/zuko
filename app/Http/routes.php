<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::pattern('id', '[0-9]+');
Route::get('/', 'PagesController@showHome');
Route::get('page/about', 'PagesController@showAboutPage');
Route::get('page/privacy', 'PagesController@showPrivacyPage');
Route::get('page/terms', 'PagesController@showTermsPage');
Route::get('page/career', 'PagesController@showCareerPage');
Route::get('page/mobile', 'PagesController@showMobilePage');
Route::get('page/contact', 'PagesController@showContactPage');
Route::post('page/contact', 'PagesController@submitContactPage');

Route::get('login', 'SessionController@showLogin');
Route::post('login', 'SessionController@submitLogin');
Route::get('logout', 'SessionController@logout');
Route::get('signup', 'SessionController@showSignup');
Route::post('signup', 'SessionController@submitSignup');
Route::get('verify/email', 'SessionController@verifyEmail');

Route::get('password/reset', 'SessionController@showPassReset');
Route::post('password/reset', 'SessionController@submitPassReset');
Route::get('password/reset/confirm', 'SessionController@showConfirmReset');
Route::post('password/reset/confirm', 'SessionController@submitConfirmReset');

Route::get('user/{id}', 'UserController@showUser');
Route::get('user/{id}/edit', array('middleware' => 'auth', 'uses' => 'UserController@showEditUser'));
Route::post('user/{id}/edit', array('middleware' => 'auth', 'uses' => 'UserController@submitEditUser'));
Route::get('user/{id}/business', array('middleware' => 'auth', 'uses' => 'UserController@showUserBusinesses'));
Route::get('user/{id}/events', array('middleware' => 'auth', 'uses' => 'UserController@showUserEvents'));
Route::get('user/{id}/{nickname}', array('uses' => 'UserController@showUser'));

Route::put('api/users/{id}', array('middleware' => 'auth', 'uses' => 'UserController@submitEditUser'));

Route::get('cities', 'PagesController@showCities');
Route::get('search', 'BusinessController@showSearchPage');

Route::get('business/add', array('middleware' => 'auth', 'uses' => 'BusinessController@showAddOne'));
Route::post('business/add', array('middleware' => 'auth', 'uses' => 'BusinessController@submitAddOne'));
Route::get('business/add/more-info/{id}', array('middleware' => 'auth', 'uses' => 'BusinessController@showAddSecond'));
Route::post('business/add/more-info/{id}', array('middleware' => 'auth',
                                                 'uses'       => 'BusinessController@submitAddSecond'
));
Route::get('business/show/{id}', 'BusinessController@showProfile');
Route::get('business/show/{id}/{name}', 'BusinessController@showProfile');
Route::get('business/{id}/edit', array('middleware' => 'auth', 'uses' => 'BusinessController@showEditPage'));
Route::post('business/{id}/edit', array('middleware' => 'auth', 'uses' => 'BusinessController@submitEditPage'));
//Route::get('business/{id}/reviews/{review_id}', '');
//Route::get('business/from/{city_name}', '');
//Route::get('business/of/{cat_name}', '');
Route::get('business/{id}/reviews', 'BusinessController@showReviews');
Route::post('business/addphotos', array('middleware' => 'auth', 'uses' => 'BusinessController@addPhotosByUser'));

Route::get('reviews/write', array('middleware' => 'auth', 'uses' => 'ReviewsController@showAddPage'));
Route::post('reviews/add', array('middleware' => 'auth', 'uses' => 'ReviewsController@addReview'));
Route::get('reviews/like/{id}', array('middleware' => 'auth', 'uses' => 'ReviewsController@likeReview'));
Route::get('reviews/{id}', 'ReviewsController@showPage');

Route::get('api/reviews/{id}', array('uses' => 'ReviewsController@show'));
Route::post('api/reviews/{review_id}/votes', array('middleware' => 'auth', 'uses' => 'ReviewsController@createVote'));
Route::post('api/reviews/{review_id}/votes/delete/{user_id}/{type}', array('middleware' => 'auth',
                                                                           'uses'       => 'ReviewsController@deleteVote'
));

Route::get('event/add', array('middleware' => 'auth', 'uses' => 'EventController@showAdd'));
Route::post('event/add', array('middleware' => 'auth', 'uses' => 'EventController@submitAdd'));
Route::get('events', array('uses' => 'EventController@listEvents'));
Route::get('event/{id}', array('uses' => 'EventController@showEvent'));
Route::get('event/{id}/{name}', array('uses' => 'EventController@showEvent'));
Route::get('events/from/{city}', 'EventController@showCityEvents');
Route::get('inbox', array('middleware' => 'auth', 'uses' => 'InboxController@showInboxPage'));
Route::post('inbox', array('middleware' => 'auth', 'uses' => 'InboxController@submitInboxAction'));
Route::get('inbox/thread/{id}', array('middleware' => 'auth', 'uses' => 'InboxController@showThreadPage'));
Route::post('inbox/thread/{id}', array('middleware' => 'auth', 'uses' => 'InboxController@submitReply'));
Route::get('inbox/invitations', array('middleware' => 'auth', 'uses' => 'InboxController@showInvitationsPage'));
Route::get('invitations/accept/{friend_id}', array('middleware' => 'auth', 'uses' => 'UserController@acceptFriend'));
Route::get('invitations/decline/{friend_id}', array('middleware' => 'auth', 'uses' => 'UserController@declineFriend'));
Route::get('invitations/send/{friend_id}', array('middleware' => 'auth', 'uses' => 'UserController@addFriend'));
Route::get('inbox/sent', array('middleware' => 'auth', 'uses' => 'InboxController@showSentPage'));
Route::get('inbox/compose', array('middleware' => 'auth', 'uses' => 'InboxController@showCompose'));
Route::post('inbox/compose', array('middleware' => 'auth', 'uses' => 'InboxController@submitCompose'));

Route::get('friends', array('middleware' => 'auth', 'uses' => 'UserController@showFindFriend'));

Route::get('blogs', array('uses' => 'BlogController@showAllBlogs'));
Route::get('blog/add', array('middleware' => 'auth', 'uses' => 'BlogController@showAddPage'));
Route::post('blog/add', array('middleware' => 'auth', 'uses' => 'BlogController@submitAddPage'));
Route::get('blog/edit/{id}', array('middleware' => 'auth', 'uses' => 'BlogController@showEditPage'));
Route::post('blog/edit/{id}', array('middleware' => 'auth', 'uses' => 'BlogController@submitEditPage'));
Route::post('blog/comment/create', array('middleware' => 'auth', 'uses' => 'BlogController@addComment'));
Route::get('blog/{id}/{title}', 'BlogController@showBlogPage');

Route::post('ajax/review/create', array('middleware' => 'auth', 'uses' => 'ReviewsController@ajaxAddReview'));
Route::post('ajax/review/comment', array('middleware' => 'auth', 'uses' => 'ReviewsController@ajaxAddComment'));
Route::post('ajax/review/like', array('middleware' => 'auth', 'uses' => 'ReviewsController@ajaxLikeReview'));
Route::post('ajax/event/interested', array('middleware' => 'auth', 'uses' => 'EventController@ajaxAddInterestedUser'));
Route::post('ajax/event/interested/delete', array('middleware' => 'auth',
                                                  'uses'       => 'EventController@ajaxDeleteInterestedUser'
));

Route::get('test', function () {
    echo preg_replace("/(^|\s)BP\s/i", "$1bp", "BP dfd bp ada");
});
Route::get('import', function () {
    $importer = new Zucko\Business\ImportFromCsv('first.csv', App::make('Zucko\Address\Repos\AddressRepo'), App::make('Zucko\Business\Repos\BusinessRepo'));
    $importer->startImport();
    //DB::unprepared(file_get_contents(base_path().'/predata.sql'));
});
Route::get('loaddb', function () {
    ///DB::unprepared(file_get_contents(base_path().'/resources/db/zucko.sql'));
    if (($handle = fopen(base_path() . '/resources/db/states.txt', "r")) !== false) {
        while (($row = fgetcsv($handle)) !== false) {
            DB::table('states')->insert(array(
                'name'           => $row[0],
                'country_id'     => 75,
                'business_count' => 0
            ));
        }
    }
});
$admin = Config::get('app.admin_dir');
Route::group(array(
    'prefix' => $admin
), function () {
    Route::get('/login', 'AdminController@showLogin');
    Route::post('/login', array('uses' => 'AdminController@submitLogin'));
    Route::group(array(
        'middleware' => 'auth:admin'
    ), function () {
        Route::get('/logout', 'Admin\AdminController@logout');
        Route::get('/', array('uses' => 'Admin\AdminController@showDashboard'));
        Route::get('/dashboard', array('uses' => 'Admin\AdminController@showDashboard'));
        Route::get('/edit-account', 'Admin\AdminController@showEditAccount');
        Route::post('/edit-account', array('uses' => 'Admin\AdminController@submitEditAccount'));

        Route::get('/users/list', array('uses' => 'Admin\UsersController@showListUsers'));
        Route::get('/users/edit/{id}', array('uses' => 'Admin\UsersController@showEditUser'));
        Route::post('/users/edit/{id}', array('uses' => 'Admin\UsersController@submitEditUser'));
        Route::post('/users/delete/{id}', array('uses' => 'Admin\UsersController@deleteUser'));

        Route::get('/business/list', array('uses' => 'Admin\BusinessController@showListPage'));
        Route::post('/business/list', array('uses' => 'Admin\BusinessController@listPageAction'));
        Route::get('/business/edit/{id}', array('uses' => 'Admin\BusinessController@showEditPage'));
        Route::post('/business/edit/{id}', array('uses' => 'Admin\BusinessController@submitEditPage'));
        Route::post('/business/delete/{id}', array('uses' => 'Admin\BusinessController@deleteBusiness'));


        Route::get('events/list', 'Admin\EventController@showListPage');
        Route::get('event/edit/{id}', 'Admin\EventController@showEditPage');
        Route::post('event/edit/{id}', array('uses' => 'Admin\EventController@submitEditPage'));
        Route::post('event/delete/{id}', array('uses' => 'Admin\EventController@deleteEvent'));

        Route::get('reviews/list', 'Admin\ReviewController@showListPage');
        Route::get('review/edit/{id}', 'Admin\ReviewController@showEditPage');
        Route::post('review/edit/{id}', 'Admin\ReviewController@submitEditPage');
        Route::post('review/delete/{id}', 'Admin\ReviewController@deleteReview');

        Route::get('comments/list', 'Admin\CommentController@showListPage');
        Route::get('comment/edit/{id}', 'Admin\CommentController@showEditPage');
        Route::post('comment/edit/{id}', 'Admin\CommentController@submitEditPage');
        Route::post('comment/delete/{id}', 'Admin\CommentController@deleteComment');

    });
});
