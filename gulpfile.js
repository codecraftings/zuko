var elixir = require('laravel-elixir');
var gulp = require('gulp');
var exec = require('child_process').exec;
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix){
   mix.scripts([ "jquery/jquery.js" ], "public/js/build/jquery.min.js");
   mix.scripts([
      "libs/**/*.js",
      "plugins/**/*.js",
      "app/**/*.js"
   ], "public/js/build/all.min.js");

   mix.styles([
      'normalize.css',
      'main.css',
      'style2.css',
      'jquery.bxslider.css',
      'jquery.ui.datepicker.css',
      'jquery.mCustomScrollbar.css',
      'magnific-popup.css'
   ], 'public/css/all.min.css');
   mix.styles([
      'admin/bootstrap.min.css',
      'admin/bootstrap-responsive.min.css',
      'admin/font-awesome.css',
      'admin/style.css',
      'admin/pages/signin.css',
      'admin/pages/dashboard.css'
   ], "public/admin/css/all.min.css");
});

gulp.task("seed", function (){
   commands([
      "php artisan migrate:refresh",
      //"php artisan db:seed --force",
      "php artisan db:seed --force --class=SeedDefaultData",
      "php artisan db:seed --force --class=UsersSeeder",
      "php artisan db:seed --force --class=CitiesSeeder",
      "php artisan db:seed --force --class=BusinessSeeder",
      "php artisan db:seed --force --class=BusinessSeeder",
      "php artisan db:seed --force --class=BusinessSeeder",
      "php artisan db:seed --force --class=ReviewsSeeder",
      "php artisan db:seed --force --class=ReviewsSeeder",
      "php artisan db:seed --force --class=ReviewsSeeder",
      "php artisan db:seed --force --class=ReviewsSeeder",
      "php artisan db:seed --force --class=EventsSeeder",
      "php artisan db:seed --force --class=EventsSeeder",
      "php artisan db:seed --force --class=EventsSeeder",
      "php artisan db:seed --force --class=BlogsSeeder",
      "php artisan db:seed --force --class=BlogsSeeder",
      "php artisan db:seed --force --class=BlogsSeeder",
      "php artisan db:seed --force --class=MessageSeeder",
      "php artisan db:seed --force --class=MessageSeeder",
      "php artisan db:seed --force --class=MessageSeeder",
      "php artisan db:seed --force --class=MessageSeeder",
      "php artisan db:seed --force --class=MessageSeeder"
   ]);
});
function commands(arr){
   var total = arr.length;
   execute(0);
   function execute($n){
      if($n===total){
         return;
      }
      command(arr[$n], function(){
         execute($n+1);
      });
   }
}
function command(commandName, nextCallback){
   nextCallback = nextCallback || function (){};
   var process = exec(commandName, function (error){
      if ( error === null ) {
         nextCallback.call();
      }
   });
   process.stdout.on('data', function (data){
      console.log("output: " + data);
   });
   process.stderr.on('data', function (data){
      console.log("error:  " + data);
   });
}