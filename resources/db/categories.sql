-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2015 at 03:34 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zucko`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `slag` varchar(30) NOT NULL,
  `style` text NOT NULL,
  `business_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slag`, `style`, `business_count`) VALUES
(1, 'Professional', 'pros', '{"color":"arsenic","class": "icon-roulottes"}', 0),
(2, 'Accomodation', 'accomodation', '{"color":"barnred","class": "icon-assistance"}', 0),
(3, 'Shopping', 'shopping', '{"color":"orange","class": "icon-shopping"}', 0),
(7, 'Brand', 'brands', '{"color":"purple","class": "icon-clubs"}', 0),
(8, 'Manager', 'managers', '{"color":"blue","class": "icon-bars"}', 0),
(10, 'Mobile', 'mobiles', '{"color":"red","class": "icon-hotels"}', 0),
(11, 'Transport', 'transports', '{"color":"lightblue","class": "icon-transports"}', 0),
(15, 'Sport', 'sports', '{"color":"green","class": "icon-sports"}', 0),
(16, 'Restaurant', 'restaurants', '{"color":"deepblue","class": "icon-restaurants"}', 0),
(17, 'Tourism', 'tourism', '{"color":"green","class": "icon-hobbies"}', 0),
(18, 'Administration', 'adminstration', '{"color":"arsenic","class": "icon-roulottes"}', 0),
(19, 'Adm Manager', 'adm-manager', '{"color":"lightred","class": "icon-tie"}', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
