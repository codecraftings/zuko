-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2015 at 03:34 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zucko`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE IF NOT EXISTS `blog_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `slag` varchar(50) NOT NULL,
  `style` varchar(150) NOT NULL,
  `blog_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `blog_categories`
--

INSERT INTO `blog_categories` (`id`, `name`, `slag`, `style`, `blog_count`) VALUES
(1, 'Roulottes', 'roulottes', '{"color":"arsenic","class": "icon-roulottes"}', 0),
(2, 'Quick Assistance', 'quick-assistance', '{"color":"barnred","class": "icon-assistance"}', 0),
(3, 'Shopping', 'shopping', '{"color":"orange","class": "icon-shopping"}', 0),
(7, 'Night Clubs', 'night-clubs', '{"color":"purple","class": "icon-clubs"}', 0),
(8, 'Bars', 'bars', '{"color":"blue","class": "icon-bars"}', 0),
(10, 'Hotels', 'hotels', '{"color":"red","class": "icon-hotels"}', 0),
(11, 'Transports', 'transports', '{"color":"lightblue","class": "icon-transports"}', 0),
(16, 'Restaurants', 'restaurants', '{"color":"deepblue","class": "icon-restaurants"}', 0),
(17, 'Hobbies', 'hobbies', '{"color":"green","class": "icon-hobbies"}', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
