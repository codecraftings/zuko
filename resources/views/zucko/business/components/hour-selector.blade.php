   
<a href="javascript:void()" class="add-hour-btn"><i class="icon-plus"></i></a>
   @foreach($business->meta->opening_hours as $key=>$hour)
   <div style="margin:5px 0px 0px 0px;" class="fl hour-selector">
      <select name="day1[]" class="input input-medium">
        <option{{$hour->day1=="sun"?" selected":""}} value="sun">Sunday</option>
        <option{{$hour->day1=="mon"?" selected":""}} value="mon">Monday</option>
        <option{{$hour->day1=="tue"?" selected":""}} value="tue">Tuesday</option>
        <option{{$hour->day1=="wed"?" selected":""}} value="wed">Wednessday</option>
        <option{{$hour->day1=="thu"?" selected":""}} value="thu">Thursday</option>
        <option{{$hour->day1=="fri"?" selected":""}} value="fri">Friday</option>
        <option{{$hour->day1=="sat"?" selected":""}} value="sat">Saturday</option>
    </select>
    To 
    <select name="day2[]" class="input input-medium">
        <option{{$hour->day2=="sun"?" selected":""}} value="sun">Sunday</option>
        <option{{$hour->day2=="mon"?" selected":""}} value="mon">Monday</option>
        <option{{$hour->day2=="tue"?" selected":""}} value="tue">Tuesday</option>
        <option{{$hour->day2=="wed"?" selected":""}} value="wed">Wednessday</option>
        <option{{$hour->day2=="thu"?" selected":""}} value="thu">Thursday</option>
        <option{{$hour->day2=="fri"?" selected":""}} value="fri">Friday</option>
        <option{{$hour->day2=="sat"?" selected":""}} value="sat">Saturday</option>
    </select>

    <input type="text" name="time1[]" value="{{$hour->time1}}" class="input input-xxxsmall time-input" placeholder="00" /> AM To &nbsp;&nbsp;<input name="time2[]" type="text" placeholder="00" value="{{$hour->time2}}" class="input input-xxxsmall time-input" /> PM
    <a href="javascript:void()" class="minus-btn {{$key==0?'hidden':''}}"><small>Remove</small></a>
    <br>
</div>
@endforeach
<div class="cf"></div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.hour-selector .minus-btn').click(function(e) {
          $(e.target).parents('.hour-selector').remove();
        });

        $('.add-hour-btn').click(function(event) {
          var b = $('.hour-selector').last().clone();
          b.find('.minus-btn').show();
          $('.hour-selector').parent().append(b);
          $('.hour-selector .minus-btn').click(function(e) {
            $(e.target).parents('.hour-selector').remove();
          });
        });
    });
</script>