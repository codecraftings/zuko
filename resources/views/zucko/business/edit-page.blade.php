       @extends('layouts.main')
       @section('footer-scripts')
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5y_HSH5nG4nXBclv13VrRZ5b81S5cRZc&amp;sensor=false"></script>
        <script type="text/javascript" src="{{url('js/map.js')}}"></script>
       @stop
       @section('window-title')
       {{$business->name}} | @parent
       @stop
       @section('main-content')
       <h2 class="title-stripped"><span class="text">Add Business</span></h2>
       <p>
         Your business is probably already registerd on our website but nevertheless here is your unique opportunity
         to make something different by filling the forms on pages 1 and 2.
         As a result, you will increase your visibility and obvisously get more customers as well!
       </p>
       <br>
       <form id="lddbd_add_business_form" enctype="multipart/form-data" autocomplete="off" method="post">      
         <div class="form-container edit-page">
           <div class="inside">
             <h3>Create Business</h3>
       @if(Session::has('info'))
       <div class="msg">{{Session::get('info')}}</div>
       @endif
       @if(Session::has('success'))
       <div class="msg success">{{Session::get('success')}}</div>
       @endif
       @if(Session::has('error'))
       <div class="msg error">{{Session::get('error')}}</div>
       @endif
             <div class="blue-header">
               <?php
               $listing_type = Session::get('listing_type')?:$business->listing_type;
               ?>
               <input type="radio" id="littypeoonbe" name="listing_type" value="place" class="radio" {{$listing_type=='place'?'checked':''}} > 
               <label for="littypeoonbe" class="text1">Local Business or Place</label> 
               <input id="littypeoonbesdf" type="radio" name="listing_type" value="product" class="radio" {{$listing_type=='product'?'checked':''}}>
               <label for="littypeoonbesdf" class="text1">Product or Brand</label> 
               <input id="littypeoonbesdfwr" type="radio" name="listing_type" value="company" class="radio" {{$listing_type=='company'?'checked':''}}> 
               <label for="littypeoonbesdfwr" class="text1">Company, Institution or Organization</label>
             </div>
             <div class="cf"></div>
             <div class="form-field half zmargi">
              <span class="label">Business Name</span>
              <input type="text" name="name" placeholder="Company, Institution or Organization" value="{{Session::get('name')?:$business->name}}" class="input input-full required" maxlength="20" />
            </div>
            <div class="form-field half zmargi" >
              <span class="label">Business Country</span>
              <select  id="country_name" name="country" class="input input-full">
               <option value="">Select Country</option>
               <?php
               $country_id = Session::get('country')?:$business->country;
               ?>
               @foreach($countries as $country)
               <option value="{{$country->name}}" {{$country_id==$country->name?"selected":""}}>{{$country->name}}</option>
               @endforeach
             </select>
           </div>
           <div class="cf"></div> 
           <div class="form-field half zmargi">
            <span class="label">Address</span>
            <input value="{{Session::get('address')?:$business->address}}" type="text" name="address" placeholder="Business Address" class="input input-full" />
          </div>      
          <div class="form-field half zmargi">
            <span class="label">City</span>
            <input value="{{Session::get('city')?:$business->city}}" type="text" name="city" placeholder="City/Town" class="input input-full" maxlength="20" />
          </div>    
          <div class="cf"></div> 
          <div class="form-field half zmargi">
            <span class="label">Island</span>
            <select class="input input-full" id="island_name" name="state" >
             <?php
             $state_id = Session::get('state')?:$business->state;
             ?>
             <option value="">Select your island</option>
             @foreach($states as $state)
             <option value="{{$state->name}}" {{$state_id==$state->name?"selected":""}}>{{$state->name}}</option>
             @endforeach
           </select>
         </div>
         <div class="form-field half zmargi">
          <span class="label">Zip Code</span>
          <input value="{{Session::get('zip_code')?:$business->zip_code}}" type="text" name="zip_code" id="zip_code" placeholder="Boite postale or Zip Code " class="input input-full required" maxlength="20" />
        </div>  
        <div class="cf"></div> 
        <div class="form-field half zmargi">
          <span class="label">Email</span>
          <input value="{{Session::get('email')?:$business->meta->email}}" type="text" name="email" placeholder="Email Address" class="input input-full email" />
        </div>      
        <div class="form-field half zmargi">
          <span class="label">Fax</span>
          <input value="{{Session::get('fax')?:$business->meta->fax}}" type="text" name="fax" placeholder="Fax" class="input input-full" maxlength="30"/>
        </div>    
        <div class="cf"></div> 
        <div class="form-field half zmargi">
          <span class="label">Type of Company</span>
          <?php
          $company_type = Session::get('company_type')?:$business->company_type;
          ?>
          <select class="input input-full" name="company_type">
            <option value="" {{$company_type==""?"selected":""}}>Type of company</option>
            <option value="SARL" {{$company_type=="SARL"?"selected":""}}>SARL</option>
            <option value="EURL" {{$company_type=="EURL"?"selected":""}}>EURL</option>
            <option value="SELARL" {{$company_type=="SELARL"?"selected":""}}>SELARL</option>
            <option value="SA" {{$company_type=="SA"?"selected":""}}>SA</option>
            <option value="SAS" {{$company_type=="SAS"?"selected":""}}>SAS</option>
            <option value="SASU" {{$company_type=="SASU"?"selected":""}}>SASU</option>
            <option value="SNU" {{$company_type=="SNU"?"selected":""}}>SNU</option>
            <option value="SCP" {{$company_type=="SCP"?"selected":""}}>SCP</option>

          </select>
        </div>      
        <div class="form-field half zmargi">
          <span class="label">Website</span>
          <input value="{{Session::get('website')?:$business->meta->website}}" type="text" name="website" placeholder="Website" class="input input-full" />
        </div>    
        <div class="cf"></div> 
        <div class="form-field half zmargi">
          <span class="label">Numero De Tahiti</span>
          <input value="{{Session::get('tahiti_numero')?:$business->tahiti_numero}}" type="text"  placeholder="Numero De Tahiti (If you are in Tahiti)" class="input input-full"  name="tahiti_numero" maxlength="20" />
        </div>      
        <div class="form-field half zmargi">
          <span class="label">Tahiti RC</span>
          <input value="{{Session::get('tahiti_rc')?:$business->tahiti_rc}}" type="text"  placeholder="RC ( If you are in Tahiti)" class="input input-full" name="tahiti_rc" maxlength="20" />
        </div>    
        <div class="cf"></div>
        <div class="form-field half zmargi">
          <span class="label">Facebook Page</span>
          <input type="text" value="{{Session::get('fb_url')?:$business->meta->fb_url}}"  placeholder="Facebook Page Url" class="input input-full" name="fb_url" maxlength="30" />
        </div>  
        <div class="form-field half zmargi">
          <span class="label">Phone Number</span>
          <input value="{{Session::get('phone')?:$business->meta->phone}}" type="text" name="phone" placeholder="Business Phone Number" class="input input-full" maxlength="20" />
        </div>    
        <div class="cf"></div>   

        <div class="form-field half zmargi">
          <span class="label">Twitter Url</span>
          <input value="{{Session::get('twitter_url')?:$business->meta->twitter_url}}" type="text"  placeholder="Twitter Page Url" class="input input-full" name="twitter_url" maxlength="30" />
        </div>    

        <div class="form-field half zmargi">
          <span class="label">Youtube Url</span>
          <input value="{{Session::get('youtube_url')?:$business->meta->youtube_url}}" type="text"  placeholder="Youtube Channel Url" class="input input-full" name="youtube_url" maxlength="30" />
        </div>    
        <div class="cf"></div> 

        <div class="form-field half zmargi">
          <span class="label">Google Plus Url</span>
          <input value="{{Session::get('gplus_url')?:$business->meta->gplus_url}}" type="text"  placeholder="Googleplus Page Url" class="input input-full" name="gplus_url" maxlength="30" />
        </div> 
        <div class="form-field half zmargi">
          <span class="label">Linkedin Page</span>
          <input type="text"  value="{{Session::get('linkedin_url')?:$business->meta->linkedin_url}}" placeholder="Linkedin Url" class="input input-full" name="linkedin_url" maxlength="30" />
        </div>    
        <div class="cf"></div>
        <div class="form-field half zmargi">
          <span class="label">Type of Activity</span>
          <select class="input input-full required" name="category_id">
            <?php
            $category_id = Session::get('category_id')?:$business->category_id;
            ?>  
            <option value="">Type of activity </option>
            @foreach($categories as $category)
            <option value="{{$category->id}}" {{$category_id==$category->id?"selected":""}}>{{$category->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-field half zmargi">
          <span class="label">Your Responsibility</span>
          <?php
          $responsibility = Session::get('responsibility')?:$business->responsibility;
          ?>
          <select class="input input-full" name="responsibility">
           <option value="">Your Responsibility </option>
           <option value="owner" {{$responsibility=="owner"?"selected":""}}>Owner</option>
           <option value="director" {{$responsibility=="director"?"selected":""}}>Director</option>
           <option value="manager" {{$responsibility=="manager"?"selected":""}}>Manager</option>
         </select>
       </div>
       <div class="form-field half">
           <div class="input-container">
               <input type="file" name="logo" style="display:none;" id="imagefileinput">
               <input type="text" class="input input-long" value="Upload a Logo" disabled placeholder="" />
               <div class="input-addon"><button style="font-size: 13px; padding: 3px 11px;" onclick="$('#imagefileinput').click();" type="button" class="red-button">Browse</button></div>
           </div>
       </div>
       <small style="color:#404040;font-size:11px;margin:-10px 0px 8px;display:block;">Leave blank if you dont want to change your business logo.</small>
       <script type="text/javascript">
           jQuery(document).ready(function($) {
               $('input[type="file"]').change(function(e) {
                 var files = e.target.files;
                 if(files.length>0){
                   $(e.target).parent().find('input[type="text"]').val(files[0].name);
                 }else{
                   $(e.target).parent().find('input[type="text"]').val("Upload a Logo");
                 }
               });
           });
       </script>
       <div class="cf"></div>
       <div style="position:relative;" class="form-field full">
        <span class="label">Opening hours</span> 
        <div class="cf"></div>
        @foreach($business->meta->opening_hours as $key=>$hour)
        <div style="margin:5px 0px 0px 0px;" class="fl hour-selector">
         <select name="day1[]" class="input input-medium">
           <option{{$hour->day1=="sun"?" selected":""}} value="sun">Sunday</option>
           <option{{$hour->day1=="mon"?" selected":""}} value="mon">Monday</option>
           <option{{$hour->day1=="tue"?" selected":""}} value="tue">Tuesday</option>
           <option{{$hour->day1=="wed"?" selected":""}} value="wed">Wednessday</option>
           <option{{$hour->day1=="thu"?" selected":""}} value="thu">Thursday</option>
           <option{{$hour->day1=="fri"?" selected":""}} value="fri">Friday</option>
           <option{{$hour->day1=="sat"?" selected":""}} value="sat">Saturday</option>
         </select>
         To 
         <select name="day2[]" class="input input-medium">
           <option{{$hour->day2=="sun"?" selected":""}} value="sun">Sunday</option>
           <option{{$hour->day2=="mon"?" selected":""}} value="mon">Monday</option>
           <option{{$hour->day2=="tue"?" selected":""}} value="tue">Tuesday</option>
           <option{{$hour->day2=="wed"?" selected":""}} value="wed">Wednessday</option>
           <option{{$hour->day2=="thu"?" selected":""}} value="thu">Thursday</option>
           <option{{$hour->day2=="fri"?" selected":""}} value="fri">Friday</option>
           <option{{$hour->day2=="sat"?" selected":""}} value="sat">Saturday</option>
         </select>

         <input type="text" name="time1[]" value="{{$hour->time1}}" class="input input-xxxsmall time-input" placeholder="00" /> AM To &nbsp;&nbsp;<input name="time2[]" type="text" placeholder="00" value="{{$hour->time2}}" class="input input-xxxsmall time-input" /> PM
         <a href="javascript:void()" class="minus-btn {{$key==0?'hidden':''}}"><small>Remove</small></a>
         <br>
       </div>
       @endforeach
       <a href="javascript:void()" class="add-hour-btn"><i class="icon-plus"></i></a>
       <div class="cf"></div>
     </div>
     <div class="cf"></div>
     <div class="form-field half">
       <span class="label">Good for groups?</span>
       {{Form::select('good_for_groups', array('1'=>'Yes','0'=>'No'), Session::get('good_for_groups')?:$business->meta->good_for_groups, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Water services?</span>
       {{Form::select('water_service', array('1'=>'Yes','0'=>'No'), Session::get('water_service')?:$business->meta->water_service, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Credit cards accepted?</span>
       {{Form::select('credit_card_accepted', array('1'=>'Yes','0'=>'No'), Session::get('credit_card_accepted')?:$business->meta->credit_card_accepted, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Parking Valet?</span>
       {{Form::select('parking_valet', array('1'=>'Yes','0'=>'No'), Session::get('parking_valet')?:$business->meta->parking_valet, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Wheelchair accessible?</span>
       {{Form::select('wheelchair_accessible', array('1'=>'Yes','0'=>'No'), Session::get('wheelchair_accessible')?:$business->meta->wheelchair_accessible, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <input type="hidden" name="price_currency" value="xpf">
       <span class="label">Price Range(In XPF)</span> <input maxlength="5" type="text" placeholder="000" value="{{Session::get('price_low')?:$business->meta->price_low}}" class="input input-xxsmall" name="price_low"/> To <input placeholder="000" value="{{Session::get('price_high')?:$business->meta->price_high}}" maxlength="5" type="text" name="price_high" class="input input-xxsmall"/>
     </div>
     <div class="form-field half">
       <span class="label">Outdoor seating?</span>
       {{Form::select('outdoor_seating', array('1'=>'Yes','0'=>'No'), Session::get('outdoor_seating')?:$business->meta->outdoor_seating, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Good for kids?</span>
       {{Form::select('good_for_kids', array('1'=>'Yes','0'=>'No'), Session::get('good_for_kids')?:$business->meta->good_for_kids, array('class'=>'input input-medium'))}}
     </div>
    <div class="form-field half">
       <span class="label">Food Type </span>
       {{Form::select('food_type', array('none'=>'Not Aplicable','tahitian'=>'Tahitian Food','chinese'=>'Chinese Food','french'=>'French Food','bbq'=>'BBQ Food','sea'=>'Sea Food','meat'=>'Meat Food'), Session::get('food_type')?:$business->food_type, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Good for </span>
       {{Form::select('good_for', array('none'=>'Not Aplicable','breakfast'=>'Breakfast','lunch'=>'Lunch','dinner'=>'Dinner'), Session::get('good_for')?:$business->meta->good_for, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Take reservations?</span>
       {{Form::select('take_reservation', array('1'=>'Yes','0'=>'No'), Session::get('take_reservation')?:$business->meta->take_reservation, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Alcohol?</span>
       {{Form::select('alcohol', array('1'=>'Yes','0'=>'No'), Session::get('alcohol')?:$business->meta->alcohol, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Delivery?</span>
       {{Form::select('delivery', array('1'=>'Yes','0'=>'No'), Session::get('delivery')?:$business->meta->delivery, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">To go?</span>
       {{Form::select('to_go', array('1'=>'Yes','0'=>'No'), Session::get('to_go')?:$business->meta->to_go, array('class'=>'input input-medium'))}}
     </div>
     <div class="form-field half">
       <span class="label">Dogs allowed?</span>
       {{Form::select('dogs_allowed', array('1'=>'Yes','0'=>'No'), Session::get('dogs_allowed')?:$business->meta->dogs_allowed, array('class'=>'input input-medium'))}}
     </div>
     <div class="cf"></div>
     <div class="form-field full editpage maps-container">
       <span class="label">Let us know your business exact position from this map:</span><br>
       <?php
        $latitude = Session::get('latitude')?:$business->latitude;
        $longitude = Session::get('longitude')?:$business->longitude;
       ?>
     <input type="hidden" id="latitude" name="latitude" value="{{$latitude}}" />
     <input type="hidden" id="longitude" name="longitude" value="{{$longitude}}" />  
       <div data="{{$latitude}},{{$longitude}}" id="map-canvas"></div>
       <small style="color:#404040;font-size:11px;margin:-18px 0px 8px;display:block;">Your gps position helps us to recommend your business to the users nearby</small>
     </div>
     <div class="form-field full editpage textarea">
       <span class="label">Description:</span><br>
       <textarea name="description" placeholder="Description" class="input input-full">{{Session::get('description')?:$business->description}}</textarea>
     </div> 
     <div class="cf"></div>
     <div class="form-field full">
       <span class="label">Pictures:</span><br>
       @if(count($business->photos)<1)
       <div class="img-placeholder fl"><img src="{{url('img/no_image.jpg')}}" /></div>
       @else
       @foreach($business->photos as $photo)
       <div class="img-placeholder fl"><img src="{{$photo->url}}" /><a class="cross-btn">x</a>
         <input type="hidden" name="oldphotos[]" value="{{$photo->id}}">
       </div>
       @endforeach
       @endif
       <div>
         <input multiple type="file" class="hidden" id="photosinput" name="newphoto[]"/>
         <div id="addphotosbtn" class="img-placeholder fl"><i class="icon-plus"></i></div>
         <div class="cf"></div>
       </div>
     </div>   
     <div class="cf"></div>
     <div class="cf"></div>
     <div style="width: 100%; margin-top: 10px;text-align:right;">
      <input type="submit" class="red-button" name="add" value="Submit Your Business" style="font-size:16px;float:none;text-transform:none;" />
    </div>


    <div class="cf"></div>

  </div>

</div>
</form>
<script type="text/javascript">
  (function($){
    $(document).ready(function(){
      $('.hour-selector .minus-btn').click(function(e) {
        $(e.target).parents('.hour-selector').remove();
      });

      $('.add-hour-btn').click(function(event) {
        var b = $('.hour-selector').last().clone();
        b.find('.minus-btn').show();
        $('.hour-selector').parent().append(b);
        $('.hour-selector .minus-btn').click(function(e) {
          $(e.target).parents('.hour-selector').remove();
        });
      });
      $('.img-placeholder .cross-btn').click(function(){
        $(this).parents('.img-placeholder').remove();
      })
      $('#addphotosbtn').click(function(){
        $('#photosinput').click();
      });
      $('#photosinput').change(function(e){
        var p = $(this).parent();
        var c = p.find('.img-placeholder').last().clone();
        var files = e.target.files;
        var len = files.length;
        p.find('.img-placeholder').remove();
        c.click(function(){
          $('#photosinput').click();
        });
        p.prepend(c);
        if(len>10){
          alert('Sorry you cannot select more than 10 files at once!');
          $(e.target).val('');
          return;
        }
        for(var i=0;i<len;i++){
          var reader = new FileReader();
          reader.onload = function(e){
            var b = c.clone();
            b.html('<img src="'+e.target.result+'">');
            p.prepend(b);
          }
          reader.readAsDataURL(files[i]);
        }
      });
    });
     })(jQuery);
   </script>
   @stop
   @section('sidebar')
   @include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
   @stop
