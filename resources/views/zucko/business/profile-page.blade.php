@extends('layouts.business-profile')
@section('extra-scripts')
@stop
@section('window-title')
{{$business->name}} | @parent
@stop
@section('footer-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5y_HSH5nG4nXBclv13VrRZ5b81S5cRZc&amp;sensor=false"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.slider .wrapper').magnificPopup({
			type: 'image',
			gallery: {
				enabled: true
			},
			delegate: 'ul li img'
		});
		if($('#static-map')){
			google.maps.event.addDomListener(window, 'load', function(){
				var mapOptions = {
					zoom: 16,
					center: new google.maps.LatLng({{$business->latitude}},{{$business->longitude}}),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};

				var map = new google.maps.Map(document.getElementById('static-map'),
					mapOptions);
				var marker = new google.maps.Marker({
					position: map.getCenter(),
					map: map,
					draggable: false,
					title: 'You Are Here'
				});
			});
		}
		$('.slider .wrapper ul').bxSlider({
		    auto: true,
		    pause: 3000,
		    autoHover: true,
		    infiniteLoop: true,
		    speed: 400,
		    slideWidth: 150,
		    slideSelector: 'li.slide',
		    minSlides: 2,
		    maxSlides: 4,
		    moveSlides: 4,
		    slideMargin: 4,
		    easing: 'easeOutBounce',
		    pager: false,
		    controls: true,
		    nextSelector: '.right-arrow',
		    prevSelector: '.left-arrow',
		    nextText: '<span style="display:inline-block;width:20px;height:20px;background:none;cursor:pointer"></span>',
		    prevText: '<span style="display:inline-block;width:20px;height:20px;background:none;cursor:pointer"></span>'
		});
		$('#addmorephotosinp').change(function(e){
		  var files = e.target.files;
		  var len = files.length;
		  if(len>10){
		    alert('Sorry you cannot select more than 10 files at once!');
		    $(e.target).val('');
		    return;
		  }
		  $('#addphotosform')[0].submit();
		});
	});
</script>
@stop
@section('main-content')
<div class="company_content_top">
	@if(!$business->is_published())
	<div class="msg info">
	{{trans("business.line1")}}
	</div>
	@endif
	<div class="">
		@if($business->logo)
		<div class="">
		<img width="200px" height="80px" src="{{$business->_logo()->url}}"/>
		</div>
		@endif
		<div class="fl">
			<h2 title="{{$business->name}}" style="color: rgb(191, 13, 20);">{{$business->present()->title(35)}}</h2>
			<div class="reviews-info">
				<div data-score="{{$business->present()->rating()}}" class="rating"></div>
				<p><a href="{{$business->review_page}}">{{$count = $business->reviews()->count()}} {{trans_choice("app.reviews", $count)}}</a></p>
			</div>
		</div>
		<div style="color: rgb(64, 64, 64); margin: 21px 11px 0px 0px; width: 218px;" class="business-category blue fr">
			<b class="blue">{{trans("app.Category")}}:</b>&nbsp; {{$business->present()->category_name()}} <i class="category_icon hidden" alt=""></i>
		</div>
		<div class="cf"></div>
	</div><!--END hote-->
	<div class="cf" style="margin-top: 30px;">
		<div class="fl">
			<address style="font-size: 18px;">
				<strong>{{$business->address}}</strong><br>
				{{$business->present()->city_name()}}, {{$business->present()->state_name()}} {{$business->zip_code}}<br>
				{{trans("business.Phone Number")}}: {{$business->present()->phone()}}
			</address>
		</div>
		<div class="fr">
			<p style="color:#828282;">{{trans("business.Follow This Business On")}}</p>
			<div style="width: 228px; margin-top: -26px;" class="stay-connected">
				<div class="social-icons-container">
					<a href="{{$business->meta->fb_url?:'javascript:void();'}}" class="{{empty($business->meta->fb_url)?'inactive ':''}}social-blue fb"></a>
					<a href="{{$business->meta->twitter_url?:'javascript:void();'}}" class="{{empty($business->meta->twitter_url)?'inactive ':''}}social-blue twitter"></a>
					<a href="{{$business->meta->youtube_url?:'javascript:void();'}}" class="{{empty($business->meta->youtube_url)?'inactive ':''}}social-blue youtube"></a>
				</div>
			</div>
		</div>
		<div class="cf"></div>
	</div><!--END category-->
	<div class="cf"></div>
	@include('common.small-slider',array('photos'=>$business->sliderPhotos()))
	<div style="margin-top:10px;" class="">
	<form method="post" id="addphotosform" enctype="multipart/form-data" action="{{url('business/addphotos')}}">
		<input type="hidden" name="business_id" value="{{$business->id}}"/>
		<input type="file" id="addmorephotosinp" class="hidden" name="photos[]" multiple/>
		<a onclick="$('#addmorephotosinp').click()" href="{{isset($current_user)?'javascript:void()':url('login')}}" class="red-button fr">{{trans("business.ADD PHOTOS")}}</a>
	</form>
	<div class="cf"></div>
	</div>
</div>
<div style="margin-bottom:20px;" class="cf"></div>
<h2 class="blue-header">Other Informations</h2>
<div class="business_info">
	<div class="coloumn">
		@if(count($business->meta->opening_hours)>0)
		<p>{{trans("business.Hours")}}:</p>
		@foreach($business->meta->opening_hours as $hour)
		<p>{{ucfirst($hour->day1).'-'.ucfirst($hour->day2).' '.$hour->time1.' a.m. - '.$hour->time2.' pm'}}</p>
		@endforeach
		@endif
		<p>{{trans("business.Good for groups")}}: {{yes_or_no($business->meta->good_for_groups)}}</p>
		<p>{{trans("business.Credit card accepted")}}: {{yes_or_no($business->meta->credit_card_accepted)}}</p>
		<p>{{trans("business.Parking Valet")}}: {{yes_or_no($business->meta->parking_valet)}}</p>
	</div><!--END coloumn-->
	<div class="coloumn">
		<p>{{trans("business.Price Range")}}: {{$business->meta->price_low}} to {{$business->meta->price_high}} XPF</p>
		<p>{{trans("business.Good for kids")}}: {{yes_or_no($business->meta->good_for_kids)}}</p>
		<p>{{trans("business.Take reservations")}}: {{yes_or_no($business->meta->take_reservation)}}</p>
		<p>{{trans("business.Delivery")}}: {{yes_or_no($business->meta->delivery)}}</p>
		<p>{{trans("business.To go")}}: {{yes_or_no($business->meta->to_go)}}</p>
		<p>{{trans("business.Water Services")}}: {{yes_or_no($business->meta->water_service)}}</p>
		<p>{{trans("business.Parking Valet")}}: {{yes_or_no($business->meta->parking_valet)}}</P>
		</div><!--END coloumn-->
		<div class="coloumn">
			<p>{{trans("business.Wheelchair accessible")}}: {{yes_or_no($business->meta->wheelchair_accessible)}}</p>
			<p>{{trans("business.Outdoor seating")}}: {{yes_or_no($business->meta->outdoor_seating)}}</p>
			<p>{{trans("business.Good for")}}: {{ucwords($business->meta->good_for)}}</p>
			<p>{{trans("business.Alcohol")}}: {{yes_or_no($business->meta->alcohol)}}</p>
			<p>{{trans("business.Dogs allowed")}}: {{yes_or_no($business->meta->dogs_allowed)}}</p>
		</div><!--END coloumn-->
		<div class="cf"></div>
	</div><!--END address-->
	<div class="cf"></div>
	<div class="button-group">
		<a href="{{$business->review_page}}" class="red-button">{{trans("review.Write a Review")}}</a>
		@if(isset($current_user)&&$business->user_id==$current_user->id)
		<a href="{{$business->edit_page}}" class="red-button">{{trans("business.Edit This Business")}}</a>
		@endif
	</div><!--END four_buttons-->
	<div class="cf"></div>
	@if(!empty($business->latitude)&&!empty($business->longitude))
	<div class="geolocation">
		<b style="color:#808080;">{{trans("business.We are here!")}}</b>
		<div id="static-map" data=""></div>
	</div>
	<script type="text/javascript">
		(function($){
			
		})(jQuery)
	</script>
	@else
	<div class="geolocation">
		<b style="color:#808080;">Map not available for this business</b>
	</div>
	@endif
	<div id="reviews">
		<h3 class="blue-header">{{Lang::choice("app.Reviews", 2)}} By Users</h3>
		<div style="padding: 0px 0px 20px;" class="reviews-container business-profile">
			@include('zucko.review.components.list-reviews-business', array('business'=>$business))
		</div>
	</div>
	@stop
	@section('sidebar')
	<div class="profile-widget video">
		@if(empty($business->meta->video_url))
		<img src="{{url('img/no_video.jpg')}}" alt="" />
		@if(isset($current_user)&&$current_user->id==$business->user_id)
		<center><a href="{{url('page/contact')}}">{{trans("business.Contact us to add video")}}</a></center>
		@endif
		@else
		<iframe class="business-video" src="{{$business->meta->video_url}}"></iframe>
		@endif
	</div><!--END video-->
	<div class="profile-widget compliments">
		<h2 class="heading">{{trans("business.Related Businesses")}}<i class="grey_arrow fr"></i></h2>
		@foreach($related_businesses as $business)
		<div class="list-item">
			<div class="img-container img-container-curve left">
				<img src="{{$business->photos()->count()>0?$business->photos->first()->thumb_url:url('img/no_image.jpg')}}" alt="sample" />
			</div>
			<div class="text_box left">
				<h5><a href="{{$business->profile_url}}">{{$business->present()->title(20)}}</a></h5>
				<p>{{$business->address}}, {{$business->present()->city_name()}}</p>
			</div>
			<div class="cf"></div>
		</div>
		@endforeach
	</div><!--END spcial offers1-->
	@stop