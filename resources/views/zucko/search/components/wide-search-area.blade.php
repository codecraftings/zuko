<div class="row search-section">
    <div class="container">
        <form id="mainsearchform" action="{{url('search')}}" method="get">
            <div class="field">
                <input type="text" value="{{Input::get('find')}}" name="find" placeholder="{{trans("header.search_what")}}" />
                @if(isset($query))
                <div class="extrafilters">
                <input type="hidden" id="searchfieldsorting" value="{{$query->sorting}}" name="sorting" placeholder="" />
                <input type="hidden" id="searchfielddistance" value="{{$query->distance}}" name="distance" placeholder="" />
                @foreach($query->features as $feature)
                <input type="hidden" class="searchfieldfeature" value="{{$feature}}" name="features[]" placeholder="" />
                @endforeach
                @foreach($query->prices as $price)
                <input type="hidden" class="searchfieldprice" value="{{$price}}" name="prices[]" placeholder="" />
                @endforeach
                @foreach($query->cats as $cat)
                <input type="hidden" class="searchfieldcats" value="{{$cat}}" name="cats[]" placeholder="" />
                @endforeach
                <input type="hidden" id="searchfieldpagging" value="{{$query->pagging}}" name="pagging" placeholder="" />
                </div>
                @endif
                <div class="tooltip">{{trans("header.search_what_caption")}} <span class="arrow-up"></span></div>
            </div>
            <div class="field">
                <input type="text" value="{{Input::get('from')}}" name="from" placeholder="{{trans("header.search_where")}}" />
                <div class="tooltip">{{trans("header.search_where_caption")}}<span class="arrow-up"></span></div>
            </div>
            <div class="field">
                <button type="submit" class="search-btn"><i class="search-icon"></i>{{trans("header.search_btn")}}</button>
            </div>
        </form>
        <div class="cf"></div>
    </div>
</div>