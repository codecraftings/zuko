@extends('layouts.user-profile')
@section('window-title')
{{$user->present()->name()}} | Events | @parent
@stop
@section('right-sidebar')
@include('zucko.user.components.left-sidebar',array('user'=>$user))
@stop
@section('middle-content')
<div class="user-recent-feeds cf">
<h5 class="blue-header-title">Events Added By {{$user->present()->name()}}</h5>
	@include('zucko.event.components.list-user-events',array('events'=>$user->events()->paginate(Input::get('pagging',10))))
</div>
@stop
@section('left-sidebar')
@include('zucko.user.components.right-sidebar', array('user'=>$user))
@stop
