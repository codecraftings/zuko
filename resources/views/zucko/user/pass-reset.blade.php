@extends('layouts.main')
@section('window-title')
Forgot Password | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">Password Reset Form</span>
</h2>
<p></p>
<br>
<div class="form-container">
    <div class="inside">
        <h3>
            Password Reset
        </h3>
        @if(Session::has('info'))
        <div class="msg">{{Session::get('info')}}</div>
        @endif
        @if(Session::has('success'))
        <div class="msg success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
        <div class="msg error">{{Session::get('error')}}</div>
        @endif
        <form name="passreset-form" id="passresetform" method="post" action="" autocomplete="off">
            @if(isset($token))
            <div class="form-field half">
                <input class="input input-long" type="password" value="{{Session::get('password')}}" name="password" placeholder="New Password" />
            </div>
            <div class="form-field half">
                <input class="input input-long" type="password" value="{{Session::get('password_confirmation')}}" name="password_confirmation" placeholder="Confirm New Password" />
            </div>
            <input type="hidden" value="{{$email}}" name="email"/>
            <input type="hidden" value="{{$token}}" name="token"/>
            @else
            <div class="form-field full">
                <input class="input input-fluid" type="text" value="{{Session::get('email')}}" name="email" placeholder="Enter Your Email" />
            </div>
            @endif
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="submit" name="submit" class="red-button fr" value="Submit" />
            <div class="cf"></div>
        </form>
    </div>
</div>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop