@extends('layouts.user-profile')
@section('window-title')
{{$user->present()->name()}} | @parent
@stop
@section('right-sidebar')
@include('zucko.user.components.left-sidebar',array('user'=>$user))
@stop
@section('middle-content')
<div class="recent_views cf">
	<p style="float:left;">{{trans("review.Recent Reviews")}}</p><a href="">{{$count = $user->reviews()->count()}} <span style="color:#808080;float:none;margin-left:5px">{{Lang::choice("app.Reviews", $count)}}</span></a>
	<div style="vertical-align:middle;margin:-3px 0px 0 0;max-width:193px;" class="input-container fr">
	<form action="">
	<input class="input" name="search" value="{{Input::get('search')}}" type="text" placeholder="{{Lang::choice("app.Reviews", 2)}}"/>
	</form>
	<div style="top:7px;right:10px;" class="input-addon"><i class="icon_search"></i></div></div>
	<div class="cf" style="margin: 0 0 7px 0;padding:0px"></div>
	<span class="span">{{trans("app.Sort By")}}:&nbsp;&nbsp;</span><i class="left_icon"></i><a href="?sort=date"><span class="date">date</span></a><i class="left_icon"></i><a href="?sort=useful"><span class="date">{{trans("app.most useful")}}</span></a><i class="left_icon"></i><a href="?sort=funny"><span class="date">{{trans("app.most funny")}}</span></a>
</div><!--END description-->
<div class="user-recent-feeds cf">
<h5 class="blue-header-title">{{trans("review.Recent Reviews")}}</h5>
	@include('zucko.review.components.list-users-reviews',array('reviews'=>$reviews))
</div>
@stop
@section('left-sidebar')
@include('zucko.user.components.right-sidebar', array('user'=>$user))
@stop
