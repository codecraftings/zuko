@extends('layouts.main')
@section('window-title')
	{{trans("header.find_friends")}} | @parent
@stop
@section('main-content')
<h2 class="title-stripped"><span class="text">{{trans("header.find_friends")}}</span></h2>
<div class="main-text">
	<p> 
	</p>
</div>
<!-- Main Content Here -->
<h3 class="blue-header-title">{{trans("app.Find friends from different parts of your life")}}</h3>

<div class="instruction-text">
	<p>
		{{trans("lines.Use the checkboxes below to discover people you know from your hometown, school, employer and more.")}}
	</p>
</div>

<div class="filters find_friend_filters fl">
	<form method="get" action="">
	<!--
		<h4>Date&nbsp; of Birth</h4>
		<div style="width: 235px;" class="input-container">
			<input class="input datepicker" value="{{Input::get('dob')}}" name="dob" placeholder="Pick a date" type="text">
			<div class="input-addon"><i class="icon-calender2 show-datepicker-button"></i></div>
		</div>
		-->
		<h4>{{trans("app.Place")}}</h4>
		<input type="text" class="input" name="city" value="{{Input::get('city')}}" placeholder="{{trans("app.Enter another city")}}" /><br/>
		<h4>{{trans("app.Complete Name")}}</h4>
		<input type="text" class="input" name="name" value="{{Input::get('name')}}" placeholder="{{trans("app.Enter Name")}}" />
		<input type="submit" class="red-button" value="{{trans("app.FIND FRIEND")}}" />
	</form>
</div><!--END info-->
<div class="friend_suggestion fr">
@if(count($friends)<1)
<div class="nothing-found-msg">
Ops no friend found!
</div>
@endif
@foreach($friends as $friend)
	<div class="single_suggestion">
		<div class="friends_suggestion_pic fl">
			<img style="width:60px;height:60px;" src="{{$friend->profile_pic}}" alt="friend" />
		</div><!--END friends_suggestion_pic-->
		<div class="friend_about fr">
			<h4><a href="{{$friend->profile_url}}">{{$friend->present()->name()}}</a></h4>
			<p style="margin-top:0px;font-size:13px;">
				{{trans("app.Living in")}} {{$friend->city_name?:"Unknown City"}}
			</p>
			<p style="margin-top:2px;font-size:13px;">
			@if(!isset($current_user)||!$friend->is_friend($current_user->id))
			<a style="margin-right:7px;" href="{{url('invitations/send/'.$friend->id)}}">Send a friend Request</a>
			@endif
			<a href="{{url('inbox/compose?user='.$friend->nickname)}}">{{trans("app.Send Message")}}</a>

			</p>
		</div><!--END friend_about-->
		<div class="cf"></div>
	</div><!--END single_suggestion-->
@endforeach
</div><!--END friend_suggestion-->
<div style="margin-bottom:30px;" class="cf"></div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.datepicker').datepicker({
			dateFormat: 'dd-mm-yy'
		});
		$('.show-datepicker-button').click(function(event) {
			$('.datepicker').datepicker('show');
		});
	});
</script>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop