@extends('layouts.user-profile')
@section('window-title')
Edit | {{$user->present()->name()}} | @parent
@stop
@section('right-sidebar')
@include('zucko.user.components.left-sidebar',array('user'=>$user))
@stop
@section('middle-content')
<div class="user-recent-feeds cf">
	<h5 class="blue-header-title">{{trans("app.Edit Your Profile")}}</h5>
	@if(Session::has('info'))
	<div class="msg">{{Session::get('info')}}</div>
	@endif
	@if(Session::has('success'))
	<div class="msg success">{{Session::get('success')}}</div>
	@endif
	@if(Session::has('error'))
	<div class="msg error">{{Session::get('error')}}</div>
	@endif
	<form style="" class="edit-profile-form" method="post" action="" enctype="multipart/form-data" autocomplete="off">
	<div class="form-field full">
			<input class="input input-fluid" value="{{Session::get('first_name')?:$user->first_name}}" type="text" name="first_name" placeholder="{{trans("app.First Name")}}" />
		</div>
		<div class="form-field full">
			<input class="input input-fluid" value="{{Session::get('last_name')?:$user->last_name}}" type="text" name="last_name" placeholder="{{trans("app.Last Name")}}" />
		</div>
		<div class="form-field full">
			{{trans("app.Birthday")}}
			<select name="dob-month" class="input input-medium">
				<option value="0">{{trans("app.Month")}}</option>
				<option{{$user->birthdate->month==1?' selected':""}} value="1">{{trans("app.January")}}</option>
				<option{{$user->birthdate->month==2?' selected':""}} value="2">{{trans("app.February")}}</option>
				<option{{$user->birthdate->month==3?' selected':""}} value="3">{{trans("app.March")}}</option>
				<option{{$user->birthdate->month==4?' selected':""}} value="4">{{trans("app.April")}}</option>
				<option{{$user->birthdate->month==5?' selected':""}} value="5">{{trans("app.May")}}</option>
				<option{{$user->birthdate->month==6?' selected':""}} value="6">{{trans("app.June")}}</option>
				<option{{$user->birthdate->month==7?' selected':""}} value="7">{{trans("app.July")}}</option>
				<option{{$user->birthdate->month==8?' selected':""}} value="8">{{trans("app.August")}}</option>
				<option{{$user->birthdate->month==9?' selected':""}} value="9">{{trans("app.September")}}</option>
				<option{{$user->birthdate->month==10?' selected':""}} value="10">{{trans("app.October")}}</option>
				<option{{$user->birthdate->month==11?' selected':""}} value="11">{{trans("app.November")}}</option>
				<option{{$user->birthdate->month==12?' selected':""}} value="12">{{trans("app.December")}}</option>
			</select> 
			<select name="dob-day" class="input input-xsmall2">
				<option value="0">{{trans("app.Day")}}</option>
				@for($i=1;$i<=31;$i++)
				<option{{$user->birthdate->day==$i?' selected':''}} value="{{$i}}">{{$i}}</option>
				@endfor
			</select>
			<select name="dob-year" class="input input-medium">
				<option value="0">{{trans("app.Year")}}</option>
				@for($i=2020;$i>=1920;$i--)
				<option{{$user->birthdate->year==$i?' selected':''}} value="{{$i}}">{{$i}}</option>
				@endfor
			</select>
		</div>
		<div class="form-field full">
			<input class="input input-xmedium"  value="{{Session::get('nickname')?:$user->nickname}}" type="text" name="nickname" placeholder="{{trans("app.Nick Name")}}" />
			<label>
				<input{{$user->hide_name?' checked':''}} name="hide_name" value="1" type="checkbox"/>
				<small style="display:inline-block;float:right;text-align: center;font-size:10px;width:140px;height:20px;line-height: 10px;">
					{{trans("app.If you click here, users will only see your nick name, your first and last name will be hidden")}}.
				</small>
			</label>
			<div class="cf"></div>
		</div>
		<div class="form-field full">
			<span class="label">{{trans("app.Gender")}}:</span> <label class="radio"><input name="gender" type="radio"{{$user->gender=='MALE'?' checked':''}} value="MALE" /> {{trans("app.Male")}}</label> &nbsp;&nbsp;<label class="radio"><input name="gender" type="radio"{{$user->gender=='FEMALE'?' checked':''}} value="FEMALE" /> {{trans("app.Female")}}</label>
		</div>
		<div class="form-field full">
			<input class="input input-fluid" value="{{Session::get('zip_code')?:$user->zip_code}}" type="text" name="zip_code" placeholder="{{trans("app.Zip Code")}}" />
		</div>
		<div class="form-field full">
			<input class="input input-fluid" value="{{Session::get('city_name')?:$user->city_name}}" type="text" name="city_name" placeholder="{{trans("app.City Name")}}" />
		</div>
        <div class="form-field full">
        	<input class="input input-fluid" value="{{Session::get('email')?:$user->email}}" type="text" name="email" placeholder="{{trans("app.Enter your email address")}}" />
        </div>
		<small style="color:#404040;font-size:11px;margin:-2px 0px 8px;display:block;">Change your pasword. If not interested, just leave it blank.</small>
		<div class="form-field full">
			<input class="input input-fluid" value="{{Session::get('oldpassword')}}" type="password" name="oldpassword" placeholder="{{trans("app.Old Password")}}" />
		</div>
		<div class="form-field full">
			<input class="input input-fluid" value="{{Session::get('password')}}" type="password" name="password" placeholder="New Password" />
		</div>
		<div class="form-field full">
			<input class="input input-fluid" value="{{Session::get('password_confirmation')}}" type="password" name="password_confirmation" placeholder="{{trans("app.Confirm New Password")}}" />
		</div>
        <small style="color:#404040;margin:-6px 0px 4px;display:block;">Change your profile picture. If not interested, just leave it untouched.</small>
        <div class="form-field full">
        	<div class="input-container">
        		<input type="file" name="profile_pic" style="display:none;" id="signuppagefileinput">
        		<input type="text" class="input input-fluid" value="{{trans("app.Upload Picture")}}" disabled placeholder="" />
        		<div class="input-addon"><button onclick="$('#signuppagefileinput').click();" type="button" class="blue-button">Browse</button></div>
        	</div>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('input[type="file"]').change(function(e) {
                  var files = e.target.files;
                  if(files.length>0){
                    $(e.target).parent().find('input[type="text"]').val(files[0].name);
                  }else{
                    $(e.target).parent().find('input[type="text"]').val("Upload Picture");
                  }
                });
            });
        </script>
        <div class="cf"></div>
        <input type="submit" name="submit" class="red-button submit-btn fr" value="Submit" />
        <div class="cf"></div>
    </form>
</div>
@stop
@section('left-sidebar')
@include('zucko.user.components.right-sidebar', array('user'=>$user))
@stop
