@extends('layouts.main')
@section('window-title')
    {{trans("app.Register")}} | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">{{trans("app.Register")}}</span>
</h2>
<p>
    {{trans("register.form1")}}
</p><br>
<form method="post" action="{{url('signup')}}" enctype="multipart/form-data" autocomplete="off">
    <div style="margin-bottom:7px;" class="form-container">
        <div class="inside">
            <h3>
                {{trans("register.Register Here")}}
            </h3>
            @if(Session::has('info'))
            <div class="msg">{{Session::get('info')}}</div>
            @endif
            @if(Session::has('success'))
            <div class="msg success">{{Session::get('success')}}</div>
            @endif
            @if(Session::has('error'))
            <div class="msg error">{{Session::get('error')}}</div>
            @endif
            <div class="form-field half">
                <input class="input input-long" value="{{Session::get('first_name')}}" type="text" name="first_name" placeholder="{{trans("register.First Name")}}" />
            </div>
            <div class="form-field half">
                <input class="input input-long" value="{{Session::get('last_name')}}" type="text" name="last_name" placeholder="{{trans("register.Last Name")}}" />
            </div>
            <div class="form-field half">
                Birthday 
                <select name="dob-month" class="input input-xsmall2">
                    <option value="0">{{trans("register.Month")}}</option>
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select> 
                <select name="dob-day" class="input input-xsmall2">
                    <option value="0">{{trans("register.Day")}}</option>
                    @for($i=1;$i<=31;$i++)
                    <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
                <select name="dob-year" class="input input-xsmall2">
                    <option value="0">{{trans("register.Year")}}</option>
                    @for($i=2020;$i>=1920;$i--)
                    <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
            </div>
            <div class="form-field half">
                <input class="input input-medium"  value="{{Session::get('nickname')}}" type="text" name="nickname" placeholder="{{trans("register.Nick Name")}}" />
                <label>
                    <input name="hide_name" type="checkbox"/>
                    <small style="display:inline-block;float:right;text-align: center;font-size:10px;width:140px;height:20px;line-height: 10px;">
                        {{trans("register.form2")}}
                    </small>
                </label>
                <div class="cf"></div>
            </div>
            <div class="form-field half">
                <input class="input input-long" value="{{Session::get('password')}}" type="password" name="password" placeholder="{{trans("register.Password")}}" />
            </div>
            <div class="form-field half">
                <input class="input input-long" value="{{Session::get('password_confirmation')}}" type="password" name="password_confirmation" placeholder="{{trans("register.Confirm Password")}}" />
            </div>
            <div class="form-field half">
                <span style="width:140px;" class="label">{{trans("register.Gender")}}:</span> <label class="radio"><input name="gender" type="radio" value="MALE" /> {{trans("register.Male")}}</label> <label class="radio"><input name="gender" type="radio" value="FEMALE" /> {{trans("register.Female")}}</label>
            </div>
            <div class="form-field half">
                <input class="input input-long" value="{{Session::get('zip_code')}}" type="text" name="zip_code" placeholder="{{trans("register.Zip Code")}}" />
            </div>
            <div class="form-field half antibot-check">
                {{trans("register.Security Code")}}<br>
                <div class="antibot-code-container fl">
                <!--
                    <img class="fl" src="img/sample-code.png" />
                    <a href="#" style="display: inline-block; margin-top: -3px;" class="fl"><i class="icon-sound"></i></a>
                    <a href="#" style="display: inline-block; margin-top: -5px;" class="fl"><i class="icon-refresh"></i></a>
                    <div class="cf"></div>
                -->
                {{ Recaptcha::render() }}
            </div>
            <div class="cf"></div>
        </div>
        <div class="form-field half">
            <input class="input input-long" value="{{Session::get('city_name')}}" type="text" name="city_name" placeholder="City Name" />
        </div>
        <div class="form-field half">
            <input class="input input-long" value="{{Session::get('email')}}" type="text" name="email" placeholder="{{trans("register.Email Address")}}" />
        </div>
        <div class="form-field half">
            <input class="input input-long" value="{{Session::get('email_confirmation')}}" type="text" name="email_confirmation" placeholder="{{trans("register.Confirm Email Adress")}}" />
        </div>
        <div class="form-field half">
            <div class="input-container">
                <input type="file" name="profile_pic" style="display:none;" id="signuppagefileinput">
                <input type="text" class="input input-long" value="Upload Photos" disabled placeholder="" />
                <div class="input-addon"><button style="font-size: 13px; padding: 3px 11px;" onclick="$('#signuppagefileinput').click();" type="button" class="red-button">{{trans("register.Browse")}}</button></div>
            </div>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('input[type="file"]').change(function(e) {
                  var files = e.target.files;
                  if(files.length>0){
                    $(e.target).parent().find('input[type="text"]').val(files[0].name);
                  }else{
                    $(e.target).parent().find('input[type="text"]').val("Upload Picture");
                  }
                });
            });
        </script>
        <div class="cf"></div>
        <div class="cf"></div>
    </div>
    <div class="cf"></div>
</div>
<div style="max-width:510px;" class="notice fl">
<p class="term-notice"> {{trans("register.form4")}}<a href="{{url('page/terms')}}">{{trans("register.Terms of Service")}}</a> {{trans("app.and")}} <a href="{{url('page/privacy')}}">{{trans("register.Privacy Policy")}}</a>.</p>
</div>
<input style="padding: 7px 30px;" type="submit" name="submit" class="red-button fr" value="{{trans("register.Submit")}}" />
<div style="margin-bottom:60px;" class="cf"></div>
</form>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop