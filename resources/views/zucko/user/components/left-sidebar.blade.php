<div class="pro_pic">
	<img src="{{$user->profile_pic}}" alt="pro_pic" />
	<h2>{{$user->present()->name()}}</h2>
</div><!--END pro_pic-->
<div data-enabled="{{isset($current_user)&&$current_user->id==$user->id?'yes':'no'}}" class="mood-status">
<p style="margin-bottom:10px;padding: 2px 20px 2px 10px;;position:relative;" class="blue-header-title status-text">
		<medium>“{{$user->present()->mood_status()}}”</medium>
		<a href="javascript:void(0);" class="sprite edit_icon"></a>
	</p>
	<div class="update-status-area hidden">
		<form autocomplete="off" class="update-status-form">
			<input type="hidden" name="user_id" value="{{$user->id}}" />
			<textarea placeholder="Write something about you..." maxlength="80" class="update-status-input"></textarea>
			<a href="javascript:void(0);" class="sprite tick_icon update-status-submit"></a>
		</form>
	</div>
</div>
<div class="user-right-widget cf">
	<a href="#" class="header"><i id="friends" class="user-icon"></i>{{$count = $user->friends()->count()}} {{Lang::choice('app.Friends', $count)}} </a><br/>
	<a href="{{$user->profile_url}}" class="header"><i id="reviews-icon" class="user-icon"></i>{{$count = $user->reviews()->count()}} {{Lang::choice('app.reviews', $count)}} </a><br/>
	<a href="#" class="header"><i id="local" class="user-icon"></i>{{$count = $user->photos()->count()}} {{Lang::choice('app.photos', $count)}}</a><br/>
	<!--
	<a href="#" class="header"><i id="updates" class="user-icon"></i>Reviews Updates </a><br/>
	<a href="#" class="header"><i id="firsts" class="user-icon"></i>16 Firsts </a><br/>
	<a href="#" class="header"><i id="tips" class="user-icon"></i>76 Tips </a><br/>
	<a href="#" class="header"><i id="lists" class="user-icon"></i>2 Lists </a>
-->
</div><!--END description-->
<div class="user-right-widget options cf">
	@if(!isset($current_user)||$current_user->id!=$user->id)
	@if(!isset($current_user)||!$user->is_friend($current_user->id))
	<a href="{{url('invitations/send/'.$user->id)}}" class="header"><i id="add_friend" class="user-icon"></i>Send A Friend Request<i class="grey_arrow fr"></i> </a><br/>
	@endif
	<a href="{{url('inbox/compose?user='.$user->nickname)}}" class="header"><i id="message" class="user-icon"></i>Send Message<i class="grey_arrow fr"></i> </a><br/>
	@endif
	@if(isset($current_user)&&$current_user->id==$user->id)
	<a href="{{url('user/'.$user->id.'/edit')}}" class="header"><i id="updates" class="user-icon"></i>Edit Your Profile<i class="grey_arrow fr"></i> </a><br/>
	<a href="{{url('user/'.$user->id.'/business')}}" class="header"><i id="updates" class="user-icon"></i>Edit Your Businesses<i class="grey_arrow fr"></i> </a><br/>
	<a href="{{url('user/'.$user->id.'/events')}}" class="header"><i id="updates" class="user-icon"></i>Edit Your Events<i class="grey_arrow fr"></i> </a><br/>
	@endif
</div><!--END options-->
<div class="user-right-widget">
	<h4 class="heading"> {{Lang::choice('app.Friends', $user->friends()->count())}}</h4>
	<div class="small-img-grid cf">
		@foreach($user->friends as $friend)
		<div class="img-container img-container-square fl">
			<a href="{{$friend->profile_url}}"><img src="{{$friend->profile_pic}}" alt="friends" /></a>
		</div>
		@endforeach
	</div>
</div><!--END options-->