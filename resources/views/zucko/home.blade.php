@extends('layouts.main')
@section('body-top')
<div class="row blue-header">
    <div class="container">
        <h2 class="fl">{{trans("home.slider_headline")}}</h2>
        <span class="blue-arrow-down"></span>
        <span style="margin-top:11px;" class="fr">
            <span class="slider-prev"></span>
            <span class="slider-next"></span>
        </span>
        <div class="cf"></div>
    </div>
</div>
<div class="row gallery">
    <div class="container">
        <div class="thumbnails-wrapper">
            <div class="thumbnails">
                @foreach($popular_businesses as $business)
                <div class="slide thumbnail homepage-thumbnail">
                    <div class="img-container img-container-square">
                        <img src="{{$business->photos()->count()>0?$business->photos->first()->thumb_url:url('img/no_image.jpg')}}" />
                        <p class="caption"><a href="{{$business->profile_url}}">{{$business->present()->title(20)}}</a></p>
                    </div>
                    <div class="details">
                        <div data-score="{{$business->present()->rating()}}" class="rating fl"></div>
                        <div class="review-count fr"><a href="{{$business->review_page}}">{{$count = $business->reviews()->count()}} {{Lang::choice("home.reviews", $count)}}</a></div>
                        <div class="cf"></div>
                    </div>
                </div>
                @endforeach
                <div class="cf"></div>
            </div>
        </div>
        <div class="cf"></div>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.thumbnails').bxSlider({
                    auto: true,
                    pause: 3000,
                    autoHover: true,
                    infiniteLoop: true,
                    speed: 600,
                    slideWidth: 256,
                    slideSelector: '.slide',
                    minSlides: 4,
                    maxSlides: 4,
                    moveSlides: 4,
                    slideMargin: 14,
                    easing: 'easeOutBounce',
                    pager: false,
                    controls: true,
                    nextSelector: '.slider-next',
                    prevSelector: '.slider-prev',
                    nextText: '<i class="blue-white-arrow-right"></i>',
                    prevText: '<i class="blue-white-arrow-left"></i>'
                });
            });
        </script>
    </div>
</div>
@stop
@section('main-content')
<div id="tabs1" class="tabs">
    <ul class="tabs-menu">
        <li><a class="tabs-menu-button" href="#tabs-category">{{trans("home.Top Categories")}}</a></li>
        <li><a class="tabs-menu-button" href="#tabs-events">{{trans("home.Popuplar Events")}}</a></li>
    </ul>
    <div class="zucko-tabs-container">
        <div id="tabs-category">
            <ul class="category-list">
                @foreach($categories as $category)
                @if(isset($category->style->color))
                <li>
                    <a href="{{$category->page_url}}" class="category color-{{$category->style->color}}"><i class="icon {{$category->style->class}}"></i>{{trans("cat.".$category->name)}}</a>
                </li>
                @endif
                @endforeach
            </ul>
        </div>
        <div id="tabs-events">
            @include('zucko.event.components.list',array('events'=>$top_events))
        </div>
    </div>
</div>
<div id="tabs2" class="tabs">
    <ul class="tabs-menu">
        <li><a class="tabs-menu-button" href="#activity-tab">{{trans("home.Recent Reviews")}}</a></li>
        <li><a class="tabs-menu-button" href="#review-tab">{{trans("home.Review of the Day")}}</a></li>
    </ul>
    <div class="zucko-tabs-container">
        <div style="padding-top:20px" id="activity-tab">
            @include('zucko.review.components.list-reviews',array('reviews'=>$recent_reviews))
        </div>
        <div style="padding-top:20px" id="review-tab">
            @include('zucko.review.components.list-reviews',array('reviews'=>$top_reviews))
        </div>
    </div>
</div>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
@stop

