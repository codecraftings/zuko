<div class="events-list">
	@foreach($events as $event)
	<div data-event='{{$event->withUserInteractions(isset($current_user)?$current_user:false)->toJson(JSON_HEX_APOS|JSON_HEX_QUOT)}}' class="event cf backbone-event-view">
		<div class="event_photo fl cf">
			<img src="{{$event->present()->photoSrc()}}" alt="event_photo" />
		</div><!--END event_photo-->
		<div class="event_info fr cf">
			<div class="info_details">
				<h4><a style="font-size: 15px;" href="{{$event->profile_url}}">{{$event->present()->show_title()}}</a> </h4>
				<p class="small-desc">
					{{$event->present()->desc(120, '...')}}
					<br>
					<a class="show_more_btn" href="{{$event->profile_url}}">{{trans("event.show more")}}</a>
				</p>
				<div style="border: medium none; padding: 0px; margin: 0px 8px 0px 0px;" class="event-details full hidden">
					@if($event->embedded_video)
					<div style="margin-top:6px;" class="event-video">
						<iframe style="width:85%;" src="{{$event->embedded_video}}"></iframe>
					</div>
					@endif
					<p class="">
						{{nl2br($event->present()->desc(2000))}}
					</p>
					<div style="font-size:13px;" class="event-metas">
						<span>Event Website: <a href="{{$event->link}}">{{$event->link}}</a></span>
						<div class="cf"></div>
					</div><!--END interested-->
					@if($event->photos->count()>1)
					<div class="photos">
						@include('common.small-slider',array('photos'=>$event->photos))
					</div>
					@endif
					<small><a class="show_less_btn" href="javascript:void()">show less</a></small>
				</div>
			</div>

			<div class="event_meta">
				<div style="max-width:180px;height:46px;overflow:hidden;" class="fl">
					<small style="margin-left: 0px;"><span class="stats-interested">{{$event->interested_count}}</span><a href="{{isset($current_user)?'javascript:void(0)':url('login')}}" class="vote-interested"> <i class="interested_icon"></i></a></small>
					<br>
					<small>{{trans("event.Submitted by")}} <a href="{{$event->user->profile_url}}">{{$event->user->nickname}}</a></small>
				</div>
				<div style="min-width:265px;max-width:300px;height:46px;overflow:hidden;" class="fr">
				<small>{{trans("event.Location")}}: {{$event->present()->show_location(30)}}</small>
				<br>
				<small class="">{{$event->event_time->formatLocalized("%A %d %B, %Y %I:%M %p")}}</small>
				</div>
				<div class="cf"></div>
			</div><!--END interested-->
		</div><!--END event_info-->
	</div><!--END event-->
	@endforeach
</div>
