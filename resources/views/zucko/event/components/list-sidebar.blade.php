@foreach($events as $key=>$event)
<div class="news-item {{($key+1)==count($events)?'last':''}}">
    <div class="img-container img-container-curve fl">
        <img src="{{$event->present()->photoSrc()}}" />
    </div>
    <div class="details fl">
        <h5><a href="{{$event->profile_url}}">{{$event->present()->show_title(15)}}</a></h5>
        <p>{{ $event->present()->desc(50) }}</p>
    </div>
    <div class="cf"></div>
</div>
@endforeach