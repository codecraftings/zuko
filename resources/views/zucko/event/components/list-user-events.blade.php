<div class="events-list">
@foreach($events as $event)
<div class="event cf">
	<div class="event_info fr cf">
		<div class="info_details">
			<h4><a href="{{$event->profile_url}}">{{$event->present()->show_title()}}</a> </h4>
			<p>
				{{nl2br($event->present()->desc(120))}}
			</p>
		</div>

		<div class="event_meta">
			<div class="fl">
			<small>Location: {{$event->present()->show_location(30)}}</small>
			<div class="cf"></div>
			<small class="">{{$event->event_time->format('l j F Y h:i A')}}</small>
			</div>
			<a href="{{url('event/'.$event->id.'/edit')}}" style="margin:8px 0px 0px 0px;" class="red-button fr">Edit Event</a>
			<div class="cf"></div>
		</div><!--END interested-->
	</div><!--END event_info-->
</div><!--END event-->
@endforeach
{{$events->render()}}
</div>