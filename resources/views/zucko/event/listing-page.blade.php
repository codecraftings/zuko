@extends('layouts.main')
@section('main-content')
@section('window-title')
	{{trans("header.events")}} | @parent
@stop
@section('footer-scripts')
<script type="text/javascript">
	jQuery(document).ready(function($) {
		var $button = $(".datepicker-btn");
		var left = $button.offset().left,
		top = $button.offset().top + $button.height();

		$('.datepicker-inp').datepicker({
			dateFormat: 'dd/mm/yy',
			onSelect: function(dateText) {
				$('.datepicker-inp').hide();
				window.location.href = "{{url_append_query('date=custom')}}&custom_date="+dateText;
			},
			beforeShow: function (event, ui) {
			}
		});

		$button.on("click", function() {
			$(".datepicker-inp").toggle();
		    //.datepicker("show");
		});
	});
</script>
@stop
<h2 class="title-stripped">
	<span class="text">{{trans("header.events")}}</span>
</h2>
<div class="list_below_heading">
	<h3>{{trans("event.Looking for a certain event? Search Below")}}</h3>
</div>
<div style="margin:0px 0px 30px;" class="business_form">
	<form autocomplete="off" action="">
		<div class="field fl">
			<div class="tool_tip">
				<span class="red">{{trans("event.Event Title")}}</span>
				<span class="down-arrow"></span>
			</div>
			<input placeholder="{{trans("event.Event Name")}}" value="{{Input::get('name')}}" name="name" style="margin-top:30px;" class="input input-long">
		</div>
		<div class="field fr">
			<div class="tool_tip">
				<span class="red">{{trans("event.Place")}}</span> ({{trans("event.Address, Neighborhood, City or Zip Code")}})
				<span class="down-arrow"></span>
			</div>
			<input placeholder="{{trans("event.Place")}}" name="place" value="{{Input::get('place')}}" style="margin-top:30px;float:" class="input input-long">
		</div>
		<div class="field cf">
			<input style="margin: 10px 0px 0px 0px;font-size:18px;padding:5px 20px;" type="submit" class="yellow-button fr" value="{{trans("event.Search Events")}}"/>
		</div>
		<div class="cf"></div>
	</form>
</div>
<div class="cf"></div>
<h3 class="blue-header-title">Find Your Events Below</h3>
<div class="events-filter">
	{{trans("event.See events for")}}
	<span class="allred">
		<a class="{{Input::get('date')=='all'?'active':''}}" href="{{url_append_query('date=all')}}">All</a> |
		<a class="{{Input::get('date')=='today'?'active':''}}" href="{{url_append_query('date=today')}}">{{trans("events.Today")}}</a> |
		<a class="{{Input::get('date')=='tomorrow'?'active':''}}" href="{{url_append_query('date=tomorrow')}}">{{trans("events.Tomorrow")}}</a> |
		<a class="{{Input::get('date')=='weekend'?'active':''}}" href="{{url_append_query('date=weekend')}}">{{trans("events.This Weekend")}}</a> |
		<a class="{{Input::get('date')=='this_week'?'active':''}}" href="{{url_append_query('date=this_week')}}">{{trans("events.This Week")}}</a> |
		<a class="{{Input::get('date')=='next_week'?'active':''}}" href="{{url_append_query('date=next_week')}}">{{trans("events.Next Week")}}</a>
		<span style="position:relative">
			<a class="datepicker-btn"><i class="icon-calender2"></i></a>
			@if(Input::get('date')=="custom")
			<span style="margin-left:5px;" class="active">{{Input::get('custom_date')}}</span>
			@endif
			<span style="position:absolute;top:24px;left:-212px;" class="datepicker-inp hidden"></span>
		</span>
	</span>
</div>
<div class="events-container">
	@if(count($events)<1)
	<div class="nothing-found-msg"> 
		Ops no events found!
	</div>
	@endif
	@include('zucko.event.components.list', array('events'=>$events));
	{{$events->appends(Input::only('name','place','pagging','date','custom_date'))->render()}}
</div> 
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop