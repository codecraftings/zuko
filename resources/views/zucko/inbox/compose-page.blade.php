@extends('layouts.main')
@section('window-title')
Send A Message | {{Input::get('user')}} | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">Send Message</span>
</h2>
<p>
Write your message and users nickname
</p>
<br>
<div class="form-container">
    <div class="inside">
        <h3>
            Send A Message
        </h3>
        @if(Session::has('info'))
        <div class="msg">{{Session::get('info')}}</div>
        @endif
        @if(Session::has('success'))
        <div class="msg success">{{Session::get('success')}}</div>
        @endif
        @if(Session::has('error'))
        <div class="msg error">{{Session::get('error')}}</div>
        @endif
        <form name="login-form" id="zuckologinform" method="post" action="{{url('inbox/compose')}}" autocomplete="off">
            <div class="form-field full">
                <input class="input input-long" type="text" value="{{Input::get('user')?:Session::get('user')}}" name="user" placeholder="User Nickname" />
            </div>
            <div class="form-field full">
                <input class="input input-long" type="text" value="{{Session::get('subject')}}" name="subject" placeholder="Message Subject" />
            </div>
            <div class="form-field full">
                <textarea style="width:500px;height:250px;" name="message" placeholder="Your Message..." class="input input-long">{{Session::get('message')}}</textarea>
            </div>
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="notice"></div>
            <input type="submit" name="" class="red-button" value="Send Message" />
            <div class="cf"></div>
        </form>
    </div>
</div>
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop