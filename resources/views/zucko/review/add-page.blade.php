@extends('layouts.main')
@section('window-title')
    {{trans("review.Write Review")}} | @parent
@stop
@section('main-content')
    <h2 class="title-stripped">
        <span class="text">{{trans("review.Write Review")}}</span>
    </h2>
    <h3 class="blue-header-title">{{trans("review.Which Business Would You Like to Review")}}?</h3>
    <div class="options_area">
        <ul>
            <li><i class="blue-round-icon icon-search"></i> {{trans("review.Search for business")}}</li>
            <li><i class="blue-round-icon icon-business"></i> {{trans("review.Select the business")}}</li>
            <li><i class="blue-round-icon icon-write"></i> {{trans("review.Write your review")}}!</li>
        </ul>
    </div>
    <!-- Main Content Here -->
    <div class="list_below_heading">
        <h3>{{trans("review.Select From the List Below or Search Again")}}</h3>
    </div>
    <form action="" method="get">
        <div class="business_form">
            <div class="field fl">
                <div class="tool_tip">
                    <span class="red">{{trans("review.Business Name")}}</span>
                    <span class="down-arrow"></span>
                </div>
                <input placeholder="{{trans("review.Business Name")}}" value="{{Input::get('name')}}" name="name"
                       style="margin-top:30px;" class="input input-long">
            </div>
            <div class="field fr">
                <div class="tool_tip">
                    <span class="red">{{trans("review.Near")}}</span>({{trans("review.Address, Neighborhood, City or Zip Code")}})
                    <span class="down-arrow"></span>
                </div>
                <input placeholder="{{trans("review.Place")}}" name="address" value="{{Input::get('address')}}"
                       style="margin-top:30px;float:" class="input input-long">
            </div>
        </div>
        <div class="cf"></div>
        <div style="margin-bottom:20px" class="search_business_button">
            <input type="submit" style="float:right" value="{{trans("review.SEARCH BUSINESS")}}" class="red-button"/>
        </div>
    </form>
    <h3 class="blue-header-title">Select From the List Below for Review</h3>
    @if(count($businesses)<1)
        <div class="nothing-found-msg">
            Ops! Nothing Found!!
        </div>
    @endif
    @foreach($businesses as $key => $business)
        <div id="item_{{$business->id}}" class="review_selection business-item">
            <div class="review_address_area cf">
                <div style="margin-top:-8px;" class="review_address fl">

                    <h4><span class="num">{{$key+1}}</span> <a
                                href="{{$business->profile_url}}">{{$business->present()->title(50)}}</a></h4>

                    <div class="desc">
                        <address>
                            {{$business->address}}, {{$business->zip_code}}<br>
                            {{trans("review.phone")}}: {{$business->present()->phone()}}
                        </address>
                        {{trans("review.Category")}}: <a style="color:inherit"
                                                         href="{{$business->category->page_url}}">{{$business->present()->category_name()}}</a><br>
                        {{trans("review.Neighborhood")}}: {{$business->present()->city_name()}}
                    </div>
                </div>
                <div style="width:200px;margin-right:16px;" class="review_rating fr">
                    <div data-score="{{$business->present()->rating()}}" class="rating fr"></div>
                    <div style="display:block;margin-bottom:32px;clear:right;margin-right:0px;min-width:100px;text-align:center;"
                         class="review-count fr"><a
                                href="{{$business->review_page}}">{{$count = $business->reviews()->count()}} {{Lang::choice("app.reviews", $count+10)}}</a>
                    </div>
                    <a style="float:right;" href="#item_{{$business->id}}"
                       class="red-button write-review-button">{{trans("review.Write a Review")}}</a>
                </div>
                <div class="cf"></div>
                <div class="review_admin area add-review-container hidden">
                    <form method="post" name="add-review-form">
                        <div class="write_comment review-writer">
                            <div class="msg hidden top"></div>
                            <div style="margin-right: 18px; margin-bottom: 5px;" class="rating-picker fr">
                                Pick a rating
                                <div class="rating2 fr"></div>
                            </div>
                            <div class="cf"></div>
                            {{Form::token()}}
                            <div style="position:absolute;" class="img-container img-container-curve fl">
                                <img src="{{isset($current_user)?$current_user->profile_pic:url('img/sample5.png')}}"/>
                            </div>
                            <div class="write_admin fr">
                                <input type="hidden" name="business_id" value="{{$business->id}}">

                                <div style="margin-bottom:7px;" class="cf"></div>
                                <textarea type="text" name="description" class="input fr"
                                          placeholder="{{trans("review.Write your review")}}..."></textarea>
                                <button style="margin-top:10px;margin-right: 30px;text-transform: uppercase;" type="button" class="blue-button submit-review-button fr"
                                        value="POST">{{trans("app.Post")}}</button>
                                <div class="cf"></div>
                            </div>
                            <div class="cf"></div>
                        </div>
                    </form>
                    <div class="others-review">
                        @foreach($business->reviews as $review)
                            <div id="review_{{$review->id}}" class="one_comment review-item2 cf">
                                <div class="img-container img-container-curve fl">
                                    <img src="{{$review->user->profile_pic}}"/>
                                </div>
                                <div class="a_comment fr cf">
                                    <h5>
                                        <a href="{{$review->user->profile_url}}">{{$review->user->nickname}}</a>
                                        &nbsp;&nbsp;<span class="date moment-date"
                                                          data-time="{{$review->created_at->timestamp}}"
                                                          data-format="MMMM DD, YYYY \a\t hh:mm A">{{$review->created_at->format('F j, Y \a\t h:i A')}}</span>
                                        <span data-score="{{$review->rating}}" class="rating fr"></span>
                                    </h5>

                                    <p>
                                        {{$review->present()->desc(200)}}
                                    </p>
                                    <button type="button" class="blue-button fr reply-button">{{trans("review.REPLY")}}</button>
                                    <a class="gray-small fr show-comments-button"
                                       style="display:inline-block;margin: 3px 27px 0px 0px;"
                                       href="#comments">{{$review->comments()->count()}} comments</a>
                                    <a class="gray-small fr like-review-button"
                                       style="display:inline-block;margin: 3px 27px 0px 0px;" data-rid="{{$review->id}}"
                                       href="{{$review->like_url}}">{{$review->likes()->count()}} likes</a>

                                    <div class="cf"></div>
                                    <div class="comments-container2 hidden review-comment-items">
                                        <div class="review-comment-items-list">
                                            @foreach($review->comments as $comment)
                                                <div style="margin:8px 0px 0px 0px;" id="comment_{{$comment->id}}"
                                                     class="one_comment review-comment-item sub cf">
                                                    <div class="img-container img-container-curve fl">
                                                        <img data-src="@{{&user.profile_pic}}"
                                                             src="{{$comment->user->profile_pic}}"/>
                                                    </div>
                                                    <div class="a_comment fr cf">
                                                        <h5>
                                                            <a data-href="@{{&user.profile_url}}"
                                                               data-bind="@{{user.nickname}}"
                                                               href="{{$comment->user->profile_url}}">{{$comment->user->nickname}}</a>
                                                            &nbsp;&nbsp;<span data-bind="@{{created_at}}"
                                                                              class="date moment-date"
                                                                              data-time="{{$comment->created_at->timestamp}}"
                                                                              data-format="MMMM DD, YYYY \a\t hh:mm A">{{$comment->created_at->format('F j, Y \a\t h:i A')}}</span>
                                                        </h5>

                                                        <p data-bind="@{{description}}">
                                                            {{$comment->description}}
                                                        </p>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <form autocomplete="off" name="review-comment-form"
                                              action="{{$review->comments_page}}" method="post">
                                            <div class="msg hidden"></div>
                                            <div class="write_reply cf">
                                                <div class="img-container img-container-curve fl">
                                                    <img src="{{isset($current_user)?$current_user->profile_pic:url('img/sample5.png')}}"/>
                                                </div>
                                                <input type="hidden" name="target_id" value="{{$review->id}}">
                                                <textarea name="description" type="text" class="input fr"
                                                          placeholder="{{trans("review.Write your reply")}}.."></textarea>

                                                <div class="cf"></div>
                                                {{Form::token()}}
                                                <button style="text-transform: uppercase;" type="button" class="blue-button write-reply-button fr">
                                                    {{trans("app.Post")}}
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div><!--END one_comment-->
                        @endforeach
                    </div>
                </div>
            </div>
        </div><!--END review_selection-->
    @endforeach
    {{$businesses->setPath("write")->appends(Input::only('name','address'))->render()}}
    <div style="margin-bottom:30px;" class="cf"></div>
    <script type="text/javascript">
        jQuery(document).ready(function ($){
            $('.rating2').raty({
                'readOnly': false,
                'score'   : function (){
                    return $(this).attr('data-score');
                }
            });
            $('.like-review-button').click(function (event){
                /* Act on the event */
                event.preventDefault();
                var $this = $(this);
                $this.html('wait..');
                $this.attr('disabled', 'true');
                var api = new ZuckoApi('review/like', {
                    'review_id': $this.attr('data-rid')
                });
                api.call('post').done(function (res){
                    $this.html(res.likes_count + " likes");
                }).fail(function (res){
                    $this.html('like');
                })
            });
            $('.submit-review-button').click(function (event){
                event.preventDefault();
                var $this = $(this);
                var parent = $this.parents('.add-review-container');
                $this.html('Posting...');
                $this.attr('disabled', 'true');
                var form = parent.find('form[name="add-review-form"]');
                var data = form.serialize();
                var api = new ZuckoApi('review/create', data);
                api.call('post').done(function (res){
                    parent.find('div.msg.top').removeClass('error info').addClass('success').html('Review sent successfully').fadeIn(200);
                    form[ 0 ].reset();
                }).fail(function (res){
                    parent.find('div.msg.top').removeClass('success info').addClass('error').html(res.error_message).fadeIn(200);
                }).always(function (){
                    $this.html('Post Review');
                    $this.removeAttr('disabled');
                });
            });
            $('.write-reply-button').click(function (event){
                event.preventDefault();
                var $this = $(this);
                var parent = $this.parents('.review-comment-items');
                $this.html('Posting..');
                $this.attr('disabled', 'true');
                var form = parent.find('form[name="review-comment-form"]');
                var data = form.serialize();
                var api = new ZuckoApi('review/comment', data);
                api.call('post').done(function (res){
                    console.log(res);
                    res.comment.created_at = "Just now";
                    var com = parent.find('.review-comment-item').first().cloneWithData(res.comment);
                    parent.find('.review-comment-items-list').append(com);
                    parent.find('div.msg').removeClass('error info').addClass('success').html(res.message).fadeIn(200);
                    form[ 0 ].reset();
                }).fail(function (res){
                    parent.find('div.msg').removeClass('success info').addClass('error').html(res.error_message).fadeIn(200);
                }).always(function (){
                    $this.html('Post');
                    $this.removeAttr('disabled');
                });
            });
            $('.write-review-button').click(function (event){
                /* Act on the event */
                event.preventDefault();
                var $this = $(this);
                var parent = $this.parents('.business-item');
                var cont = parent.find('.add-review-container');
                if ( cont.is(':visible') ) {
                    return;
                }
                if ( $('.add-review-container:visible').length > 0 ) {
                    var old = $('.add-review-container:visible');
                    old.fadeOut(400, function (){
                        //alert(1);
                        $("html,body").animate({
                            scrollTop: $($this.attr('href')).offset().top - 20
                        }, {
                            duration: 200,
                            done    : function (){
                                //alert(2);
                            }
                        });
                        old.find('.others-review').mCustomScrollbar('destroy');
                    });
                }
                cont.fadeIn(400, function (){
                    cont.find('.others-review').mCustomScrollbar();
                });
            });
            $('.reply-button, .show-comments-button').click(function (event){
                /* Act on the event */
                event.preventDefault();
                var $this = $(this);
                var parent = $this.parents('.review-item2');
                var cont = parent.find('.comments-container2');
                if ( cont.is(':visible') ) {
                    cont.fadeOut('400', function (){

                    });
                    return;
                }
                cont.fadeIn('400', function (){

                });
            });
        });
    </script>
@stop
@section('sidebar')
    @include('common.sidebar',array('widgets'=>array('add-btn', 'mobile-app')))
@stop