<div class="add-review-module">
	@if(Session::has('info'))
	<div class="msg">{{Session::get('info')}}</div>
	@endif
	@if(Session::has('success'))
	<div class="msg success">{{Session::get('success')}}</div>
	@endif
	@if(Session::has('error'))
	<div class="msg error">{{Session::get('error')}}</div>
	@endif
	@if(!empty($myreview->description))
	<div class="msg">You already review this business. You can edit your review from here</div>
	@endif
	<div class="poster-pic fl">
		<div class="img-container img-container-curve">
			<img src="{{$current_user->profile_pic}}"/>
		</div>
	</div>
	<div class="review-content fl">
		<form method="post" action="{{url('reviews/add')}}">
		<div class="rating-picker fr">Pick a rating <div data-score="{{$myreview->rating or '0'}}" class="rating2 fr"></div></div>
		<div class="cf"></div>
		<input type="hidden" name="user_id" value="{{$current_user->id}}">
		<input type="hidden" name="business_id" value="{{$business->id}}">
		<textarea name="description" placeholder="Write your comments here..">{{$myreview->description or Session::get('description')}}</textarea>
		<input type="submit" class="blue-button submit-btn fr" value="Submit Review"/>
		</form>
	</div>
	<div class="cf"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.rating2').raty({
			'readOnly':false,
			'score': function(){
				return $(this).attr('data-score');
			}
		});
	});
</script>