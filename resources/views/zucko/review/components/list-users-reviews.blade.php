@if($reviews->count()<1)
<div style="padding:20px">No activity to show here :(</div>
@endif
@foreach($reviews as $key=>$review)
<div data-review='{{$review->withUserInteractions(isset($current_user)?$current_user:false)->toJson(JSON_HEX_APOS|JSON_HEX_QUOT)}}' class="feed-item cf backbone-review-view">
	<div class="businss fl">
		<h4 style="font-size:16px;font-weight:400;color:#d72e00;"><a href="{{$review->business->profile_url}}">{{$review->business->present()->title(18)}}</a></h4>
		<p style="color:#404040;font-size:12px;">Category: <a class="red" href="{{$review->business->category->page_url}}">{{$review->business->present()->category_name()}}</a></p>
		<div data-score="{{$review->business->rating_score}}" style="margin-top:2px;" class="rating"></div>
	</div><!--END hotel-->
	<div class="business-address fr">
		<address style="font-style:normal!important;">
			{{$review->business->present()->addr(20)}}<br/>
			{{$review->business->present()->city_name()}}, {{$review->business->zip_code}}<br/>
		</address>
		{{trans("review.phone")}}: {{$review->business->present()->phone()}}
			
	</div><!--END category-->
	<div class="desc2">
		<p>
			{{nl2br(e($review->description))}}
		</p>
		<p class="post-date">
			Posted on {{$review->created_at->toDayDateTimeString()}}
		</p>
	</div><!--END desc-->
	<div style="margin-top:4px;color:#808080;font-size:13px;font-weight:400;" class="fl">
		{{trans("review.Was this review")}}
		<a href="{{isset($current_user)?'javascript:void(0)':url('login')}}" style="" data-rid="{{$review->id}}" data-type="useful" class="red-button vote-button useful review-rating-button">{{trans("review.USEFUL")}}<i class="icon-tick"></i></a>
		<a href="{{isset($current_user)?'javascript:void(0)':url('login')}}" style="margin-left:0px!important" data-rid="{{$review->id}}" data-type="funny" class="red-button vote-button funny review-rating-button">{{trans("review.FUNNY")}}<i class="icon-tick"></i></a>
	</div>
	<div class="cf"></div>
	<div style="margin-top:7px;" class="review-actions-blue comment_options cf">
		<!--
		<a class="" href=""><i class="send_commpliments"></i> Send Complement&nbsp;&nbsp;</a>
		<a href=""><i class="send_friend"></i> Like This&nbsp;&nbsp;</a>
		-->
		<a href="{{$review->permalink}}"><i class="ink_review"></i> {{trans("review.Link to this review")}}</a>
		<a style="margin-left:8px;text-decoration:none;cursor:default;" href="javascript:void(0)"><i class="vote_result_icon"></i> {{trans("review.Useful")}} <span class="stats-useful">0%</span></a>
		<a style="margin-left:8px;text-decoration:none;cursor:default;" href="javascript:void(0)"><i class="vote_result_icon"></i> {{trans("review.Funny")}} <span class="stats-funny">0%</span></a>
		<span class="stats-likes">00</span><a class="like-btn" style="margin-left:5px;" href="{{isset($current_user)?'javascript:void(0)':url('login')}}"><i class="likes_count_icon"></i></a>
	</div>
	<div class="feed-story-separator"></div>
</div>
@endforeach
{{$reviews->render()}}
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.vote-button').click(function(event) {
			
		});
	});
</script>