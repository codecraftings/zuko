@extends('admin.layouts.master')
@section('body')
@include('admin.common.top-menu',array('page_id'=>'business'))
<div class="account-container big">
	
	<div class="content clearfix">
		{{Form::model($business, array('enctype'=>"multipart/form-data", 'autocomplete'=>"off", 'method'=>'post'))}}

		<div class="form-fields">
			<h1>Edit: {{$business->name}}</h1>	
			@if(Session::has('error'))
			<div class="alert alert-error">
				{{Session::get('error')}}
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-success">
				{{Session::get('success')}}
			</div>
			@endif
			{{Form::token()}}
			<div class="field clearfix">
				{{Form::label('name','Company Name')}}
				{{Form::text('name', null, array('required'=>'true', 'placeholder'=>'Compnay Name', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('listing_status','Status')}}
				{{Form::select('listing_status', array(
					Zucko\Core\BaseModel::DRAFT => 'Draft',
					Zucko\Core\BaseModel::PUBLISHED => 'Published'
					),null, array('class'=>'input'))}}
				</div>
				<div class="field clearfix">
					{{Form::label('listing_type','Listing Type')}}
					{{Form::select('listing_type', array(
						'place' => 'Local Place',
						'product' => 'Product or Brand',
						'company' => 'Company, Institute, Organization'
						),null, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('website','Website')}}
						{{Form::text('website', $business->meta->website, array('placeholder'=>'Website', 'class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('video_url','Video Url')}}
						{{Form::text('video_url', $business->meta->video_url, array( 'placeholder'=>'Video Url', 'class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('tahiti_numero','Tahiti Numero')}}
						{{Form::text('tahiti_numero', null, array('placeholder'=>'Tahiti Numero', 'class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('tahiti_rc','Tahiti RC')}}
						{{Form::text('tahiti_rc', null, array('placeholder'=>'Tahiti RC', 'class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('phone','Phone')}}
						{{Form::text('phone', Input::old('phone')?:$business->meta->phone, array('placeholder'=>'Phone Number', 'class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('fax','Fax')}}
						{{Form::text('fax', Input::old('fax')?:$business->meta->fax, array('placeholder'=>'Fax', 'class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('category_id','Type of Activity')}}
						{{Form::select('category_id', create_array($categories, 'id','name'),null, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('city','City Name')}}
						{{Form::text('city', $business->present()->city_name(), array('required'=>'true', 'placeholder'=>'City Name', 'class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('state_id','Island')}}
						{{Form::select('state_id', create_array($states, 'id','name'),null, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('country_id','Country')}}
						{{Form::select('country_id', create_array($countries, 'id','name'),null, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('zip_code','Zip Code')}}
						{{Form::text('zip_code', null, array('required'=>'true', 'placeholder'=>'Zip Code', 'class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('company_type','Company Type')}}
						{{Form::select('company_type', array(
							'SARL' => 'SARL',
							'EURL' => 'EURL',
							'SELARL' => 'SELARL',
							'SA' => 'SA',
							'SAS' => 'SAS',
							'SASU' => 'SASU',
							'SNU' => 'SNU',
							'SCP' => 'SCP'
							),null, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('good_for_groups','Good For Groups')}}
						{{Form::select('good_for_groups', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('good_for_groups')?:$business->meta->good_for_groups, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('water_service','Water Service')}}
						{{Form::select('water_service', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('water_service')?:$business->meta->water_service, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('credit_card_accepted','Credit Card Accepted')}}
						{{Form::select('credit_card_accepted', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('credit_card_accepted')?:$business->meta->credit_card_accepted, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('parking_valet','Parking Valet')}}
						{{Form::select('parking_valet', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('parking_valet')?:$business->meta->parking_valet, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('wheelchair_accessible','Wheelchair Accessible')}}
						{{Form::select('wheelchair_accessible', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('wheelchair_accessible')?:$business->meta->wheelchair_accessible, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('outdoor_seating','Outdoor Seating')}}
						{{Form::select('outdoor_seating', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('outdoor_seating')?:$business->meta->outdoor_seating, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('good_for_kids','Good For Kids')}}
						{{Form::select('good_for_kids', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('good_for_kids')?:$business->meta->good_for_kids, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('take_reservation','Take Reservation')}}
						{{Form::select('take_reservation', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('take_reservation')?:$business->meta->take_reservation, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('good_for','Good For')}}
						{{Form::select('good_for', array(
							'breakfast' => 'Breakfast',
							'lunch' => 'Lunch',
							'dinner' => 'Dinner'
							),Input::old('good_for')?:$business->meta->good_for, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('alcohol','Alcohol')}}
						{{Form::select('alcohol', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('alcohol')?:$business->meta->alcohol, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('delivery','Delivery')}}
						{{Form::select('delivery', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('delivery')?:$business->meta->delivery, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('to_go','To Go')}}
						{{Form::select('to_go', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('to_go')?:$business->meta->to_go, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
						{{Form::label('dogs_allowed','Dogs Allowed')}}
						{{Form::select('dogs_allowed', array(
							'0' => 'No',
							'1' => 'Yes'
							),Input::old('dogs_allowed')?:$business->meta->dogs_allowed, array('class'=>'input'))}}
					</div>
					<div class="field clearfix">
					{{Form::label('price_range','Price Range(XPF)')}}
					{{Form::text('price_low',Input::old('price_low')?:$business->meta->price_low,array('class'=>'input input-small'))}}-
					{{Form::text('price_high',Input::old('price_high')?:$business->meta->price_high,array('class'=>'input input-small'))}}
					</div>
					<div class="clearfix">
					@if($business->logo)
							<img src="{{$business->_logo()->url}}" width="200px" height="80px" />
					@endif
					</div>
					<div class="field clearfix">
							{{Form::label('logo','Company Logo')}}
							{{Form::file('logo', array('class'=>'input'))}}
					</div>
					<div class="clearfix">
					Business photos
					</div>
					<div class="clearfix">
							@foreach($business->photos as $photo)
							<div class="screenshot-thumb fl">
								<img style="width:100px;height:100px;" src="{{$photo->thumb_url}}"/>
								<input type="hidden" name="oldphotos[]" value="{{$photo->id}}">
								<a href="javascript:void();" onclick="$(this).parent().remove();" class="cross">x</a>
							</div>
							@endforeach
					</div>
					<div class="clearfix">
					{{Form::label('newphoto[]','Upload a new photos')}}
					{{Form::file('newphoto[]', array('class'=>'input', 'multiple'=>'true'))}}
					</div>
					<div style="position:relative;" class="clearfix">
					Opening Hours
					<br>
					@include('zucko.business.components.hour-selector', array('business'=>$business))
					</div>
					</div> <!-- /login-fields -->

					<div class="actions clearfix">

						<button class="button btn btn-success btn-large">Submit</button>

					</div> <!-- .actions -->



					{{Form::close()}}

				</div> <!-- /content -->

			</div>
			@stop