@extends('admin.layouts.master')
@section('body')
@include('admin.common.top-menu',array('page_id'=>'comments'))
<div class="main">
	<div class="main-inner">
		<div class="container">
			<div class="row">
				<div class="span10">
					<div class="widget widget-table action-table">
						<form autocomplete="off" style="margin-top:15px;" id="filterform" class="form-inline" method="get" action="">
							@if(Session::has('error'))
							<div class="alert alert-error">
								{{Session::get('error')}}
							</div>
							@endif
							@if(Session::has('success'))
							<div class="alert alert-success">
								{{Session::get('success')}}
							</div>
							@endif
							<label for="sort">Sort By</label>
							<select onchange="document.getElementById('filterform').submit();" id="sort" name="sort">
								<option{{Input::get('sort')=="updated"?" selected":""}} value="updated">Last Updated</option>
								<option{{Input::get('sort')=="new"?" selected":"";}} value="new">Newly Added</option>
								<option{{Input::get('sort')=="old"?" selected":""}} value="old">Older</option>
							</select>
							<label for="pagging">Pagging </label>
							<select onchange="document.getElementById('filterform').submit();" id="pagging" name="pagging">
								<option{{Input::get('pagging')=="100"?" selected":""}} value="100">100</option>
								<option{{Input::get('pagging')=="200"?" selected":""}} value="200">200</option>
								<option{{Input::get('pagging')=="500"?" selected":""}} value="500">500</option>
								<option{{Input::get('pagging')=="1000"?" selected":""}} value="1000">1000</option>
							</select>
						</form>
						<div class="widget-header"> <i class="icon-th-list"></i>
							<h3>List of comments</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>User</th>
										<th>Description</th>
										<th>Last Updated</th>
										<th>Added On</th>
										<th class="td-actions"> </th>
									</tr>
								</thead>
								<tbody>
									@foreach($comments as $comment)
									<tr>
										<td><a target="_blank" href="{{$comment->user->profile_url}}">{{$comment->user->present()->name(20)}}</a></td>
										<td>{{e(limit_str($comment->description,200, ".."))}}</td>
										<td>{{$comment->updated_at->diffForHumans()}} ago</td>
										<td>{{$comment->created_at->format('d/m/Y')}}</td>
										<td class="td-actions">
											<!--
											<a target="_blank" href="{{$comment->permalink}}" class="btn btn-small btn-success">
												<i class="btn-icon-only icon-link"> </i>
											</a>
											-->
											<a href="{{admin_url('comment/edit/'.$comment->id)}}" class="btn btn-small btn-primary">
												<i class="btn-icon-only icon-pencil"> </i>
											</a>
											<a href="javascript:;" onclick="if(confirm('Do you really want to delete this comment?')){document.getElementById('delete-form-{{$comment->id}}').submit();}" class="btn btn-danger btn-small">
												<i class="btn-icon-only icon-trash"> </i>
											</a>
											<form method="post" id="delete-form-{{$comment->id}}" name="delete-form" action="{{admin_url('comment/delete/'.$comment->id)}}">
											{{Form::token()}}
											</form>
										</td>
									</tr>
									@endforeach
								</tbody>
								<tfoot>
								</tfoot>
							</table>
						</div>
						{{$comments->appends(Input::only('pagging','sort'))->render()}}
						<!-- /widget-content --> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /widget --> 
@stop