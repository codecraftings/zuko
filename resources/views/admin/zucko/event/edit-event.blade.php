@extends('admin.layouts.master')
@section('body')
@include('admin.common.top-menu',array('page_id'=>'event'))
<div class="account-container big">
	
	<div class="content clearfix">
		{{Form::model($event, array('enctype'=>"multipart/form-data", 'autocomplete'=>"off", 'method'=>'post'))}}

		<div class="form-fields">
			<h1>Edit: {{$event->title}}</h1>	
			@if(Session::has('error'))
			<div class="alert alert-error">
				{{Session::get('error')}}
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-success">
				{{Session::get('success')}}
			</div>
			@endif
			{{Form::token()}}
			<div class="field clearfix">
				{{Form::label('title','Event Title')}}
				{{Form::text('title', null, array('required'=>'true', 'placeholder'=>'Event Title', 'class'=>'input'))}}
			</div>
			<div class="field clearfix">
				{{Form::label('listing_status','Status')}}
				{{Form::select('listing_status', array(
					Zucko\BaseModel::DRAFT => 'Draft',
					Zucko\BaseModel::PUBLISHED => 'Published'
					),null, array('class'=>'input'))}}
				</div>
				<div class="field clearfix">
					{{Form::label('location','Location')}}
					{{Form::text('location', null, array('required'=>'true', 'placeholder'=>'Location', 'class'=>'input'))}}
				</div>
				<div class="field clearfix">
					{{Form::label('city','City')}}
					{{Form::text('city', Input::old('city')?:$event->present()->city_name(), array('required'=>'true', 'placeholder'=>'City', 'class'=>'input'))}}
				</div>
				<div class="field clearfix">
					{{Form::label('link','Website Link')}}
					{{Form::text('link', null, array('placeholder'=>'Website Link', 'class'=>'input'))}}
				</div>
				<div class="field clearfix">
					{{Form::label('video_url','Video Url')}}
					{{Form::text('video_url', null, array('placeholder'=>'Video Url', 'class'=>'input'))}}
				</div>
				<div class="field clearfix">
					{{Form::label('description','Description')}}
					{{Form::text('description', null, array('required'=>'true', 'placeholder'=>'Description', 'class'=>'input'))}}
				</div>
				<div class="field clearfix">
					{{Form::label('event_time','Event Date')}}
					{{Form::text('event_time', null, array('required'=>'true', 'placeholder'=>'Event Date', 'class'=>'input'))}}
				</div>
				<div class="clearfix">
					Event photos
				</div>
				<div class="clearfix">
					@foreach($event->photos as $photo)
					<div class="screenshot-thumb fl">
						<img style="width:100px;height:100px;" src="{{$photo->thumb_url}}"/>
						<input type="hidden" name="oldphotos[]" value="{{$photo->id}}">
						<a href="javascript:void();" onclick="$(this).parent().remove();" class="cross">x</a>
					</div>
					@endforeach
				</div>
				<div class="clearfix">
					{{Form::label('newphoto[]','Upload a new photos')}}
					{{Form::file('newphoto[]', array('class'=>'input', 'multiple'=>'true'))}}
				</div>
			</div> <!-- /login-fields -->

			<div class="actions clearfix">

				<button class="button btn btn-success btn-large">Submit</button>

			</div> <!-- .actions -->



			{{Form::close()}}

		</div> <!-- /content -->

	</div>
	@stop