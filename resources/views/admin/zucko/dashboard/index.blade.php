@extends('admin.layouts.master')
@section('body')
@include('admin.common.top-menu', array('page_id'=>'dashboard'))
<div class="main">
	<div class="main-inner">
		<div class="container">
			<div class="row">
				<div class="span6">
					<div class="widget">
						<div class="widget-header"> <i class="icon-bookmark"></i>
							<h3>Quick Navigations</h3>
						</div>
						<!-- /widget-header -->
						<div class="widget-content">
							<div class="shortcuts">
								<a href="{{admin_url('business/list')}}" class="shortcut">
									<i class="shortcut-icon icon-cloud"></i>
									<span class="shortcut-label">View All Businesses</span> 
								</a>
								<?php if(isset($admin)){?>
								<a href="{{admin_url('users/list')}}" class="shortcut">
									<i class="shortcut-icon icon-group"></i> 
									<span class="shortcut-label">View All User</span> 
								</a>
								<a href="{{admin_url('events/list')}}" class="shortcut">
									<i class="shortcut-icon icon-calendar"></i> 
									<span class="shortcut-label">View All Events</span> 
								</a>
								<?php }?>
								<a href="{{admin_url('edit-account')}}" class="shortcut">
									<i class="shortcut-icon icon-lock"></i>
									<span class="shortcut-label">Change Password</span> 
								</a>
								<a href="{{admin_url('edit-account')}}" class="shortcut">
									<i class="shortcut-icon icon-envelope-alt"></i>
									<span class="shortcut-label">Change Email</span> 
								</a>
							</div>
							<!-- /shortcuts --> 
						</div>
						<!-- /widget-content --> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop