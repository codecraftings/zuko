@extends('admin.layouts.master')
@section('body')
<div class="account-container">
	
	<div class="content clearfix">
		
		<form action="" autocomplete="off" method="post">
		
			<h1>Admin Login</h1>		
			
			<div class="login-fields">
				
				<p>Please provide your details</p>
				@if(Session::has('error'))
				<div class="alert alert-error">
				{{Session::get('error')}}
				</div>
				@endif
				<input type="hidden" name="_token" value="<?php echo csrf_token();?>">
				<div class="field">
					<label for="email">Email</label>
					<input type="email" id="email" name="email" value="{{Session::get('email')}}" placeholder="Email" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<button class="button btn btn-success btn-large">Sign In</button>
				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> 
@stop