<!DOCTYPE html>
<html lang="en">
<head>
	@include('admin.common.html-head')
</head>
<body>
	@include('admin.common.header')
	@section('body')
	@show
	@include('admin.common.footer')
</body>
</html>