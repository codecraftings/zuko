<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li class="{{$page_id=='dashboard'?'active':''}}"><a href="{{admin_url('dashboard')}}"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        @if(isset($admin))
        <li class="dropdown{{$page_id=='users'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-group"></i><span>Users</span><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('users/list')}}">View All Users</a></li>
          </ul>
        </li>
        @endif
        <li class="dropdown{{$page_id=='business'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cloud"></i><span>Businesses</span><b class="caret"></b></a> 
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('business/list')}}">View All Businesses</a></li>
          </ul>
        </li>
        @if(isset($admin))
        <li class="dropdown{{$page_id=='events'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-calendar"></i><span>Events</span><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('events/list')}}">View All Events</a></li>
          </ul>
        </li>
        @endif
        <li class="dropdown{{$page_id=='reviews'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-book"></i><span>Reviews</span><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('reviews/list')}}">View All Reviews</a></li>
          </ul>
        </li>
        <li class="dropdown{{$page_id=='comments'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-comment"></i><span>Comments</span><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('comments/list')}}">View All Comments</a></li>
          </ul>
        </li>
        <li class="dropdown{{$page_id=='settings'?' active':''}}"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cogs"></i><span>Settings</span><b class="caret"></b></a> 
          <ul class="dropdown-menu">
            <li><a href="{{admin_url('edit-account')}}">Change Password</a></li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->