<div class="navbar navbar-fixed-top">

		<div class="navbar-inner">

			<div class="container">

				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<a class="brand" href="{{url('/')}}">
					Zuckoo Admin Panel				
				</a>		

				<div class="nav-collapse">
					<ul class="nav pull-right">
						@if(isset($current_user)&&$current_user)
							<li class="">						
								<a href="{{url(Config::get('app.admin_dir').'/logout')}}" class="">
									Logout
								</a>
							</li>
						@endif
						</ul>

					</div><!--/.nav-collapse -->	

				</div> <!-- /container -->

			</div> <!-- /navbar-inner -->

</div> <!-- /navbar -->