<div class="row footer1">
    <div class="container">
        <div class="footer-widget about">
            <h4 class="title">{{trans("footer.About")}}</h4>
            <ul class="vmenu about">
                <li>
                    <a href="{{url('page/about')}}">{{trans("footer.About")}} Zuckoo</a>
                </li>
                <li>
                    <a href="{{url('blogs')}}">Zuckoo Blog</a>
                </li>
                <li>
                    <a href="{{url('page/terms')}}">{{trans("footer.Terms of Service")}}</a>
                </li>
                <li>
                    <a href="{{url('page/privacy')}}">{{trans("footer.Privacy Policy")}}</a>
                </li>
            </ul>
        </div>
        <div class="footer-widget help">
            <h4 class="title">{{trans("footer.Help")}}</h4>
            <ul class="vmenu help">
                <li>
                    <a href="{{url('page/contact')}}">{{trans("footer.Advertise")}}</a>
                </li>
                <li>
                    <a href="{{url('page/contact')}}">{{trans("footer.Contact Us")}}</a>
                </li>
            </ul>
        </div>
        <div style="margin-right: 30px;" class="footer-widget more">
            <h4 class="title">{{trans("footer.More")}}</h4>
            <ul class="vmenu more">
                <li>
                    <a href="{{url('page/career')}}">{{trans("footer.Careers")}}</a>
                </li>
                <li>
                    <a href="{{url('page/mobile')}}">Annu@ire Mobile</a>
                </li>
            </ul>
        </div>
        <div style="margin-right: 40px;" class="footer-widget contact">
            <h4 class="title">{{trans("footer.Contact Us")}}</h4>
            <form method="post" action="{{url('page/contact')}}" autocomplete="off">
                <input class="input" type="text" name="name" placeholder="{{trans("footer.Your Name")}}" /><br>
                <input class="input" type="text" name="email" placeholder="{{trans("footer.Email Address")}}" /><br>
                <textarea class="input" name="message" placeholder="Message"></textarea><br>
                <input type="submit" name="submit" value="{{trans("app.Submit")}}" />
            </form>
            <div class="cf"></div>
        </div>
        <div class="footer-widget tweets">
            <h4 class="title"><i class="twitter-bird"></i>{{trans("footer.Recent")}} <span class="different">Tweets</span></h4>
            <p>Great local place. Owner made our coffee and sandwiches. Check it out if your on the area! </p>
        </div>
        <div class="footer-widget follow">
            <h4 class="title">{{trans("footer.Follow")}} <span class="different">{{trans("footer.Us On")}}</span></h4>
            <ul class="menu social">
                <li>
                    <a href="#" class="social-gray fb"></a>
                </li>
                <li>
                    <a href="#" class="social-gray linkedin"></a>
                </li>
                <li>
                    <a href="#" class="social-gray youtube"></a>
                </li>
                <li>
                    <a href="#" class="social-gray gplus"></a>
                </li>
                <li>
                    <a href="#" class="social-gray twitter"></a>
                </li>
            </ul>
        </div>
        <div class="cf"></div>
    </div>
</div>
<div class="row footer2">
    <div class="container">
        <div class="footer-menu-container fl">
            <ul class="menu footer-menu">
                <li class="footer-menu-item">
                    <a href="{{url('/')}}">{{trans("header.home")}}</a>
                </li>
                <li class="footer-menu-item">
                    <a href="{{url('page/about')}}">{{trans("header.about_us")}}</a>
                </li>
                <li class="footer-menu-item">
                    <a href="{{url('reviews/write')}}">{{trans("header.write_review")}}</a>
                </li>
                <li class="footer-menu-item">
                    <a href="{{url('friends')}}">{{trans("header.find_friends")}}</a>
                </li>
                <li class="footer-menu-item">
                    <a href="{{url('inbox')}}">{{trans("header.messages")}}</a>
                </li>
                <li class="footer-menu-item">
                    <a href="{{url('blogs')}}">{{trans("header.blog")}}</a>
                </li>
                <li class="footer-menu-item">
                    <a href="{{url('events')}}">{{trans("header.events")}}</a>
                </li>
                <li class="footer-menu-item">
                    <a href="{{url('page/contact')}}">{{trans("header.contact_us")}}</a>
                </li>
            </ul>
        </div>

        <div class="footer-logo fr">
        </div>

        <div class="copyright-text fl">
            <p>&COPY;{{date('Y')}} Annu@ire. All rights reserved</p>
        </div>
        <div class="cf"></div>
    </div>
</div>
{{HTML::script(url('js/build/all.min.js'))}}
@section('footer-scripts')
@show