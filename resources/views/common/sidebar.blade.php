<?php if(in_array('add-btn', $widgets)){?>
<div class="sidebar-widget">
    <div class="sidebar-widget-inside">
    <a href="{{url('event/add')}}" class="add-button">{{trans("sidebar.Add an Event Now")}}</a>
        <a href="{{url('business/add')}}" class="add-button">{{trans("sidebar.Add a Business Now")}}</a>
    </div>
</div>
<?php }?>
<?php if(in_array('top-category', $widgets)){?>
<div class="sidebar-widget category-widget">
    <div class="sidebar-widget-inside">
        <h3 class="widget-title">{{trans("home.Top Categories")}}</h3>
        <ul class="category-list">
            @foreach($categories as $category)
            @if(isset($category->style->color))
            <li>
                <a href="{{$category->page_url}}" class="category color-{{$category->style->color}}"><i class="icon {{$category->style->class}}"></i>{{trans("cat.".$category->name)}}</a>
            </li>
            @endif
            @endforeach
        </ul>
        <div class="cf"></div>
    </div>
</div>
<?php }?>
<?php if(in_array('latest-news', $widgets)&&isset($latest_events)){?>
<div class="sidebar-widget">
    <div class="sidebar-widget-inside">
        <h3 class="widget-title">{{trans("sidebar.Latest Events")}}</h3>
        @include('zucko.event.components.list-sidebar', array('events'=>$latest_events))

        <div style="text-align:right;margin-bottom:10px;margin-top:-8px;margin-right:10px;">
        <a class="red-button" href="{{url('events')}}">{{trans("sidebar.SHOW MORE")}}</a>
        </div>
        <div class="cf"></div>
    </div>
</div>
<?php }?>
<?php if(in_array('mobile-app', $widgets)){?>
<div class="mobile-app-notice {{Config::get("app.locale")}}">
    <a href="#" class="iphone-app-btn"></a>
    <a href="#" class="android-app-btn"></a>
</div>
<?php }?>
