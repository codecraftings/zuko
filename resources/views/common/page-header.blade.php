<div class="row top">
    <div class="container">
        <div class="top-menu-container">
            <ul class="menu top-menu">
                @foreach($top_cities as $city)
                <li class="top-menu-item">
                    <a href="{{$city->events_page}}">{{$city->name}}</a>
                </li>
                @endforeach
                <li class="top-menu-item">
                    <a class="more-btn" href="{{url('cities')}}">{{trans("header.more_cities")}} >></a>
                </li>
            </ul>
           
        </div>
        <div class="enter-notice fr">
            @if(isset($current_user))
                <div class="current-user-module">
                    <img width="50px" height="50px" class="fl" src="{{$current_user->profile_pic}}"/>
                    <div class="info fl">
                        <a class="name" href="{{$current_user->profile_url}}">{{$current_user->present()->name()}}</a>
                        <a class="small" href="{{url('user/'.$current_user->id.'/edit')}}">Edit Profile</a>
                        |
                        <a class="small" href="{{url('logout')}}">Logout</a>
                    </div>
                    <div class="cf"></div>
                </div>
            @else
            <a href="{{url('signup')}}" class="btn signup">{{trans("header.signup")}}<i class="black-arrow"></i></a>
            <a href="{{url('login')}}" class="btn login">{{trans("header.login")}}<i class="black-arrow"></i></a>
            @endif
        </div>
         <span class="fr" style="margin-right: 21px; text-align: right; margin-top: 13px;">
            <a href="{{url_append_query("locale=en")}}" class="sprite eng"></a>
            <a href="{{url_append_query("locale=fr")}}" class="sprite french"></a>
        </span>
    </div>
</div>
<div class="row middle">
    <div class="container">
        <div class="logo-container">
            <h1 class="logo"><a href="{{url('/')}}">Annuaire Polynesian</a></h1>
        </div>
        <div class="stay-connected">
            <div class="graphic">{{trans("header.stay_connected")}}</div>
            <div class="social-icons-container">
                <a href="#" class="social-blue fb"></a>
                <a href="#" class="social-blue twitter"></a>
                <a href="#" class="social-blue youtube"></a>
            </div>
        </div>
    </div>
</div>
<div class="row bottom">
    <div class="container">
        <div class="main-menu-container">
            <ul class="menu main-menu">
                <li class="main-menu-item">
                    <a href="{{url('/')}}">{{trans("header.home")}}</a>
                </li>
                <li class="main-menu-item">
                    <a href="{{url('page/about')}}">{{trans("header.about_us")}}</a>
                </li>
                <li class="main-menu-item">
                    <a href="{{url('reviews/write')}}">{{trans("header.write_review")}}</a>
                </li>
                <li class="main-menu-item">
                    <a href="{{url('friends')}}">{{trans("header.find_friends")}}</a>
                </li>
                <li class="main-menu-item">
                    <a href="{{url('inbox')}}">{{trans("header.messages")}}</a>
                </li>
                <li class="main-menu-item">
                    <a href="{{url('blogs')}}">{{trans("header.blog")}}</a>
                </li>
                <li class="main-menu-item">
                    <a href="{{url('events')}}">{{trans("header.events")}}</a>
                </li>
                <li class="main-menu-item">
                    <a href="{{url('page/contact')}}">{{trans("header.contact_us")}}</a>
                </li>
            </ul>
        </div>
    </div>
</div>