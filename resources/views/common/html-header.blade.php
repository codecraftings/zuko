<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Annuaire Polynesian">
<link rel="shortcut icon" href="img/favicon.png">
<title>
@section('window-title')
Zuckoo By Annuaire Polynesian
@show
</title>
<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,400italic|Titillium+Web:600,400|PT+Sans+Narrow:400' rel='stylesheet' type='text/css'>
{{HTML::style('css/all.min.css')}}
<script type="text/javascript">
	var base_url = "{{url('/')}}";
	@if(isset($current_user))
		var current_user = {{json_encode($current_user)}};
		@else
		var current_user = "null";
	@endif
</script>
{{HTML::script('js/build/jquery.min.js')}}
@section('extra-scripts')
@show
<!--[if IE]>
<link rel="stylesheet" href="css/ie.css">
<link rel="stylesheet" href="css/jquery.placeholder.min.css">
<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
<![endif]-->