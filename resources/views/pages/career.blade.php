@extends('layouts.main')
@section('window-title')
Career At Zucko | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">Career At Zucko</span>
</h2>
<p>
    No job position available at this moment.
</p><br>
<!-- Main Content Here -->
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
@stop