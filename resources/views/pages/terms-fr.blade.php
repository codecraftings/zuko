@extends('layouts.main')
@section('window-title')
    Termes et Condtions Générales | @parent
@stop
@section('main-content')
<h2 class="title-stripped">
    <span class="text">Termes et Condtions Générales</span>
</h2>
<br>
<?php
        $data = <<<EOF
Dernière mise à jour: 18 juin 2014
Veuillez prendre connaissance avec soin de ces Termes et Conditions Générales ("Termes", "Termes et Conditions Générales") avant de recourir au site Internet  http://www.zuckoo.com  (le "Service") exploité par  B EDITION, rue Wallis, Immeuble Wallisa, 2e étage, Fariipiti, Papeete ; BP 1930 _ 98713 Papeete

Votre accès au Service et à son utilisation restent subordonnés à votre acceptation de ces Conditions Générales  et à votre engagement de les respecter. Ces Termes s'appliquent à tous les visiteurs, usagers et autres qui accèdent au Service ou ont recours au Service.

En accédant au Service ou en utilisant le Service, vous acceptez de vous soumettre aux obligations  de ces Conditions Générales. Si vous n'êtes pas d'accord avec une quelconque clause de ces termes, alors vous ne serez pas autorisés à accéder au Service.


Contenu

Notre Service vous permet de poster, lier, stocker, partager et encore de faciliter l'accès à certaines informations, à des textes, illustrations, vidéos, ou autre support matériel (“Contenu”) Vous demeurez responsable  du Contenu que vous postez au Service, du point de vue de sa légalité, de son sérieux et de sa pertinence.
En postant le Contenu au Service, vous nous accordez le droit et la permission d'utiliser, modifier, exécuter publiquement , présenter publiquement, reproduire et distribuer ce Contenu sur et à travers le Service. Vous conservez chacun et l'ensemble de vos droits à tout Contenu que vous soumettez, postez ou affichez sur ou à travers le service et vous êtes responsable de la protection de ces droits. Vous acceptez que cette permission comprenne le droit pour nous de mettre votre Contenu à la disposition d'autres usagers du Service, lesquels peuvent également faire usage de votre Contenu dans le respect de ces Conditions Générales,

Vous déclarez et garantissez que : (i)  le Contenu est à vous (il vous appartient en propre) ou que vous avez le droit de l'utiliser et que vous nous accordez le droits et la licence conformément à ces Termes, et (ii)  que poster votre Contenu sur ou à travers le Service ne viole en rien les droits à la confidentialité, les droits publicitaires, les droits de reproduction, les droits contractuels et tous les autres droits de quelque personne que ce soit.

Comptes
Lorsque vous créez un compte chez nous, vous devez nous fournir des renseignements qui soient précis, complets et à jour à tout moment. Tout manquement à cette obligation constitue une infraction aux Conditions Générales qui peut se solder par une résiliation immédiate de votre compte sur notre Service,

Vous êtes responsable de la sauvegarde du mot de passe que vous utilisez pour accéder au Service et pour toutes activités ou actions réalisées sous couvert de votre mot de passe, que votre mot de passe soit utilisé avec notre Service ou avec un service tiers.

Vous consentez à ne pas communiquer votre mot de passe à quelque tiers que ce soit. Vous devez nous aviser immédiatement si vous êtes informé de toute violation de la sécurité ou de l'utilisation frauduleuse de votre compte.

Vous nêtes pas autorisé à utiliser comme nom d'utilisateur le nom d'une autre personne ou entité ou un nom qui ne serait pas légalement disponible, un nom ou une marque commerciale sur lesquels existent des droits d'une autre personne ou entité autre que vous-même sans autorisation appropriée, ou un nom qui pourrait être choquant, vulgaire ou obscène.

Propriété Intellectuelle

Le Service et son contenu d'origine (à l'exclusion du Contenu fourni par les usagers), les caractéristiques et fonctionnalités sont et resteront la propriété exclusive de  B EDITION et de ses bailleurs de licence, Le Service est protégé par des droits de reproduction réservés, des marques commerciales, et autres textes de lois aussi bien de  Polynésie Française que de pays étrangers. Nos marques commerciales et images commerciales ne peuvent être exploitées  en rapport avec quelque produit ou service sans l'accord préalable écrit de B EDITION.

Liens vers d'autres Sites Web

Notre Service peut contenir des liens vers des sites web tiers ou des services qui n'appartiennent pas à ou ne sont pas contrôlés par  B EDITION.

B EDITION  n'a aucun contrôle sur, et ne saurait assumer quelque responsabilité à l'égard du contenu, des politiques de confidentialité, ou des pratiques de tous sites web ou services tiers. Vous reconnaissez en outre et acceptez que B EDITON ne saura en aucun cas être tenu responsable ou comptable, directement ou indirectement, de tous dommages ou pertes causés ou présumés causés  par ou en rapport avec l'usage de ou la dépendance à ce contenu, ces biens ou services disponibles sur ou à travers ces sites ou services web.

Nous vous conseillons vivement de lire les termes et conditions générales ainsi que les politiques de confidentialité de tous sites ou services web tiers que vous visitez.

Résiliation
Nous avons la faculté de résilier ou suspendre l'accès à notre Service avec effet immédiat, sans avis préalable ou responsabilité de notre part, pour tout motif quel qu'il soit, y compris, sans limitation aucune, si vous ne respectez pas les conditions générales.

Toutes les dispositions des Termes qui, de par leur nature, devraient être maintenues après résiliation, survivront de fait à la résiliation, y compris, sans limitation aucune, les dispositions concernant la propriété, les renonciations de garanties, les indemnités et les limitations de responsabilité.

Nous avons le loisir de résilier ou suspendre votre compte avec effet immédiat, sans préavis ou responsabilité de notre part, pour toute raison quelle qu'elle soit, y compris et sans limitation si vous ne respectez pas les Conditions.

A la résiliation, votre droit d'utiliser le Service prendra fin immédiatement. Si vous souhaitez résilier votre compte, vous pouvez simplement cesser d'utiliser le Service,

Toutes les dispositions des Conditions qui, par leur nature, devraient survivre à la résiliation, survivront effectivement à la résiliation, y compris, sans que la liste soit restrictive, des dispositions de propriété, des renonciations à des garanties, des indemnités et des limites de responsabilité,

Indemnisation

Vous consentez à protéger, indemniser ou tenir B EDITION et ses licenciés et bailleurs de licence, ainsi que leurs employés, partenaires commerciaux, agents, employés et administrateurs indemnes de toute action intentée  pour plainte, dommages, obligations, pertes, responsabilités, frais ou dettes, et autres dépenses (y compris mais, sans que ce soit limitatif, les honoraires d'avocat), résultant de ou nés de a) votre utilisation et accès au Service de votre fait ou du fait d'une personne utilisant votre compte ou votre mot de passe, ou  b) une rupture de ces Termes et Conditions, ou c) le Contenu posté sur le Service

You agree to defend, indemnify and hold harmless B EDITION and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password; b) a breach of these Terms, or c) Content posted on the Service.

Limitation de Responsabilité
En aucun cas  ni B EDITION, ni ses administrateurs, employés, partenaires, agents, fournisseurs, ou affiliés ne sauront être tenus pour responsables de dommages indirects, connexes, spéciaux, directs ou répressifs, y compris sans limitation aucune, pertes de profit, de données, privation d'utilisation, perte de valeur, ou autres pertes immatérielles, résultant de  (i) votre accès à ou utilisation de ou incapacité à accéder ou à utiliser le Service; (ii) toute activité ou utilisation de contenu d'un tiers quelconque sur le Service; (iii) tout contenu obtenu du Service; et  (iv) accès , usage non autorisé, ou altération de votre transmission ou du contenu, que ce soit au titre de la garantie, du contrat ou pour raison délictuelle (y compris négligence), ou pour toute autre raison légale; que nous ayons ou n'ayons pas été informés de la possibilité de survenance de ces sinistres, et même si un recours prévu dans les présentes est reconnu comme n'ayant pas atteint son objectif essentiel.

Dégagement de Responsabilité au titre de la Garantie
Vous utilisez le Service à vos risques exclusifs. Le service est fourni sur la base “EN L'ETAT” et “POUR AUTANT QU'IL EST DISPONIBLE”. Le Service est fourni sans garanties d'aucune sorte, expresse ou implicite, y compris, mais sans que ce soit restrictif, les garanties implicites de  qualité marchande, d'adéquation à un but particulier, la non-infraction ou les modalités d'exécution.

B EDITION, ses succursales, filiales, et ses licenciés ne garantissent pas que a) le Service  fonctionnera de façon ininterrompue, stable ou sera disponible à tout moment ou à tout endroit particulier; b) toutes erreurs ou défectuosités seront corrigées; c) le Service est exempt de virus ou autres composants dangereux; ou d) les résultats de l'utilisation du Service correspondront à vos exigences.

Dérogations
Certaines juridictions n'admettent pas l'exclusion de certaines garanties ou l'exclusion ou limite de responsabilité pour des dommages consécutifs et accessoires, de sorte que les limitations signalées ci-dessus peuvent ne pas s'appliquer à vous,

Droit applicable
Ces Termes seront régis par et interprétés selon les lois en vigueur en  Polynésie Française, indépendamment de leurs dispositions en matière de conflit de loi et excluent les Conventions des Nations Unies sur les Contrats de Vente Internationale de Marchandises (CVIM).
Notre incapacité à faire respecter certains droits et dispositions de ces Termes ne pourra être interprétée comme une renonciation à ces mêmes droits. Si l'une des dispositions de ces Conditions est tenue pour invalide ou inapplicable par un tribunal, les autres clauses de ces Termes et Conditions  conserveront leur effet. Ces Termes constituent l'ensemble de l'accord qui nous lie concernant notre Service et annule et remplace tous accords préalables que nous pourrions avoir passés concernant le Service.

Modifications
Nous nous réservons le droit, à notre entière discrétion, de modifier ou remplacer ces Conditions à  quelque moment que ce soit. Si une révision est de type matériel, nous essaierons de donner un préavis d'au moins 30 jours avant la prise d'effet de nouvelles conditions. Ce qui constitue un changement matériel sera déterminé à notre seule discrétion.

En continuant d'accéder et d'utiliser notre Service après la prise d'effet de ces révisions, vous acceptez de vous conformer aux conditions modifiées. Si vous n'acceptez pas les nouvelles conditions, veuillez-vous abstenir d'utiliser le Service.

Nous contacter
Si vous avez quelque question à formuler concernant ces Conditions Générales, veuillez prendre contact avec nous.
EOF;
        echo nl2br($data);
        ?>
<br>
<br>
<!-- Main Content Here -->
@stop
@section('sidebar')
@include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
@stop
