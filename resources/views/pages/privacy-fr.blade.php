@extends('layouts.main')
@section('window-title')
    Charte de confidentialité | @parent
@stop
@section('main-content')
    <h2 class="title-stripped">
        <span class="text">Charte de confidentialité</span>
    </h2>
    <br>
    <p>
        <?php
        $data = <<<EOF
Dernière mise à jour: 25 mars 2015
B EDITION exploite le site Internet  http://www.zuckoo.com (le"Service").

Cette page vous informe des politiques que nous appliquons dans le cadre du recueil, de l'utilisation et de la communication de Renseignements Personnels lorsque vous avez recours à notre Service.

Nous n'utiliserons ni ne divulguerons vos informations auprès de qui que ce soit sauf dans les conditions  décrites dans cette Charte de Confidentialité.

Si nous nous servons de vos Renseignements Personnels, c'est pour mettre notre Service à votre disposition et pour l'améliorer. En ayant recours à notre Service, vous acceptez la collecte et l'exploitation des renseignements dans les conditions stipulées dans cette charte. Sauf s'il leur est donné un autre sens dans cette Charte de Confidentialité, les termes utilisés dans cette Charte de Confidentialité ont la même signification que dans nos Conditions Générales dont vous pouvez prendre connaissance en visitant le site http://www.zuckoo.com

Recueil et Utilisation des Informations

Lorsque vous utilisez notre Service, il est possible que nous vous demandions de nous fournir  certains renseignements d'identification personnelle qui peuvent être utilisés pour vous contacter ou vous identifier. Ces renseignements d'identification personnelle peuvent comprendre, sans que la liste soit limitative, votre adresse email, votre nom, d'autres informations (“Renseignements Personnels”).

Données Enregistrées à la Connexion

Nous collectons les informations que votre navigateur envoie chaque fois que vous visitez notre Service (“Données Enregistrées à la Connexion”). Ces Données de Connexion peuvent comprendre des informations telles que votre adresse IP (“Protocole Internet”), le type de navigateur que vous utilisez, la version du navigateur, les pages de notre Service que vous parcourez, l'heure et la date de votre visite, le temps que vous avez passé sur ces pages et d'autres données statistiques.

En outre, nous avons la faculté d'utiliser des services tiers tels que Google Analytics qui nous permettent de recueillir, contrôler et analyser ce type de renseignements de manière à accroître les fonctionnalités de notre Service. Ces fournisseurs de services tiers ont leurs propres politiques de confidentialité expliquant comment ils traitent ce genre d'informations,


Cookies

Les Cookies sont des fichiers contenant une petite quantité de données, lesquelles peuvent inclure un identifiant anonyme unique. Les Cookies sont envoyés à votre navigateur à partir d'un site web et sont stockés sur le disque dur de votre ordinateur. .

Nous utilisons les Cookies pour collecter des informations. Vous pouvez donner instruction à votre navigateur de refuser tous les cookies ou de vous prévenir quand un cookie est en cours d'envoi. Cependant, si vous n'acceptez pas les cookies, il vous sera peut-être impossible d'utiliser certaines fonctionnalités de notre Service.

Communication Do Not Track (Sans Suivi)

Nous ne proposons pas de dispositif de suivi  Do Not Track ("DNT"). Do Not Track est une option que vous pouvez sélectionner dans votre navigateur web pour informer les sites internet que vous ne souhaitez pas qu'un suivi de vos opérations soit effectué.

Vous pouvez activer ou désactiver ou can enable or disable Do Not Track by visiting the Preferences or Settings page of your web browser.

Prestataires de Service

Nous avons la faculté de recourir à des tiers, sociétés et personnes individuelles, pour faciliter la mise à disposition de notre Service, pour offrir notre Service en notre nom, pour effectuer des prestations liées à notre Service ou pour nous assister dans l'analyse de la façon dont notre Service est  utilisé.

Ces tiers n'ont accès à vos Renseignements Personnels que pour exécuter ces tâches en notre nom et ils ont l'obligation de ne pas divulguer ou utiliser ces informations à un quelconque autre titre.


Sécurité

La Sécurité de vos renseignements personnels est très importante à nos yeux, mais gardez bien à l'esprit qu'aucune méthode de transmission via l'Internet, ou méthode de stockage électronique n'est sûre à 100%. Si nous nous efforçons de recourir à des moyens économiquement acceptables pour protéger vos  Renseignements Personnels, nous ne pouvons garantir qu'ils sont absolument sûrs.

Transfert International

Vos informations, y compris vos Renseignements Personnels peuvent être transférés à – et conservés dans – des ordinateurs situés hors de votre état, province, pays ou toute autre juridiction gouvernementale dans lesquels les lois de protection des données peuvent différer de celles de votre ressort territorial.

Si vous résidez hors de la Polynésie Française et si vous choisissez de nous fournir des informations, veuillez noter que nous transférons les informations, y compris les Renseignements Personnels en  Polynésie Française et que nous les traitons ici,

Votre accord sur cette Charte de Confidentialité suivie de la communication de ces informations  représente votre consentement concernant ce transfert.

Liens vers d'autres sites

Notre Service peut vous proposer des liens vers d'autres sites qui ne relèvent pas de notre exploitation. Si vous cliquez un lien de tierce partie, vous serez redirigés vers le site de ce tiers. Nous vous recommandons instamment de prendre connaissance de la Charte de Confidentialité de tous les sites que vous visitez,

Nous ne disposons d'aucun contrôle et en conséquence n'assumons aucune responsabilité quant au contenu, aux chartes de confidentialité ou pratiques de tous les sites ou services tiers.


Protection de la Vie Privée des Enfants

Notre Service ne s'adresse à aucune personne de moins de 13 ans (“Enfants”).

Nous ne recueillons intentionnellement aucune information d'identification personnelle d'enfants de moins de 13 ans. Si vous êtes un parent ou un tuteur et si vous apprenez que votre Enfant nous a fourni des Renseignements Personnels, veuillez prendre contact avec nous. Si nous découvrons que des Enfants de moins de 13 ans nous ont communiqué des Renseignements Personnels, nous effacerons immédiatement toutes ces informations de nos serveurs,

Modifications de cette Charte de Confidentialité

Nous avons la faculté de mettre à jour périodiquement notre Charte de Confidentialité. Nous vous informerons de toute modification éventuelle en postant la nouvelle Charte de Confidentialité sur cette page,
Il vous est conseillé de relire périodiquement cette Charte de Confidentialité pour le cas où il y aurait des modifications. Les modifications de cette Charte de Confidentialité prennent effet lorsqu'elles sont postées sur cette page. .

Nous contacter
Si vous souhaitez des réponses à toutes questions que vous vous posez sur cette Charte de Confidentialité, veuillez prendre contact avec nous par email adressé à contact[at]zuckoo.com

EOF;
        echo nl2br($data);
        ?>
    </p><br>
    <!-- Main Content Here -->
@stop
@section('sidebar')
    @include('common.sidebar',array('widgets'=>array('add-btn','latest-news','mobile-app')))
@stop