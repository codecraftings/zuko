<?php 
return [
	"form1" => "L'Annuaire Polynesien est egalement un reseau social vous permettant de facilement communiquer avec vos amis et vos proches. En vous enregistrant sur notre site internet vous allez pouvoir acceder a botre boite mail mais aussi aux profiles d'autres utilisateurs et bien plus!",
	"Register Here" => "Dites-nous en un peu plus sur vous",
	"First Name" => "Prenom",
	"Last Name" => "Nom",
	"Birthday" => "Anniversaire",
	"Month" => "Mois",
	"Day" => "jour",
	"Year" => "Annee",
	"Nick Name" => "Pseudonyme",
	"form2" => "Si vous cochez la case, les utilisateurs ne verront que votre pseudonyme et plus votre nom et prenom",
	"Password" => "Mot de passe",
	"Confirm Password" => "Confirmer le mot de passe",
	"Gender" => "Sexe",
	"Male" => "Homme",
	"Female" => "Femme",
	"Zip Code" => "Code postale",
	"City Name" => "Ville",
	"Email Address" => "Adresse Email
",
	"Confirm Email Adress" => "Confirmer adresse Email",
	"Security Code" => "Code de verification",
	"Browse" => "Parcourir",
	"Submit" => "Envoyer",

	"form4" => "En creant votre compte, vous etes d'accord avec les",

	"Terms of Service" => "Termes d'utillisation",
	"Privacy Policy" => "donnes personelles",
];