<?php 
return [
	"About" => "Infos",
	"Terms of Service" => "Termes d'utilisation",
	"Privacy Policy" => "Donnees personnelles",
	"Help" => "Aide",
	"Advertise" => "Publiser sur notre site",
	"Contact Us" => "Contactez Nous",
	"More" => "Plus",
	"Careers" => "L'emploi",
	"Your Name" => "Nom",
	"Email Address" => "Adresse Email",
	"Follow" => "Suivez",
	"Us On" => "Nous Sur",
	"Recent" => "Recents",
];