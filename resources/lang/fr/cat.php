<?php
return [
    "Accomodation"     => "Hebergement",
    "Tourism"          => "Tourisme",
    "Roulottes"        => "Roulottes",
    "Quick Assistance" => "Quick Assistance",
    "Night Clubs"      => "Night Clubs",
    "Bars"             => "Bars",
    "Brand"            => "Marque",
    "Mobile"           => "VINI",
    "Hotels"           => "Hotels",
    "Transports"       => "Transports",
    "Restaurants"      => "Restaurants",
    "Hobbies"          => "Hobbies",
    "Administration"   => "Administration",
];