<?php 
return [
	"Post comment" => "Poster commentaire",
	"Add A Blog" => "Creer un blog",
	"Pick a category" => "Choisissez une categorie",
	"Enter blog title and description" => "Entrez un titre et une description",
	"Your Blog Title" => "Titre du blog",
];