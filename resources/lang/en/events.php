<?php 
return [
	"Find Your Events Below" => "Find Your Events Below",
	"Today" => "Today",
	"Tomorrow" => "Tomorrow",
	"This Weekend" => "This Weekend",
	"This Week" => "This Week",
	"Next Week" => "Next Week",
];