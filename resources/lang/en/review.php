<?php 
return [
	"Write Review" => "Write Review",
	"Post Review" => "Post Review",
	"Write Review" => "Write Review",
	"Which Business Would You Like to Review" => "Which Business Would You Like to Review",
	"Search for business" => "Search for business",
	"Select the business" => "Select the business",
	"Write your review" => "Write your review",
	"Select From the List Below or Search Again" => "Select From the List Below or Search Again",
	"Business Name" => "Business Name",
	"Near" => "Near",
	"SEARCH BUSINESS" => "SEARCH BUSINESS",
	"Place" => "Place",
	"Category" => "Category",
	"Neighborhood" => "Neighborhood",
	"Write a Review" => "Write a Review",
	"Write your review" => "Write your review",
	"REPLY" => "REPLY",
	"Write your reply" => "Write your reply",
	"Recent Reviews" => "Recent Reviews",
	"USEFUL" => "USEFUL",
	"FUNNY" => "FUNNY",
	"Link to this review" => "Link to this review",
	"Was this review" => "Was this review",
	"Useful" => "Useful",
	"Funny" => "Funny",
	"POST" => "POST",
	"phone" => "Phone",
	"Address, Neighborhood, City or Zip Code" => "Address, Neighborhood, City or Zip Code",
	"Pick a rating" => "Pick a rating",
];