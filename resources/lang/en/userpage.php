<?php
return array(
		'friends_count' => '{0} No Friends|{1} 1 Friend|[1,Inf] :count Friends',
		'reviews_count' => '{0} No Reviews|{1} 1 Review|[1,Inf] :count Reviews',
		'photos_count' => '{0} No Photos|{1} 1 Photo|[1,Inf] :count Photos'
	);